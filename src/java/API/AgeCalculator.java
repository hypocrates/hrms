/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;

/**
 *
 * @author Wamuyu
 */
public class AgeCalculator {
    public static int calculateAge(String dOB) {
        String[] birth = dOB.split("/");
        LocalDate birthDate = LocalDate.of(Integer.parseInt(birth[2]), Integer.parseInt(birth[0]), Integer.parseInt(birth[1]));
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }
    
    public static int calculateAge2(Date date, String dOB){
        String data = date.toString();
        String[] birth = dOB.split("/");
        String[] thenDate = data.split("-");
        LocalDate setDate = LocalDate.of(Integer.parseInt(thenDate[0]), Integer.parseInt(thenDate[1]), Integer.parseInt(thenDate[2]));
        LocalDate birthDate = LocalDate.of(Integer.parseInt(birth[2]), Integer.parseInt(birth[0]), Integer.parseInt(birth[1]));
        if ((birthDate != null) && (setDate != null)) {
            return Period.between(birthDate, setDate).getYears();
        } else {
            return 0;
        }
        
    }
}
