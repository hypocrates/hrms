/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.Year;
import DBConnector.DBConnection;
import java.sql.*;
import java.util.Calendar;

/**
 *
 * @author Wamuyu
 */
public class ChartDB {
    public Year getDoctorsData(int dRef, int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        //Get current Year
        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        
        Year year = new Year();
        String query = "SELECT COUNT(*) FROM `session` "
                + "WHERE `D_Ref` = ? "
                + "AND `Hospital` = ? "
                + "AND `Entry_Date` < ? "
                + "AND `Entry_Date` >= ?";
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setInt(2, hRef);
            
            for(int x = 01; x<13; x++){
                int mwaka = thisYear;
                int nextM = x+1;
                int thisM = x;
                String nextMn;
                String thisMn;
                if (nextM <10){
                    nextMn = "0"+nextM; 
                } else{
                    nextMn = ""+nextM;
                }
                if (thisM <10){
                    thisMn = "0"+thisM; 
                } else{
                    thisMn = ""+thisM;
                }
                String nextMonth = mwaka+"-"+nextMn+"-01 00:00:00";
                String thisMonth = mwaka+"-"+thisMn+"-01 00:00:00";
                
                ps.setString(3, nextMonth);
                ps.setString(4, thisMonth);
                
                rs = ps.executeQuery();
                ps.setString(3, nextMonth);
                if(rs.first()){
                    switch (x) {
                        case 1:
                            year.setJan(rs.getInt("COUNT(*)"));
                            break;
                        case 2:
                            year.setFeb(rs.getInt("COUNT(*)"));
                            break;
                        case 3:
                            year.setMarch(rs.getInt("COUNT(*)"));
                            break;
                        case 4:
                            year.setApril(rs.getInt("COUNT(*)"));
                            break;
                        case 5:
                            year.setMay(rs.getInt("COUNT(*)"));
                            break;
                        case 6:
                            year.setJune(rs.getInt("COUNT(*)"));
                            break;
                        case 7:
                            year.setJuly(rs.getInt("COUNT(*)"));
                            break;
                        case 8:
                            year.setAugust(rs.getInt("COUNT(*)"));
                            break;
                        case 9:
                            year.setSept(rs.getInt("COUNT(*)"));
                            break;
                        case 10:
                            year.setOct(rs.getInt("COUNT(*)"));
                            break;
                        case 11:
                            year.setNov(rs.getInt("COUNT(*)"));
                            break;
                        case 12:
                            year.setDec(rs.getInt("COUNT(*)"));
                            break;
                        default:
                            break;
                    }
                }
            }
            System.out.println(year.toString());
            return year;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public Year getPatientsData(int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        //Get current Year
        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        
        Year year = new Year();
        String query = "SELECT COUNT(*) FROM `session` "
                + "WHERE `Hospital` = ? "
                + "AND `Entry_Date` < ? "
                + "AND `Entry_Date` >= ?";
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hRef);
            
            for(int x = 01; x<13; x++){
                int mwaka = thisYear;
                int nextM = x+1;
                int thisM = x;
                String nextMn;
                String thisMn;
                if (nextM <10){
                    nextMn = "0"+nextM; 
                } else{
                    nextMn = ""+nextM;
                }
                if (thisM <10){
                    thisMn = "0"+thisM; 
                } else{
                    thisMn = ""+thisM;
                }
                String nextMonth = mwaka+"-"+nextMn+"-01 00:00:00";
                String thisMonth = mwaka+"-"+thisMn+"-01 00:00:00";
                
                ps.setString(2, nextMonth);
                ps.setString(3, thisMonth);
                
                rs = ps.executeQuery();
                ps.setString(3, nextMonth);
                if(rs.first()){
                    switch (x) {
                        case 1:
                            year.setJan(rs.getInt("COUNT(*)"));
                            break;
                        case 2:
                            year.setFeb(rs.getInt("COUNT(*)"));
                            break;
                        case 3:
                            year.setMarch(rs.getInt("COUNT(*)"));
                            break;
                        case 4:
                            year.setApril(rs.getInt("COUNT(*)"));
                            break;
                        case 5:
                            year.setMay(rs.getInt("COUNT(*)"));
                            break;
                        case 6:
                            year.setJune(rs.getInt("COUNT(*)"));
                            break;
                        case 7:
                            year.setJuly(rs.getInt("COUNT(*)"));
                            break;
                        case 8:
                            year.setAugust(rs.getInt("COUNT(*)"));
                            break;
                        case 9:
                            year.setSept(rs.getInt("COUNT(*)"));
                            break;
                        case 10:
                            year.setOct(rs.getInt("COUNT(*)"));
                            break;
                        case 11:
                            year.setNov(rs.getInt("COUNT(*)"));
                            break;
                        case 12:
                            year.setDec(rs.getInt("COUNT(*)"));
                            break;
                        default:
                            break;
                    }
                }
            }
            System.out.println(year.toString());
            return year;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public Year getPatientsDataLastYear(int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        //Get current Year
        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        
        Year year = new Year();
        String query = "SELECT COUNT(*) FROM `session` "
                + "WHERE `Hospital` = ? "
                + "AND `Entry_Date` < ? "
                + "AND `Entry_Date` >= ?";
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hRef);
            
            for(int x = 01; x<13; x++){
                int mwaka = thisYear - 1;
                int nextM = x+1;
                int thisM = x;
                String nextMn;
                String thisMn;
                if (nextM <10){
                    nextMn = "0"+nextM; 
                } else{
                    nextMn = ""+nextM;
                }
                if (thisM <10){
                    thisMn = "0"+thisM; 
                } else{
                    thisMn = ""+thisM;
                }
                String nextMonth = mwaka+"-"+nextMn+"-01 00:00:00";
                String thisMonth = mwaka+"-"+thisMn+"-01 00:00:00";
                
                ps.setString(2, nextMonth);
                ps.setString(3, thisMonth);
                
                rs = ps.executeQuery();
                ps.setString(3, nextMonth);
                if(rs.first()){
                    switch (x) {
                        case 1:
                            year.setJan(rs.getInt("COUNT(*)"));
                            break;
                        case 2:
                            year.setFeb(rs.getInt("COUNT(*)"));
                            break;
                        case 3:
                            year.setMarch(rs.getInt("COUNT(*)"));
                            break;
                        case 4:
                            year.setApril(rs.getInt("COUNT(*)"));
                            break;
                        case 5:
                            year.setMay(rs.getInt("COUNT(*)"));
                            break;
                        case 6:
                            year.setJune(rs.getInt("COUNT(*)"));
                            break;
                        case 7:
                            year.setJuly(rs.getInt("COUNT(*)"));
                            break;
                        case 8:
                            year.setAugust(rs.getInt("COUNT(*)"));
                            break;
                        case 9:
                            year.setSept(rs.getInt("COUNT(*)"));
                            break;
                        case 10:
                            year.setOct(rs.getInt("COUNT(*)"));
                            break;
                        case 11:
                            year.setNov(rs.getInt("COUNT(*)"));
                            break;
                        case 12:
                            year.setDec(rs.getInt("COUNT(*)"));
                            break;
                        default:
                            break;
                    }
                }
            }
            System.out.println(year.toString());
            return year;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
