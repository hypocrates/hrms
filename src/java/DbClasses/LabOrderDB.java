/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.CardicTestData;
import Bean.CreatinineTestData;
import Bean.ElectrolytesTestData;
import Bean.GeneralTestData;
import Bean.LiverTestData;
import Bean.OtherTestData;
import DBConnector.DBConnection;
import java.sql.*;
/**
 *
 * @author Wamuyu
 */
public class LabOrderDB {
    public int addGeneralTests(int session, GeneralTestData gTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `general_tests`(`Session`, `Blood Sugar`, "
                + "`Urea`, `Uric Acid`, `Calcium`, `Acid Phosphate`, "
                + "`Cholesterol`, `Amylase`, `Phosphorus`, `Total Protein`, "
                + "`Albumen`, `Globulin`) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, gTests.getbSugar());
            ps.setInt(3, gTests.getUrea());
            ps.setInt(4, gTests.getUricAcid());
            ps.setInt(5, gTests.getCalcium());
            ps.setInt(6, gTests.getAcidPhospate());
            ps.setInt(7, gTests.getCholestrol());
            ps.setInt(8, gTests.getAmylase());
            ps.setInt(9, gTests.getPhosphorus());
            ps.setInt(10, gTests.getTotalProtein());
            ps.setInt(11, gTests.getAlbumen());
            ps.setInt(12, gTests.getGlobulin());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addElectrolytesTests(int session, ElectrolytesTestData eTests){
        Connection connection =  DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `electrolytes_tests`(`Session`, "
                + "`Sodium`, `Potassium`, `Chloride`, `Bicarbonate`) "
                + "VALUES (?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, eTests.getSodium());
            ps.setInt(3, eTests.getPotassium());
            ps.setInt(4, eTests.getChloride());
            ps.setInt(5, eTests.getBicarbonate());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addLiverTest(int session, LiverTestData lTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `liver_tests`(`Session`, `BTotal`, "
                + "`BDirect`, `BIndirect`, `AlkalinePhosphate`, `SGOT`, `SGPT`, "
                + "`YGT`) "
                + "VALUES (?,?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, lTests.getbTotal());
            ps.setInt(3, lTests.getbDirect());
            ps.setInt(4, lTests.getbIndirect());
            ps.setInt(5, lTests.getAlkalinePhosphate());
            ps.setInt(6, lTests.getSgot());
            ps.setInt(7, lTests.getSgpt());
            ps.setInt(8, lTests.getYgt());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addCardicTest(int session, CardicTestData cTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `cardic_tests`(`Session`, `LDH`, "
                + "`HBD`, `CPK`, `SGOT`) "
                + "VALUES (?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, cTests.getLdh());
            ps.setInt(3, cTests.getHbd());
            ps.setInt(4, cTests.getCpk());
            ps.setInt(5, cTests.getSgot());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addCreatinineTest(int session, CreatinineTestData crTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `creatinine_tests`(`Session`, "
                + "`Clearance`, `Serum`, `Urine`) "
                + "VALUES (?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, crTests.getClearance());
            ps.setInt(3, crTests.getSerum());
            ps.setInt(4, crTests.getUrine());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addOtherTests(int session, OtherTestData oTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `other_tests`(`Session`, `Serum`,"
                + " `Plasma`, `Lipid Profile`, `Thyroid Function`, `PSA`,"
                + " `NormalTest`) "
                + "VALUES (?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, oTests.getSerum());
            ps.setInt(3, oTests.getPlasma());
            ps.setInt(4, oTests.getLipidProfile());
            ps.setInt(5, oTests.getThyroidFunction());
            ps.setInt(6, oTests.getPsa());
            ps.setInt(7, oTests.getNormalTest());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateOtherTestsNormal(int session, int normal){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `other_tests` SET `NormalTest` = ? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, normal);
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
        
    }
    
    public int updateOtherTestsLipid(int session, int lipid){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `other_tests` SET `Lipid Profile` = ? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, lipid);
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
        
    }
    
    public int updateOtherTestsThyroid(int session, int thyroid){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `other_tests` SET `Thyroid Function` = ? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, thyroid);
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
        
    }
    
    public int updateOtherTestsPsa(int session, int psa){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `other_tests` SET `PSA` = ? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, psa);
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
        
    }
}
