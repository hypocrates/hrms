/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.ChiefComplainsData;
import DBConnector.DBConnection;
import java.sql.*;
/**
 *
 * @author Wamuyu
 */
public class ChiefComplainsDB {
    public int saveChiefComplains(ChiefComplainsData ccData){
        int session = ccData.getSession();
        if(this.countChiefComplains(session)==0){
            return this.insertChiefComplain(ccData);
        } else{
            return this.updateChiefComplain(ccData);
        }
    }
    
    public int insertChiefComplain(ChiefComplainsData ccData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `chief_complains`(`Session`, "
                + "`D_Ref`, `Hospital`, `Diagnosis`, `Chief_Complains`, `BMI`, "
                + "`Blood_Pressure`) "
                + "VALUES (?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, ccData.getSession());
            ps.setInt(2, ccData.getDoctor());
            ps.setInt(3, ccData.getHospital());
            ps.setString(4, ccData.getDiagnosis());
            ps.setString(5, ccData.getChiefComplains());
            ps.setString(6, ccData.getbMI());
            ps.setString(7, ccData.getBloodPressure());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateChiefComplain(ChiefComplainsData ccData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `chief_complains` SET `D_Ref`=?,`Hospital`=?,"
                + "`Diagnosis`=?,`Chief_Complains`=?,`BMI`=?,`Blood_Pressure`=?"
                + " WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, ccData.getDoctor());
            ps.setInt(2, ccData.getHospital());
            ps.setString(3, ccData.getDiagnosis());
            ps.setString(4, ccData.getChiefComplains());
            ps.setString(5, ccData.getbMI());
            ps.setString(6, ccData.getBloodPressure());
            ps.setInt(7, ccData.getSession());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int countChiefComplains(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT COUNT(*) FROM `chief_complains` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public ChiefComplainsData getChiefComplain(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Session`, `D_Ref`, `Hospital`, `Diagnosis`,"
                + " `Chief_Complains`, `BMI`, `Blood_Pressure` "
                + "FROM `chief_complains` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            ChiefComplainsData ccData = new ChiefComplainsData();
            if(rs.first()){
                ccData.setSession(rs.getInt("Session"));
                ccData.setDoctor(rs.getInt("D_Ref"));
                ccData.setHospital(rs.getInt("Hospital"));
                ccData.setDiagnosis(rs.getString("Diagnosis"));
                ccData.setChiefComplains(rs.getString("Chief_Complains"));
                ccData.setbMI(rs.getString("BMI"));
                ccData.setBloodPressure(rs.getString("Blood_Pressure"));
            }
            
            return ccData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
