/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.PrescriptionData;
import DBConnector.DBConnection;
import java.sql.*;

/**
 *
 * @author Wamuyu
 */
public class PrescriptionDB {
    public int insertPrescriptionDiag(PrescriptionData dpData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `diagnosis`(`Session`, `D_Ref`,"
                + " `Diagnosis`, `Prescription`)"
                + " VALUES (?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dpData.getSession());
            ps.setInt(2, dpData.getDoctor());
            ps.setString(3, dpData.getDiagnosis());
            ps.setString(4, dpData.getPrescription());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    public int countPrescriptionDiag(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT COUNT(*) FROM `diagnosis` WHERE `Session`=?";
        
        try{
           ps = connection.prepareStatement(query);
           ps.setInt(1, session);
           rs = ps.executeQuery();
           int count = 0; 
           if(rs.first()){
               count = rs.getInt("COUNT(*)");
           }
           return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int savePrescriptionDiag(PrescriptionData dpData){
        int session = dpData.getSession();
        
        if(this.countPrescriptionDiag(session) == 0){
            return this.insertPrescriptionDiag(dpData);
        } else {
            return this.updatePrescriptionDiag(dpData);
        }
    }
    
    public int updatePrescriptionDiag(PrescriptionData dpData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `diagnosis` "
                + "SET `D_Ref`=?,`Diagnosis`=?,`Prescription`=? "
                + "WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dpData.getDoctor());
            ps.setString(2, dpData.getDiagnosis());
            ps.setString(3, dpData.getPrescription());
            ps.setInt(4, dpData.getSession());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public PrescriptionData getPrescriptionDiag(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `D_Ref`, `Diagnosis`, `Prescription` FROM `diagnosis` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            PrescriptionData dpData = new PrescriptionData();
            if(rs.first()){
                dpData.setDoctor(rs.getInt("D_Ref"));
                dpData.setDiagnosis(rs.getString("Diagnosis"));
                dpData.setPrescription(rs.getString("Prescription"));
            }
            return dpData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
