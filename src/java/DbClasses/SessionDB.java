/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.Session;
import DBConnector.DBConnection;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Wamuyu
 */
public class SessionDB {
    public int createSession(Session session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO session (D_Ref, Hospital, Patient, Type, Status) "
                + "VALUES (?, ?, ?, ?, ?)";
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session.getdRef());
            ps.setInt(2, session.getHospital());
            ps.setInt(3, session.getPatient());
            ps.setString(4, session.getType());
            ps.setString(5, session.getStatus());
            return ps.executeUpdate();
        } catch(SQLException e){
            System.out.println(e);
            return 0;
        }
    }
    
    public int checkSession(int patient, int hospital){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT COUNT(Session) "
                + "FROM session "
                + "WHERE Hospital=? "
                + "AND Patient=? "
                + "AND Exit_Date IS NULL";
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hospital);
            ps.setInt(2, patient);
            rs = ps.executeQuery();
            int count;
            if(rs.first()){
                count = rs.getInt("COUNT(Session)");
            } else{
                count = 999;
            }
            return count;
        } catch(SQLException e){
            System.out.println(e);
            return 999;
        }
    }
    
    public ArrayList<Session> patientsInHospital(int hp){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Session> session = new ArrayList<Session>();
        
        String query = "SELECT * FROM `session` "
                + "WHERE `Hospital` = ? "
                + "AND `Exit_Date` IS NULL";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hp);
            rs = ps.executeQuery();
            Session sess = null;
            while(rs.next()){
                sess = new Session();
                sess.setSession(rs.getInt("Session"));
                sess.setPatient(rs.getInt("Patient"));
                sess.setHospital(rs.getInt("Hospital"));
                sess.setDateEntry(rs.getDate("Entry_Date"));
                sess.setType(rs.getString("Type"));
                sess.setStatus(rs.getString("Status"));
                sess.setDateExit(rs.getDate("Exit_Date"));
                
                session.add(sess);
            }
            return session;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<Session> patientsClearedForChemist(int hp){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Session> session = new ArrayList<Session>();
        
        String query = "SELECT * FROM `session` "
                + "WHERE `Hospital` = ? "
                + "AND `Status` = 'Cleared' "
                + "AND `Exit_Date` IS NULL";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hp);
            rs = ps.executeQuery();
            Session sess = null;
            while(rs.next()){
                sess = new Session();
                sess.setSession(rs.getInt("Session"));
                sess.setPatient(rs.getInt("Patient"));
                sess.setHospital(rs.getInt("Hospital"));
                sess.setDateEntry(rs.getDate("Entry_Date"));
                sess.setType(rs.getString("Type"));
                sess.setStatus(rs.getString("Status"));
                sess.setDateExit(rs.getDate("Exit_Date"));
                
                session.add(sess);
            }
            return session;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public int changeStatus(String status, int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `session` SET `Status` = ? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, status);
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int changeType(String type, int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `session` SET `Type` = ? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, type);
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int endSession(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "UPDATE `session` SET `Type`=?, `Exit_Date`=now() WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, "Closed");
            ps.setInt(2, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public ArrayList<Session> getSessionThroStatus(String status, int hRef, int dRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Session> sessions = new ArrayList<>();
        String query = "SELECT * FROM `session` WHERE `Status`=? AND `Hospital`=? AND `D_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, status);
            ps.setInt(2, hRef);
            ps.setInt(3, dRef);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Session sess = new Session();
                sess.setSession(rs.getInt("Session"));
                sess.setPatient(rs.getInt("Patient"));
                sess.setHospital(rs.getInt("Hospital"));
                sess.setDateEntry(rs.getDate("Entry_Date"));
                sess.setType(rs.getString("Type"));
                sess.setStatus(rs.getString("Status"));
                sess.setDateExit(rs.getDate("Exit_Date"));
                
                sessions.add(sess);
            }
            return sessions;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<Session> getSessionThroStatus(String status, int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Session> sessions = new ArrayList<>();
        String query = "SELECT * FROM `session` WHERE `Status`=? AND `Hospital`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, status);
            ps.setInt(2, hRef);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Session sess = new Session();
                sess.setSession(rs.getInt("Session"));
                sess.setPatient(rs.getInt("Patient"));
                sess.setHospital(rs.getInt("Hospital"));
                sess.setDateEntry(rs.getDate("Entry_Date"));
                sess.setType(rs.getString("Type"));
                sess.setStatus(rs.getString("Status"));
                sess.setDateExit(rs.getDate("Exit_Date"));
                
                sessions.add(sess);
            }
            return sessions;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<Session> getSessionThroType(String type, int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Session> sessions = new ArrayList<>();
        String query = "SELECT * FROM `session` WHERE `Type`=? AND `Hospital`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, type);
            ps.setInt(2, hRef);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Session sess = new Session();
                sess.setSession(rs.getInt("Session"));
                sess.setPatient(rs.getInt("Patient"));
                sess.setHospital(rs.getInt("Hospital"));
                sess.setDateEntry(rs.getDate("Entry_Date"));
                sess.setType(rs.getString("Type"));
                sess.setStatus(rs.getString("Status"));
                sess.setDateExit(rs.getDate("Exit_Date"));
                
                sessions.add(sess);
            }
            return sessions;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public int timeInHospital(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SELECT now()-`Entry_Date` FROM session WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            if(rs.first()){
            return rs.getInt("now()-`Entry_Date`");
            } else{
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public String entryDate(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SELECT `Entry_Date` FROM `session` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            if(rs.first()){
            return rs.getString("Entry_Date");
            } else{
                return "Error";
            }
        } catch(SQLException e){
            e.printStackTrace();
            return "Error";
        }
    }
    
    public String getStatus(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Status` FROM `session` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            String status = "";
            if(rs.first()){
                status =  rs.getString("Status");
            }
            return status;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public String getType(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Status` FROM `session` WHERE `Type`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            String type = "";
            if(rs.first()){
                type =  rs.getString("Type");
            }
            return type;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
