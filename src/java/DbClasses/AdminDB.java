/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.AdminsAddStuffData;
import API.PasswordUtility;
import Bean.UsersData;
import DBConnector.DBConnection;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class AdminDB {
    public int addDoctor(AdminsAddStuffData aasData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `users`(`National_ID`, `Sir Name`,"
                + " `First Name`, `Other Names`, `DoB`,"
                + " `Type`, `Specialisation`, `Email`) "
                + "VALUES (?,?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, aasData.getNationalID());
            ps.setString(2, aasData.getSirName());
            ps.setString(3, aasData.getFirstName());
            ps.setString(4, aasData.getOtherNames());
            ps.setString(5, aasData.getDoB());
            ps.setString(6, "Doctor");
            ps.setString(7, aasData.getSpecialisation());
            ps.setString(8, aasData.getEmail());
//            ps.setString(9, aasData.getEmail());
            int rows = ps.executeUpdate();
            if(rows != 0){
                return this.savePassword(aasData.getEmail());
            } else{
                return rows;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    public int addNurse(AdminsAddStuffData aasData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `users`(`National_ID`, `Sir Name`,"
                + " `First Name`, `Other Names`, `DoB`,"
                + " `Type`, `Email`) "
                + "VALUES (?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, aasData.getNationalID());
            ps.setString(2, aasData.getSirName());
            ps.setString(3, aasData.getFirstName());
            ps.setString(4, aasData.getOtherNames());
            ps.setString(5, aasData.getDoB());
            ps.setString(6, "Nurse");
            ps.setString(7, aasData.getEmail());
//            ps.setString(8, aasData.getEmail());
            
            int rows = ps.executeUpdate();
            if(rows != 0){
                return this.savePassword(aasData.getEmail());
            } else{
                return rows;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    public int addLabTech(AdminsAddStuffData aasData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `users`(`National_ID`, `Sir Name`,"
                + " `First Name`, `Other Names`, `DoB`,"
                + " `Type`, `Email`) "
                + "VALUES (?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, aasData.getNationalID());
            ps.setString(2, aasData.getSirName());
            ps.setString(3, aasData.getFirstName());
            ps.setString(4, aasData.getOtherNames());
            ps.setString(5, aasData.getDoB());
            ps.setString(6, "LabTech");
            ps.setString(7, aasData.getEmail());
//            ps.setString(8, aasData.getEmail());
            
            int rows = ps.executeUpdate();
            if(rows != 0){
                return this.savePassword(aasData.getEmail());
            } else{
                return rows;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int employUser(int dRef, int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `physicians_hospitals`(`D_Ref`, `H_Ref`) "
                + "VALUES (?,?)";
        
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setInt(2, hRef);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int getDRef(String email){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref` FROM `users` WHERE `Email` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("D_Ref");
            } else{
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int searchNationalID(int nID){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `users` WHERE `National_ID` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, nID);
            rs = ps.executeQuery();
            if(rs.first()){
                return rs.getInt("COUNT(*)");
            } else{
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int employDoctor(AdminsAddStuffData aasData, int hRef){
        int exist = this.searchNationalID(aasData.getNationalID());
        int dRef;
        
        if(exist == 1){
            dRef = this.getDRef(aasData.getEmail());
            return this.employUser(dRef, hRef);
        } else if(exist == 0){
            int rows = this.addDoctor(aasData);
            if(rows != 0){
                dRef = this.getDRef(aasData.getEmail());
                int sRows = this.employUser(dRef, hRef);
                return sRows;
            } else{
                return 0;
            }
        } else{
            return 0;
        }
    }
    
    public int employNurse(AdminsAddStuffData aasData, int hRef){
        int exist = this.searchNationalID(aasData.getNationalID());
        int dRef;
        
        if(exist == 1){
            dRef = this.getDRef(aasData.getEmail());
            return this.employUser(dRef, hRef);
        } else if(exist == 0){
            int rows = this.addNurse(aasData);
            if(rows != 0){
                dRef = this.getDRef(aasData.getEmail());
                int sRows = this.employUser(dRef, hRef);
                return sRows;
            } else{
                return 0;
            }
        } else{
            return 0;
        }
    }
    public int employLabTech(AdminsAddStuffData aasData, int hRef){
        int exist = this.searchNationalID(aasData.getNationalID());
        int dRef;
        
        if(exist == 1){
            dRef = this.getDRef(aasData.getEmail());
            return this.employUser(dRef, hRef);
        } else if(exist == 0){
            int rows = this.addLabTech(aasData);
            if(rows != 0){
                dRef = this.getDRef(aasData.getEmail());
                int sRows = this.employUser(dRef, hRef);
                return sRows;
            } else{
                return 0;
            }
        } else{
            return 0;
        }
    }
    
    public int dismissUser(int dRef, int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "DELETE FROM `physicians_hospitals` "
                + "WHERE `D_Ref` = ? AND `H_Ref` = ?";
        
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setInt(2, hRef);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public ArrayList<UsersData> getUsers(String type, int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `users`.`D_Ref`, `users`.`National_ID`, `users`.`Sir Name`,"
                + " `users`.`First Name`, `users`.`Other Names`, `users`.`Specialisation`,"
                + " `users`.`Date` "
                + "FROM users "
                + "INNER JOIN physicians_hospitals "
                + "ON users.D_Ref=physicians_hospitals.D_Ref "
                + "WHERE physicians_hospitals.H_Ref=? "
                + "AND `users`.`Type`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hRef);
            ps.setString(2, type);
            rs = ps.executeQuery();
            
            ArrayList<UsersData> usArray = new ArrayList<>();
            UsersData usData;
            while(rs.next()){
                usData = new UsersData();
                usData.setdRef(rs.getInt("D_Ref"));
                usData.setNationalID(rs.getInt("National_ID"));
                usData.setSirName(rs.getString("Sir Name"));
                usData.setFirstname(rs.getString("First Name"));
                usData.setOtherNames(rs.getString("Other Names"));
                usData.setSpecialisation(rs.getString("Specialisation"));
                usData.setDate(rs.getDate("Date"));
                
                usArray.add(usData);
            }
            
            return usArray;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public int savePassword(String email){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "INSERT INTO `pass_file`(`User`, `Salt`, `Password`) VALUES (?,?,?)";
        
        PasswordUtility pwUtil = new PasswordUtility();
        
        int D_Ref = this.getDRef(email); //get userID
        String salt = pwUtil.getSalt(); //get salt
        String hashPassword;
        try{
            hashPassword = pwUtil.hashAndSaltPassword(email, salt); //get hashedPassword
            try{
                ps = connection.prepareStatement(query);
                ps.setInt(1, D_Ref);
                ps.setString(2, salt);
                ps.setString(3, hashPassword);

                return ps.executeUpdate();  //Store the password
            } catch(SQLException e){
                e.printStackTrace();
                return 0;
            }
        } catch(NoSuchAlgorithmException e){
            System.out.println(e);
            return 0;
        }
    }
}
