/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import DBConnector.DBConnection;
import java.sql.*;

/**
 *
 * @author Beast
 */
public class DashboardDB {
    public int getNumberOfHospitalPatients(int href){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `session` "
                + "WHERE `Hospital` = ? "
                + "AND `Exit_Date` IS null";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, href);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("COUNT(*)");
            } else
                return 0;
            
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int getNumberOfHospitalDoctors(int href){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `physicians_hospitals` "
                + "INNER JOIN `users` "
                + "ON `physicians_hospitals`.`D_Ref`=`users`.`D_Ref` "
                + "WHERE `users`.`Type` = \"Doctor\" "
                + "AND `physicians_hospitals`.`H_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, href);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("COUNT(*)");
            } else
                return 0;
            
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int getNumberOfHospitalNurses(int href){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `physicians_hospitals` "
                + "INNER JOIN `users` "
                + "ON `physicians_hospitals`.`D_Ref`=`users`.`D_Ref` "
                + "WHERE `users`.`Type` = \"Nurse\" "
                + "AND `physicians_hospitals`.`H_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, href);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("COUNT(*)");
            } else
                return 0;
            
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int getNumberOfHospitalLabTechs(int href){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `physicians_hospitals` "
                + "INNER JOIN `users` "
                + "ON `physicians_hospitals`.`D_Ref`=`users`.`D_Ref` "
                + "WHERE `users`.`Type` = \"LabTech\" "
                + "AND `physicians_hospitals`.`H_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, href);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("COUNT(*)");
            } else
                return 0;
            
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
}
