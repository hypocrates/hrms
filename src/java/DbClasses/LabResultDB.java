/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.CardicTests;
import Bean.CreatinineTests;
import Bean.ElectrolytesTests;
import Bean.GeneralTests;
import Bean.LipidProfileTests;
import Bean.LiverFunctionTests;
import Bean.ProstaticTests;
import Bean.ThyroidFunctionTests;
import DBConnector.DBConnection;
import java.sql.*;

/**
 *
 * @author Wamuyu
 */
public class LabResultDB {
    
    // Blood Work Save and Read
    
    public int addBloodWork(int session, int dRef, GeneralTests gTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `blood_data`(`Session`, `D_Ref`,"
                + " `Blood_Sugar`, `Urea`, `Uric_Acid`, `Calcium`,"
                + " `Acid_Phosphate`, `Cholestrol`, `Amylase`, `Phosphorus`, "
                + "`Total_Protein`,"
                + " `Albumen`, `Globulin`)"
                + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, gTests.getBloodSugar());
            ps.setString(4, gTests.getUrea());
            ps.setString(5, gTests.getUricAcid());
            ps.setString(6, gTests.getCalcium());
            ps.setString(7, gTests.getAcidPhosphate());
            ps.setString(8, gTests.getCholesterol());
            ps.setString(9, gTests.getAmylase());
            ps.setString(10, gTests.getPhosphorus());
            ps.setString(11, gTests.getTotalProtein());
            ps.setString(12, gTests.getAlbumen());
            ps.setString(13, gTests.getGlobulin());
            
            int rows = ps.executeUpdate();
            return rows;
        } catch(SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    public int checkBloodWork(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `blood_data` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return (count);
        } catch(SQLException e){
            e.printStackTrace();
            return 88;
        }
    }
    
    public int updateBloodWork(int session, int dRef, GeneralTests gTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `blood_data` SET `D_Ref`=?,`Blood_Sugar`=?,"
                + "`Urea`=?,`Uric_Acid`=?,`Calcium`=?,`Acid_Phosphate`=?,"
                + "`Cholestrol`=?,`Amylase`=?,`Phosphorus`=?,`Total_Protein`=?,"
                + "`Albumen`=?,`Globulin`=? "
                + "WHERE `Session` = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, gTests.getBloodSugar());
            ps.setString(3, gTests.getUrea());
            ps.setString(4, gTests.getUricAcid());
            ps.setString(5, gTests.getCalcium());
            ps.setString(6, gTests.getAcidPhosphate());
            ps.setString(7, gTests.getCholesterol());
            ps.setString(8, gTests.getAmylase());
            ps.setString(9, gTests.getPhosphorus());
            ps.setString(10, gTests.getTotalProtein());
            ps.setString(11, gTests.getAlbumen());
            ps.setString(12, gTests.getGlobulin());
            ps.setInt(13, session);
            
            int rows = ps.executeUpdate();
            return rows;
        } catch(SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    public int saveBloodWork(int session, int dRef, GeneralTests gTests){
        
        if(this.checkBloodWork(session)== 0){
            return (this.addBloodWork(session, dRef, gTests));
        } else {
            return this.updateBloodWork(session, dRef, gTests);
        }
    }
    
    public GeneralTests getBloodWork(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Session`, `D_Ref`, `Blood_Sugar`, `Urea`,"
                + " `Uric_Acid`, `Calcium`, `Acid_Phosphate`, `Cholestrol`,"
                + " `Amylase`, `Phosphorus`, `Total_Protein`, `Albumen`, `Globulin` "
                + "FROM `blood_data` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            if(rs.first()){
                GeneralTests gTests = new GeneralTests();
                gTests.setBloodSugar(rs.getString("Blood_Sugar"));
                gTests.setUrea(rs.getString("Urea"));
                gTests.setUricAcid(rs.getString("Uric_Acid"));
                gTests.setCalcium(rs.getString("Calcium"));
                gTests.setAcidPhosphate(rs.getString("Acid_Phosphate"));
                gTests.setCholesterol(rs.getString("Cholestrol"));
                gTests.setAmylase(rs.getString("Amylase"));
                gTests.setPhosphorus(rs.getString("Phosphorus"));
                gTests.setTotalProtein(rs.getString("Total_Protein"));
                gTests.setAlbumen(rs.getString("Albumen"));
                gTests.setGlobulin(rs.getString("Globulin"));
                return gTests;
            } else{
                GeneralTests gTests = null;
                return gTests;
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    
    //Electrolytes Save and Read
    
    public int saveElectrolytes(int session, int dRef, ElectrolytesTests eTests){
        if(this.checkElectrolytes(session)== 0){
            return (this.addElectrolytes(session, dRef, eTests));
        } else {
            return(this.updateElectrolytes(session, dRef, eTests));
        }
    }
    
    public int checkElectrolytes(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `electrolytes`"
                + " WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return (count);
        } catch(SQLException e){
            e.printStackTrace();
            return 88;
        }
    }
    
    public int addElectrolytes(int session, int dRef, ElectrolytesTests eTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `electrolytes`(`Session`, `D_Ref`,"
                + " `Sodium`, `Potassium`, `Chloride`, `Bicarbonate`)"
                + " VALUES (?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, eTests.getSodium());
            ps.setString(4, eTests.getPotassium());
            ps.setString(5, eTests.getChloride());
            ps.setString(6, eTests.getBicarbonate());
            int rows = ps.executeUpdate();
            return rows;
            
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateElectrolytes(int session, int dRef, ElectrolytesTests eTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `electrolytes` SET `D_Ref`=?,`Sodium`=?,"
                + "`Potassium`=?,`Chloride`=?,`Bicarbonate`=?"
                + " WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, eTests.getSodium());
            ps.setString(3, eTests.getPotassium());
            ps.setString(4, eTests.getChloride());
            ps.setString(5, eTests.getBicarbonate());
            ps.setInt(6, session);
            
            int rows = ps.executeUpdate();
            return (rows);
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }

    }
    
    public ElectrolytesTests getElectrolytesTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `Sodium`, `Potassium`, `Chloride`,"
                + " `Bicarbonate` "
                + "FROM `electrolytes` "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            if(rs.first()){
                ElectrolytesTests eTests = new ElectrolytesTests();
                eTests.setSodium(rs.getString("Sodium"));
                eTests.setChloride(rs.getString("Chloride"));
                eTests.setPotassium(rs.getString("Potassium"));
                eTests.setBicarbonate(rs.getString("Bicarbonate"));
                return eTests;
            } else{
                ElectrolytesTests eTests = null;
                return eTests;
            }
            
            
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    //LiverFunction Save and Read
    
    public int saveLiverTests(int session, int dRef, LiverFunctionTests lfTests){
        if(this.checkLiverTests(session) == 0){
            return this.addLiverTests(session, dRef, lfTests);
        } else
            return this.updateLiverTests(session, dRef, lfTests);
    }
    
    public int checkLiverTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `liver_function`"
                + " WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            
            return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 88;
        }
    }
    
    public int addLiverTests(int session, int dRef, LiverFunctionTests lfTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `liver_function`(`Session`, `D_Ref`, "
                + "`Bilirubin_(Total)`, `Bilirubin_(Direct)`, "
                + "`Bilirubin_(Indirect)`, `Alkaline_Phosphate`, "
                + "`Aminotransfcrase_(S.G.O.T)`, `Aminotransfcrase_(S.G.P.T)`,"
                + " `Y-Glutamy_Transfcrase_(Y-GT)`)"
                + " VALUES (?,?,?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, lfTests.getbTotal());
            ps.setString(4, lfTests.getbDirect());
            ps.setString(5, lfTests.getbIndirect());
            ps.setString(6, lfTests.getAlkaline());
            ps.setString(7, lfTests.getSGOT());
            ps.setString(8, lfTests.getSGPT());
            ps.setString(9, lfTests.getYGT());
            
            return ps.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateLiverTests(int session, int dRef, LiverFunctionTests lfTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `liver_function` SET `D_Ref`=?,"
                + "`Bilirubin_(Total)`=?,`Bilirubin_(Direct)`=?,"
                + "`Bilirubin_(Indirect)`=?,`Alkaline_Phosphate`=?,"
                + "`Aminotransfcrase_(S.G.O.T)`=?,"
                + "`Aminotransfcrase_(S.G.P.T)`=?,"
                + "`Y-Glutamy_Transfcrase_(Y-GT)`=? "
                + "WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, lfTests.getbTotal());
            ps.setString(3, lfTests.getbDirect());
            ps.setString(4, lfTests.getbIndirect());
            ps.setString(5, lfTests.getAlkaline());
            ps.setString(6, lfTests.getSGOT());
            ps.setString(7, lfTests.getSGPT());
            ps.setString(8, lfTests.getYGT());
            ps.setInt(9, session);
            
            return ps.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public LiverFunctionTests getLiverFunctionTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `Bilirubin_(Total)`, "
                + "`Bilirubin_(Direct)`, `Bilirubin_(Indirect)`, "
                + "`Alkaline_Phosphate`, `Aminotransfcrase_(S.G.O.T)`, "
                + "`Aminotransfcrase_(S.G.P.T)`, `Y-Glutamy_Transfcrase_(Y-GT)`"
                + " FROM `liver_function` "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            if(rs.first()){
                LiverFunctionTests lfTests = new LiverFunctionTests();
                lfTests.setbTotal(rs.getString("Bilirubin_(Total)"));
                lfTests.setbDirect(rs.getString("Bilirubin_(Direct)"));
                lfTests.setbIndirect(rs.getString("Bilirubin_(Indirect)"));
                lfTests.setAlkaline(rs.getString("Alkaline_Phosphate"));
                lfTests.setSGOT(rs.getString("Aminotransfcrase_(S.G.O.T)"));
                lfTests.setSGPT(rs.getString("Aminotransfcrase_(S.G.P.T)"));
                lfTests.setYGT(rs.getString("Y-Glutamy_Transfcrase_(Y-GT)"));
                return lfTests;
            } else{
                LiverFunctionTests lfTests = null;
                return lfTests;
            }
            
            
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    //Cardic Tests and Read
    
    public int saveCardicTests (int session, int dRef, CardicTests cTests){
        if(this.checkCardicTest(session) == 0){
            return this.addCardicTest(session, dRef, cTests);
        } else
            return this.updateCardicTest(session, dRef, cTests);
    }
    
    public int checkCardicTest (int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `cardic_enzymes` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 88;
        }
    }
    
    public int addCardicTest(int session, int dRef, CardicTests cTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `cardic_enzymes`(`Session`, `D_Ref`, "
                + "`LDH`, `HBD`, `CPK`, `S.G.O.T`) "
                + "VALUES (?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, cTests.getLDH());
            ps.setString(4, cTests.getHBD());
            ps.setString(5, cTests.getCPK());
            ps.setString(6, cTests.getSGOT());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateCardicTest(int session, int dRef, CardicTests cTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `cardic_enzymes` SET `D_Ref`=?,`LDH`=?,`HBD`=?,"
                + "`CPK`=?,`S.G.O.T`=?"
                + " WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, cTests.getLDH());
            ps.setString(3, cTests.getHBD());
            ps.setString(4, cTests.getCPK());
            ps.setString(5, cTests.getSGOT());
            ps.setInt(6, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public CardicTests getCardicTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `LDH`, `HBD`, `CPK`, `S.G.O.T` "
                + "FROM `cardic_enzymes` "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            if(rs.first()){
                CardicTests cTests = new CardicTests();
                cTests.setLDH(rs.getString("LDH"));
                cTests.setHBD(rs.getString("HBD"));
                cTests.setCPK(rs.getString("CPK"));
                cTests.setSGOT(rs.getString("S.G.O.T"));
                return cTests;
            } else{
                CardicTests cTests = null;
                return cTests;
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    //Creatinine Save and Read
    
    public int saveCreatinineTests(int session, int dRef, CreatinineTests crTests){
        if(this.checkCreatinineTests(session) == 0){
            return this.addCreatinineTests(session, dRef, crTests);
        } else
            return this.updateCreatinineTests(session, dRef, crTests);
    }
    
    public int checkCreatinineTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `creatinine` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return(count);
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addCreatinineTests(int session, int dRef, CreatinineTests crTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `creatinine`(`Session`, `D_Ref`,"
                + " `Creatinine_Clearance`, `Creatinine_(Serum)`, "
                + "`Creatinine_(Urine)`) "
                + "VALUES (?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, crTests.getClearance());
            ps.setString(4, crTests.getSerum());
            ps.setString(5, crTests.getUrine());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateCreatinineTests(int session, int dRef, CreatinineTests crTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `creatinine` SET `D_Ref`=?,"
                + "`Creatinine_Clearance`=?,`Creatinine_(Serum)`=?,"
                + "`Creatinine_(Urine)`=?"
                + " WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, crTests.getClearance());
            ps.setString(3, crTests.getSerum());
            ps.setString(4, crTests.getUrine());
            ps.setInt(5, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public CreatinineTests getCreatinineTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `Creatinine_Clearance`, "
                + "`Creatinine_(Serum)`, `Creatinine_(Urine)` "
                + "FROM `creatinine` "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            
            if(rs.first()){
                CreatinineTests crTests = new CreatinineTests();
                crTests.setClearance(rs.getString("Creatinine_Clearance"));
                crTests.setSerum(rs.getString("Creatinine_(Serum)"));
                crTests.setUrine(rs.getString("Creatinine_(Urine)"));
                return crTests;
            } else{
                CreatinineTests crTests = null;
                return crTests;
            }
             
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    //Lipid Profile save and Read
    
    public int saveLipidProfileTest(int session, int dRef, LipidProfileTests lpTests){
        if(this.checkLipidProfileTest(session) == 0){
            return this.addLipidProfileTest(session, dRef, lpTests);
        } else
            return this.updateLipidProfileTest(session, dRef, lpTests);
    }
    
    public int checkLipidProfileTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `lipid_profile` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int addLipidProfileTest(int session, int dRef, LipidProfileTests lpTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `lipid_profile`(`Session`, `D_Ref`, "
                + "`Total_Cholesterol`, `HDL_Cholesterol`, `LDL_Cholesterol`,"
                + " `Triglycerides`, `T.C/HDL_Ratio`) "
                + "VALUES (?,?,?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, lpTests.getTotalCholesterol());
            ps.setString(4, lpTests.getHdlCholesterol());
            ps.setString(5, lpTests.getLdlCholesterol());
            ps.setString(6, lpTests.getTriglycerides());
            ps.setString(7, lpTests.getTchdlRatio());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateLipidProfileTest(int session, int dRef, LipidProfileTests lpTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `lipid_profile` SET `D_Ref`=?,"
                + "`Total_Cholesterol`=?,`HDL_Cholesterol`=?,"
                + "`LDL_Cholesterol`=?,`Triglycerides`=?,"
                + "`T.C/HDL_Ratio`=? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, lpTests.getTotalCholesterol());
            ps.setString(3, lpTests.getHdlCholesterol());
            ps.setString(4, lpTests.getLdlCholesterol());
            ps.setString(5, lpTests.getTriglycerides());
            ps.setString(6, lpTests.getTchdlRatio());
            ps.setInt(7, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public LipidProfileTests getLipidProfileTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `Total_Cholesterol`, "
                + "`HDL_Cholesterol`, `LDL_Cholesterol`, `Triglycerides`, "
                + "`T.C/HDL_Ratio` "
                + "FROM `lipid_profile` "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            
            if(rs.first()){
                LipidProfileTests lpTests = new LipidProfileTests();
                lpTests.setTotalCholesterol(rs.getString("Total_Cholesterol"));
                lpTests.setHdlCholesterol(rs.getString("HDL_Cholesterol"));
                lpTests.setLdlCholesterol(rs.getString("LDL_Cholesterol"));
                lpTests.setTriglycerides(rs.getString("Triglycerides"));
                lpTests.setTchdlRatio(rs.getString("T.C/HDL_Ratio"));
                return lpTests;
            } else{
                LipidProfileTests lpTests = null;
                return lpTests;
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    // Prostatic Save and Read
    
    public int saveProstaticTests(int session, int dRef, ProstaticTests pTests){
        if(this.checkProstaticTests(session) == 0){
            return this.addProstaticTests(session, dRef, pTests);
        } else
            return this.updateProstaticTests(session, dRef, pTests);
    }
    
    public int checkProstaticTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `prostatic_data` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            return count;
        } catch (SQLException e){
            e.printStackTrace();
            return 88;
        }
    }
    
    public int addProstaticTests(int session, int dRef, ProstaticTests pTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `prostatic_data`(`Session`, `D_Ref`, "
                + "`P.S.A`) "
                + "VALUES (?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, pTests.getProstatic());
            
            return ps.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateProstaticTests(int session, int dRef, ProstaticTests pTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `prostatic_data` SET `D_Ref`=?,`P.S.A`=? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, pTests.getProstatic());
            ps.setInt(3, session);
            
            return ps.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public ProstaticTests getProstaticTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `P.S.A` FROM `prostatic_data` "
                + "WHERE `Session`= ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            if(rs.first()){
                ProstaticTests pTests = new ProstaticTests();
                pTests.setProstatic(rs.getString("P.S.A"));
                return pTests;
            } else{
                ProstaticTests pTests = null;
                return pTests;
            }
            
            
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    // thyroid test Save and Read
    
    public int saveThyroidTest(int session, int dRef, ThyroidFunctionTests tfTests){
        if(this.checkThyroidTest(session) == 0){
            return this.addThyroidTest(session, dRef, tfTests);
        } else
            return this.updateThyroidTest(session, dRef, tfTests);
    }
    
    public int checkThyroidTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `thyroid_function` WHERE `Session`= ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            int count = 0;
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
            
            return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 88;
        }
    }
    
    public int addThyroidTest(int session, int dRef, ThyroidFunctionTests tfTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `thyroid_function`(`Session`, `D_Ref`, "
                + "`TSH_Levels`, `Total_T3_Levels`, `Total_T4_Levels`) "
                + "VALUES (?,?,?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            ps.setInt(2, dRef);
            ps.setString(3, tfTests.getTshLevels());
            ps.setString(4, tfTests.getTotalT3Levels());
            ps.setString(5, tfTests.getTotalT4Levels());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int updateThyroidTest(int session, int dRef, ThyroidFunctionTests tfTests){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `thyroid_function` SET `D_Ref`=?,"
                + "`TSH_Levels`=?,`Total_T3_Levels`=?,`Total_T4_Levels`=? "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, tfTests.getTshLevels());
            ps.setString(3, tfTests.getTotalT3Levels());
            ps.setString(4, tfTests.getTotalT4Levels());
            ps.setInt(5, session);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public ThyroidFunctionTests getThyroidFunctionTests(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `TSH_Levels`, `Total_T3_Levels`,"
                + " `Total_T4_Levels` "
                + "FROM `thyroid_function` "
                + "WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            if(rs.first()){
                ThyroidFunctionTests tfTests = new ThyroidFunctionTests();
                tfTests.setTshLevels(rs.getString("TSH_Levels"));
                tfTests.setTotalT3Levels(rs.getString("Total_T3_Levels"));
                tfTests.setTotalT4Levels(rs.getString("Total_T4_Levels"));
                return tfTests;
            } else{
                ThyroidFunctionTests tfTests = null;
                return tfTests;
            }
            
            
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}