/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

/**
 *
 * @author Wamuyu
 */
import Bean.AssignDoctorData;
import Bean.BioData;
import Bean.ContactData;
import Bean.PatientData;
import DBConnector.DBConnection;
import java.sql.*;
import java.util.ArrayList;
public class SearchDB {
    public PatientData getPatientData(int NationalId){

        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        PatientData pData;
        
        String query
                = "SELECT * FROM patient_data "
                + "WHERE National_Id=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, NationalId);
            rs = ps.executeQuery();
            if(rs.first()){
                pData = new PatientData(rs.getInt("P_Ref"),
                        rs.getInt("National_ID"),
                        rs.getString("Sir_Name"),
                        rs.getString("First_Name"),
                        rs.getString("Middle_Name"),
                        rs.getString("Last_Name"));
                return pData;
            } else {
                return null;
            }
        } catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    public BioData getBioData(int pRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        BioData bioData;
        
        String query = "SELECT * FROM bio_data"
                + " WHERE P_Ref=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            rs = ps.executeQuery();
            
            if(rs.first()){
                System.out.println("Bio Data found");
                bioData = new BioData(rs.getInt("P_Ref"),
                    rs.getString("DoB"),
                    rs.getString("Gender"),
                    rs.getString("Marital_Status"),
                    rs.getInt("Children"),
                    rs.getString("Blood_Group"));
                return bioData;
            } else {
                System.out.println("Bio Data not found");
                return null;
            }
        }catch(SQLException e){
            System.out.println(e);
            return null;
            }
    }
    
    public ContactData getContactData(int pRef){
        Connection connection =DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ContactData cData;
        
        String query = "SELECT * FROM contact_data "
                + "WHERE P_Ref=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            rs = ps.executeQuery();
            
            if(rs.first()){
                System.out.println("Contact Data Found in Database");
                cData = new ContactData(rs.getInt("P_Ref"),
                rs.getString("Occupation"),
                rs.getString("County"),
                rs.getString("Residence"),
                rs.getString("Phone_Number"),
                rs.getString("Email"));
                return cData;
            } else {
                System.out.println("Contact Data not Found");
                return null;
            }
        } catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public PatientData getPatientName(int pRef){

        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        PatientData pData;
        
        String query
                = "SELECT * FROM patient_data "
                + "WHERE P_Ref=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            rs = ps.executeQuery();
            if(rs.first()){
                System.out.println("Patient Found found in DataBase");
                pData = new PatientData(rs.getInt("P_Ref"),
                        rs.getInt("National_ID"),
                        rs.getString("Sir_Name"),
                        rs.getString("First_Name"),
                        rs.getString("Middle_Name"),
                        rs.getString("Last_Name"));
                return pData;
            } else {
                System.out.println("No search reference number found in DB");
                return null;
            }
        } catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public ArrayList<AssignDoctorData> getDoctorsAssign(int hospital){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        PreparedStatement ps2;
        ResultSet rs2;
        
        String query = "SELECT `users`.`D_Ref`, `users`.`Sir Name`, "
                + "`users`.`First Name`, `users`.`Other Names`, "
                + "`users`.`Specialisation` "
                + "FROM `users` "
                + "INNER JOIN `physicians_hospitals` "
                + "ON `users`.`D_Ref`=`physicians_hospitals`.`D_Ref` "
                + "WHERE `physicians_hospitals`.`H_Ref` = ? "
                + "AND `users`.`Type` = 'Doctor'";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hospital);
            rs = ps.executeQuery();
            
            ArrayList<AssignDoctorData> adDataArray = new ArrayList<>();
            AssignDoctorData adData;
            while (rs.next()){
                adData = new AssignDoctorData();
                String name = rs.getString("Sir Name")+ " "+rs.getString("First Name");
                if(rs.getString("Other Names") != null){
                    name+= " "+rs.getString("Other Names");
                }
                int dRef = rs.getInt("D_Ref");
                adData.setdRef(dRef);
                adData.setName(name);
                adData.setSpecialisation(rs.getString("Specialisation"));
                String query2 = "SELECT COUNT(*) "
                        + "FROM `session` "
                        + "WHERE `D_Ref`=? "
                        + "AND `Status` = 'With the Doctor' "
                        + "OR `Status`  = 'Waiting to see the Doctor'";
                try {
                    ps2 = connection.prepareStatement(query2);
                    ps2.setInt(1, dRef);
                    rs2 = ps2.executeQuery();
                    if(rs2.first()){
                        adData.setNoPatients(rs2.getInt("COUNT(*)"));
                    }
                    
                } catch(SQLException e){
                    e.printStackTrace();
                    adData.setNoPatients(00);
                }
                System.out.println(adData.toString());
                adDataArray.add(adData);
                
            }
            
            return adDataArray;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
        
    }
}
