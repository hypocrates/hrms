/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.HospitalData;
import Bean.MedicalHistoryListData;
import Bean.PatientData;
import Bean.PrescriptionData;
import Bean.Session;
import DBConnector.DBConnection;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Wamuyu
 */
public class MedicalHistoryDB {
    public int checkExistence(int pRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT COUNT(*) FROM `session` WHERE `Patient` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            int count = 0;
            rs = ps.executeQuery();
            
            if(rs.first()){
                count = rs.getInt("COUNT(*)");
            }
             return count;
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int[] getSessions(int pRef, int length){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Session` FROM `session` WHERE `Patient` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            rs = ps.executeQuery();
            int [] session = new int[length];
            int counter = 0;
            while(rs.next()){
                session[counter] = rs.getInt("Session");
                counter += 1;
            }
            return session;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public PatientData getPatientName(int pRef){

        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        PatientData pData;
        
        String query
                = "SELECT * FROM patient_data "
                + "WHERE P_Ref=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            rs = ps.executeQuery();
            if(rs.first()){
                System.out.println("Patient Found found in DataBase");
                pData = new PatientData(rs.getInt("P_Ref"),
                        rs.getInt("National_ID"),
                        rs.getString("Sir_Name"),
                        rs.getString("First_Name"),
                        rs.getString("Middle_Name"),
                        rs.getString("Last_Name"));
                return pData;
            } else {
                System.out.println("No search reference number found in DB");
                return null;
            }
        } catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public Session getSession(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        String query = "SELECT * FROM `session` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            
            rs = ps.executeQuery();
            
            Session sess;
            if(rs.first()){
                sess = new Session();
                sess.setSession(rs.getInt("Session"));
                sess.setPatient(rs.getInt("Patient"));
                sess.setHospital(rs.getInt("Hospital"));
                sess.setDateEntry(rs.getDate("Entry_Date"));
                sess.setType(rs.getString("Type"));
                sess.setStatus(rs.getString("Status"));
                sess.setDateExit(rs.getDate("Exit_Date"));
                
            } else{
                sess = null;
            }
                return sess;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public String getHospitalName(int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Name` FROM `hospitals` WHERE `H_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hRef);
            rs = ps.executeQuery();
            String hospitalName = "";
            if(rs.first()){
                hospitalName = rs.getString("Name");
            }
            
            return hospitalName;
        } catch(SQLException e){
            e.printStackTrace();
            return "";
        }
    }
    
    public PrescriptionData getPrescriptionDiag(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref`, `Diagnosis`, `Prescription` FROM `diagnosis` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            PrescriptionData dpData = new PrescriptionData();
            if(rs.first()){
                dpData.setDoctor(rs.getInt("D_Ref"));
                dpData.setDiagnosis(rs.getString("Diagnosis"));
                dpData.setPrescription(rs.getString("Prescription"));
            }
            return dpData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public String getChiefComplains(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Chief_Complains` "
                + "FROM `chief_complains` WHERE `Session` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            String chiefComplains = "";
            if(rs.first()){
                chiefComplains = rs.getString("Chief_Complains");
            }
            return chiefComplains;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public HospitalData getHospital(int hRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT * FROM `hospitals` WHERE `H_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, hRef);
            rs = ps.executeQuery();
            HospitalData hData;
            if(rs.first()){
                hData = new HospitalData();
                hData.sethRef(rs.getInt("H_Ref"));
                hData.setName(rs.getString("Name"));
                hData.setLocation(rs.getString("Location"));
                hData.setSize(rs.getString("Size"));
                hData.setAddress(rs.getString("Address"));
            } else{
                hData = null;
            }
            return hData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public String getDoctor(int dRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Name` FROM `users` WHERE `D_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            rs = ps.executeQuery();
            String doctorName = "";
            if(rs.first()){
                doctorName = rs.getString("Name");
            }
            
            return doctorName;
        } catch(SQLException e){
            e.printStackTrace();
            return "";
        }
    }
}
