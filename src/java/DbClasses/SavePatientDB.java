/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.PatientData;
import Bean.BioData;
import Bean.ContactData;
import DBConnector.DBConnection;
import java.sql.*;

/**
 *
 * @author Wamuyu
 */
public class SavePatientDB {
    public int savePersonalDetails(PatientData pData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        PreparedStatement p = null;
        ResultSet rs = null;
        
        String query = "INSERT INTO patient_data(`National_ID`, `Sir_Name`, `First_Name`, `Middle_Name`, `Last_Name`) "
                + "VALUES (?, ?, ?, ?, ?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pData.getNationalID());
            ps.setString(2, pData.getSirName());
            ps.setString(3, pData.getFirstName());
            ps.setString(4, pData.getMiddleName());
            ps.setString(5, pData.getLastName());
            
            int rows = ps.executeUpdate();
            if(rows !=0){
                String q = "SELECT `P_Ref` FROM `patient_data` WHERE `National_ID`=?";
                try{
                    p = connection.prepareStatement(q);
                    p.setInt(1, pData.getNationalID());
                    rs = p.executeQuery();
                    if(rs.first()){
                        return rs.getInt("P_Ref");
                    } else{
                        return 0;
                    }
                } catch(SQLException e){
                    System.out.println(e);
                    return 0;
                }
            } else {
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int saveBioData(int pRef, BioData bioData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `bio_data`(`P_Ref`, `DoB`, `Gender`, `Marital_Status`, `Children`, `Blood_Group`) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            ps.setString(2, bioData.getDob());
            ps.setString(3, bioData.getGender());
            ps.setString(4, bioData.getMaritalStatus());
            ps.setInt(5, bioData.getChildren());
            ps.setString(6, bioData.getBloodGroup());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int saveContactData(int pRef, ContactData cData){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        
        String query = "INSERT INTO `contact_data`(`P_Ref`, `Occupation`, `County`, `Residence`, `Phone_Number`, `Email`) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, pRef);
            ps.setString(2, cData.getOccupation());
            ps.setString(3, cData.getCounty());
            ps.setString(4, cData.getResidence());
            ps.setString(5, cData.getPhoneNumber());
            ps.setString(6, cData.getEmail());
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
}
