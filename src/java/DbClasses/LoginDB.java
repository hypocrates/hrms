/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DbClasses;

import Bean.HospitalData;
import Bean.Login;
import DBConnector.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Wamuyu
 */
public class LoginDB {
    public String AuthenticateUser(Login login)
    {
        int dRef = login.getDRef();
        String password = login.getPassword();
        
        Connection con = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String sql = "SELECT `Password` FROM `pass_file` WHERE `User`=?";
//        int Db_dRef = 0;
        String Db_password = "";
        
        try {
            
            ps = con.prepareStatement(sql);
            ps.setInt(1, dRef);
            rs = ps.executeQuery();
            
            if(rs.first())
            {
                Db_password = rs.getString("Password");
                
                if(password.equals(Db_password))
                {
                    return "SUCCESS";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return "Invalid user credentials"; 
    }
    
    public String getType(String email){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Type` FROM `users` WHERE `Email` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getString("Type");
            } else{
                return null;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public int getDRef(String email){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `D_Ref` FROM `users` WHERE `Email` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("D_Ref");
            } else{
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public String getName(String email){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Sir Name`, `First Name` FROM `users` WHERE `Email` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            if(rs.first()){
                String sirName = rs.getString("Sir Name");
                String firstName = rs.getString("First Name");
                String name = sirName+" "+firstName;
                return name;
            } else{
                return null;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<HospitalData> getHospital(String email){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT hospitals.H_Ref, hospitals.Name, hospitals.Location " +
                        "FROM hospitals " +
                        "INNER JOIN " +
                        "    (SELECT physicians_hospitals.H_Ref " +
                        "	FROM physicians_hospitals " +
                        "	INNER JOIN " +
                        "     	(SELECT users.D_Ref " +
                        "     	FROM users " +
                        "     	WHERE users.Email = ?) dRef" +
                        "     ON dRef.D_Ref = physicians_hospitals.D_Ref) href\n" +
                        "ON href.H_Ref = hospitals.H_Ref";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            ArrayList<HospitalData> hpArray = new ArrayList<>();
            HospitalData hpData;
            while(rs.next()){
                hpData = new HospitalData();
                hpData.sethRef(rs.getInt("H_Ref"));
                hpData.setName(rs.getString("Name"));
                hpData.setLocation(rs.getString("Location"));
                
                hpArray.add(hpData);
            }
            
            return hpArray;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public int getPasswordCheck(int dRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `PasswordCheck` FROM `users` WHERE `D_Ref` = ?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getInt("PasswordCheck");
            } else{
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int changePasswordChecker(int dRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `users` SET `PasswordCheck`=? "
                + "WHERE `D_Ref`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, 1);
            ps.setInt(2, dRef);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int changePassword(int dRef, String salt, String pw){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "UPDATE `pass_file` SET `Salt`=?,`Password`=? "
                + "WHERE `User`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setString(1, salt);
            ps.setString(2, pw);
            ps.setInt(3, dRef);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public int createPassword(int dRef, String salt, String pw){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        
        String query = "INSERT INTO `pass_file`(`User`, `Salt`, `Password`) "
                + "VALUES (?,?,?)";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            ps.setString(2, salt);
            ps.setString(3, pw);
            
            return ps.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    public String getPassword(int dRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Password` FROM `pass_file` WHERE `User`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getString("Password");
            } else{
                return null;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public String getSalt(int dRef){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps;
        ResultSet rs;
        
        String query = "SELECT `Salt` FROM `pass_file` WHERE `User`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, dRef);
            
            rs = ps.executeQuery();
            
            if(rs.first()){
                return rs.getString("Salt");
            } else{
                return null;
            }
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
