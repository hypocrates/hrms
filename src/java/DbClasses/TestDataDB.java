package DbClasses;

import Bean.CardicTestData;
import Bean.CreatinineTestData;
import Bean.ElectrolytesTestData;
import Bean.GeneralTestData;
import Bean.LiverTestData;
import Bean.OtherTestData;
import DBConnector.DBConnection;
import java.sql.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wamuyu
 */
public class TestDataDB {
    public CardicTestData getCardicTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `LDH`, `HBD`, `CPK`, `SGOT` FROM `cardic_tests` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            CardicTestData ctData = new CardicTestData();
            if(rs.first()){
                ctData.setSession(session);
                ctData.setLdh(rs.getInt("LDH"));
                ctData.setHbd(rs.getInt("HBD"));
                ctData.setCpk(rs.getInt("CPK"));
                ctData.setSgot(rs.getInt("SGOT"));
            }
            return ctData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public CreatinineTestData getCreatinineTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Clearance`, `Serum`, `Urine` FROM `creatinine_tests` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            CreatinineTestData crData = new CreatinineTestData();
            if(rs.first()){
                crData.setSession(session);
                crData.setSerum(rs.getInt("Clearance"));
                crData.setSerum(rs.getInt("Serum"));
                crData.setUrine(rs.getInt("Urine"));
            }
            return crData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public ElectrolytesTestData getElectrolytesTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Sodium`, `Potassium`, `Chloride`, `Bicarbonate` FROM `electrolytes_tests` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            ElectrolytesTestData etData = new ElectrolytesTestData();
            if(rs.first()){
                etData.setSession(session);
                etData.setSodium(rs.getInt("Sodium"));
                etData.setPotassium(rs.getInt("Potassium"));
                etData.setChloride(rs.getInt("Chloride"));
                etData.setBicarbonate(rs.getInt("Bicarbonate"));
            }
            return etData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public GeneralTestData getGeneralTest(int session){
        Connection connection  = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Blood Sugar`, `Urea`, `Uric Acid`, `Calcium`,"
                + " `Acid Phosphate`, `Cholesterol`, `Amylase`, `Phosphorus`,"
                + " `Total Protein`, `Albumen`, `Globulin` "
                + "FROM `general_tests` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            GeneralTestData gtData = new GeneralTestData();
            if(rs.first()){
                gtData.setSession(session);
                gtData.setbSugar(rs.getInt("Blood Sugar"));
                gtData.setUrea(rs.getInt("Urea"));
                gtData.setUricAcid(rs.getInt("Uric Acid"));
                gtData.setCalcium(rs.getInt("Calcium"));
                gtData.setAcidPhospate(rs.getInt("Acid Phosphate"));
                gtData.setCholestrol(rs.getInt("Cholesterol"));
                gtData.setAmylase(rs.getInt("Amylase"));
                gtData.setPhosphorus(rs.getInt("Phosphorus"));
                gtData.setTotalProtein(rs.getInt("Total Protein"));
                gtData.setAlbumen(rs.getInt("Albumen"));
                gtData.setGlobulin(rs.getInt("Globulin"));
            }
            
            return gtData;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public LiverTestData getLiverTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `BTotal`, `BDirect`, `BIndirect`,"
                + " `AlkalinePhosphate`, `SGOT`, `SGPT`, `YGT` "
                + "FROM `liver_tests` WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            LiverTestData ltData = new LiverTestData();
            if(rs.first()){
                ltData.setSession(session);
                ltData.setbTotal(rs.getInt("BTotal"));
                ltData.setbDirect(rs.getInt("BDirect"));
                ltData.setbIndirect(rs.getInt("BIndirect"));
                ltData.setAlkalinePhosphate(rs.getInt("AlkalinePhosphate"));
                ltData.setSgot(rs.getInt("SGOT"));
                ltData.setSgpt(rs.getInt("SGPT"));
                ltData.setYgt(rs.getInt("YGT"));
            }
            
            return ltData;
            
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public OtherTestData getOtherTest(int session){
        Connection connection = DBConnection.createConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT `Serum`, `Plasma`, `Lipid Profile`,"
                + " `Thyroid Function`, `PSA`, `NormalTest` FROM `other_tests` "
                + "WHERE `Session`=?";
        
        try{
            ps = connection.prepareStatement(query);
            ps.setInt(1, session);
            rs = ps.executeQuery();
            
            OtherTestData otData = new OtherTestData();
            if(rs.first()){
                otData.setSession(session);
                otData.setSerum(rs.getInt("Serum"));
                otData.setPlasma(rs.getInt("Plasma"));
                otData.setLipidProfile(rs.getInt("Lipid Profile"));
                otData.setThyroidFunction(rs.getInt("Thyroid Function"));
                otData.setPsa(rs.getInt("PSA"));
                otData.setNormalTest(rs.getInt("NormalTest"));
            }
            return otData;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
