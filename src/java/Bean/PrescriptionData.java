/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class PrescriptionData implements Serializable{
    int dpRef;
    int session;
    int doctor;
    String prescription;
    String diagnosis;

    public PrescriptionData() {
        dpRef = 0;
        session = 0;
        doctor = 0;
        prescription = "";
        diagnosis = "";
    }

    public PrescriptionData(int dpRef, int session, int doctor, String prescription, String diagnosis) {
        this.dpRef = dpRef;
        this.session = session;
        this.doctor = doctor;
        this.prescription = prescription;
        this.diagnosis = diagnosis;
    }

    public int getDpRef() {
        return dpRef;
    }

    public void setDpRef(int dpRef) {
        this.dpRef = dpRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    
    
    
}
