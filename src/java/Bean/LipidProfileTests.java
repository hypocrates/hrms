/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class LipidProfileTests implements Serializable{
    String totalCholesterol;
    String hdlCholesterol;
    String ldlCholesterol;
    String triglycerides;
    String tchdlRatio;
    
    public LipidProfileTests(){
        totalCholesterol = "NULL";
        hdlCholesterol = "NULL";
        ldlCholesterol= "NULL";
        triglycerides = "NULL";
        tchdlRatio = "NULL";
    }

    public LipidProfileTests(String totalCholesterol, String hdlCholesterol, String ldlCholesterol, String triglycerides, String tchdlRatio) {
        this.totalCholesterol = totalCholesterol;
        this.hdlCholesterol = hdlCholesterol;
        this.ldlCholesterol = ldlCholesterol;
        this.triglycerides = triglycerides;
        this.tchdlRatio = tchdlRatio;
    }

    public String getTotalCholesterol() {
        return totalCholesterol;
    }

    public void setTotalCholesterol(String totalCholesterol) {
        this.totalCholesterol = totalCholesterol;
    }

    public String getHdlCholesterol() {
        return hdlCholesterol;
    }

    public void setHdlCholesterol(String hdlCholesterol) {
        this.hdlCholesterol = hdlCholesterol;
    }

    public String getLdlCholesterol() {
        return ldlCholesterol;
    }

    public void setLdlCholesterol(String ldlCholesterol) {
        this.ldlCholesterol = ldlCholesterol;
    }

    public String getTriglycerides() {
        return triglycerides;
    }

    public void setTriglycerides(String triglycerides) {
        this.triglycerides = triglycerides;
    }

    public String getTchdlRatio() {
        return tchdlRatio;
    }

    public void setTchdlRatio(String tchdlRatio) {
        this.tchdlRatio = tchdlRatio;
    }
}
