/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class LabPatientListData implements Serializable{
    int pRef;
    int nationalID;
    String name;
    String Status;
    int session;
    int general;
    int lipid;
    int psa;
    int thyroid;

    public LabPatientListData() {
        pRef = 0;
        nationalID = 0;
        name = "";
        Status = "";
        session = 0;
        general = 0;
        lipid = 0;
        psa = 0;
        thyroid = 0;
    }

    public LabPatientListData(int pRef, int nationalID, String name, String Status, int session, int general, int lipid, int psa, int thyroid) {
        this.pRef = pRef;
        this.nationalID = nationalID;
        this.name = name;
        this.Status = Status;
        this.session = session;
        this.general = general;
        this.lipid = lipid;
        this.psa = psa;
        this.thyroid = thyroid;
    }

    public int getpRef() {
        return pRef;
    }

    public void setpRef(int pRef) {
        this.pRef = pRef;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getGeneral() {
        return general;
    }

    public void setGeneral(int general) {
        this.general = general;
    }

    public int getLipid() {
        return lipid;
    }

    public void setLipid(int lipid) {
        this.lipid = lipid;
    }

    public int getPsa() {
        return psa;
    }

    public void setPsa(int psa) {
        this.psa = psa;
    }

    public int getThyroid() {
        return thyroid;
    }

    public void setThyroid(int thyroid) {
        this.thyroid = thyroid;
    }
}
