/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class OtherTests implements Serializable{
    int serum;
    int plasma;
    int lipid;
    int thyroid;
    int PSA;
    int normalTest;

    public OtherTests() {
        serum = 0;
        plasma = 0;
        lipid = 0;
        thyroid = 0;
        PSA = 0;
        normalTest = 0;
    }

    public OtherTests(int serum, int plasma, int lipid, int thyroid, int PSA, int normalTest) {
        this.serum = serum;
        this.plasma = plasma;
        this.lipid = lipid;
        this.thyroid = thyroid;
        this.PSA = PSA;
        this.normalTest = normalTest;
    }

    public int getSerum() {
        return serum;
    }

    public void setSerum(int serum) {
        this.serum = serum;
    }

    public int getPlasma() {
        return plasma;
    }

    public void setPlasma(int plasma) {
        this.plasma = plasma;
    }

    public int getLipid() {
        return lipid;
    }

    public void setLipid(int lipid) {
        this.lipid = lipid;
    }

    public int getThyroid() {
        return thyroid;
    }

    public void setThyroid(int thyroid) {
        this.thyroid = thyroid;
    }

    public int getPSA() {
        return PSA;
    }

    public void setPSA(int PSA) {
        this.PSA = PSA;
    }

    public int getNormalTest() {
        return normalTest;
    }

    public void setNormalTest(int normalTest) {
        this.normalTest = normalTest;
    }
    
    
}
