/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class CreatinineTestData implements Serializable{
    int crRef;
    int session;
    int clearance;
    int serum;
    int urine;

    public CreatinineTestData() {
        crRef = 0;
        session = 0;
        clearance = 0;
        serum = 0;
        urine = 0;
    }

    public CreatinineTestData(int crRef, int session, int clearance, int serum, int urine) {
        this.crRef = crRef;
        this.session = session;
        this.clearance = clearance;
        this.serum = serum;
        this.urine = urine;
    }

    public int getCrRef() {
        return crRef;
    }

    public void setCrRef(int crRef) {
        this.crRef = crRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getClearance() {
        return clearance;
    }

    public void setClearance(int clearance) {
        this.clearance = clearance;
    }

    public int getSerum() {
        return serum;
    }

    public void setSerum(int serum) {
        this.serum = serum;
    }

    public int getUrine() {
        return urine;
    }

    public void setUrine(int urine) {
        this.urine = urine;
    }
    
}
