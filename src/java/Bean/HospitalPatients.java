/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class HospitalPatients implements Serializable {
    int nationalID;
    String name;
    String status;
    String category;
    String action;
    
    public HospitalPatients(){
        nationalID = 0;
        name = "";
        status = "";
        category = "";
        action = "";
    }

    public HospitalPatients(int nationalID, String name, String status, String category, String action) {
        this.nationalID = nationalID;
        this.name = name;
        this.status = status;
        this.category = category;
        this.action = action;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
    
    
}
