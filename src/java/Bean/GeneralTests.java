/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class GeneralTests implements Serializable{
    
    String bloodSugar;
    String urea;
    String uricAcid;
    String calcium;
    String acidPhosphate;
    String cholesterol;
    String amylase;
    String phosphorus;
    String totalProtein;
    String albumen;
    String globulin;
    
    public GeneralTests(){
        bloodSugar = "NULL";
        urea = "NULL";
        uricAcid = "NULL";
        calcium = "NULL";
        acidPhosphate = "NULL";
        cholesterol = "NULL";
        amylase = "NULL";
        phosphorus = "NULL";
        totalProtein = "NULL";
        albumen = "NULL";
        globulin = "NULL";
    }

    public GeneralTests(String bloodSugar, String urea, String uricAcid, String calcium, String acidPhosphate, String cholesterol, String amylase, String phosphorus, String totalProtein, String albumen, String globulin) {
        this.bloodSugar = bloodSugar;
        this.urea = urea;
        this.uricAcid = uricAcid;
        this.calcium = calcium;
        this.acidPhosphate = acidPhosphate;
        this.cholesterol = cholesterol;
        this.amylase = amylase;
        this.phosphorus = phosphorus;
        this.totalProtein = totalProtein;
        this.albumen = albumen;
        this.globulin = globulin;
    }

    public String getBloodSugar() {
        return bloodSugar;
    }

    public void setBloodSugar(String bloodSugar) {
        this.bloodSugar = bloodSugar;
    }

    public String getUrea() {
        return urea;
    }

    public void setUrea(String urea) {
        this.urea = urea;
    }

    public String getUricAcid() {
        return uricAcid;
    }

    public void setUricAcid(String uricAcid) {
        this.uricAcid = uricAcid;
    }

    public String getCalcium() {
        return calcium;
    }

    public void setCalcium(String calcium) {
        this.calcium = calcium;
    }

    public String getAcidPhosphate() {
        return acidPhosphate;
    }

    public void setAcidPhosphate(String acidPhosphate) {
        this.acidPhosphate = acidPhosphate;
    }

    public String getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(String cholesterol) {
        this.cholesterol = cholesterol;
    }

    public String getAmylase() {
        return amylase;
    }

    public void setAmylase(String amylase) {
        this.amylase = amylase;
    }

    public String getPhosphorus() {
        return phosphorus;
    }

    public void setPhosphorus(String phosphorus) {
        this.phosphorus = phosphorus;
    }

    public String getTotalProtein() {
        return totalProtein;
    }

    public void setTotalProtein(String totalProtein) {
        this.totalProtein = totalProtein;
    }

    public String getAlbumen() {
        return albumen;
    }

    public void setAlbumen(String albumen) {
        this.albumen = albumen;
    }

    public String getGlobulin() {
        return globulin;
    }

    public void setGlobulin(String globulin) {
        this.globulin = globulin;
    }
    
    
}
