/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class AssignDoctorData implements Serializable{
    int dRef;
    String name;
    String specialisation;
    int noPatients;

    public AssignDoctorData() {
        dRef = 0;
        name = "";
        specialisation = "";
        noPatients = 0;
    }

    public AssignDoctorData(int dRef, String name, String specialisation, int noPatients) {
        this.dRef = dRef;
        this.name = name;
        this.specialisation = specialisation;
        this.noPatients = noPatients;
    }

    public int getdRef() {
        return dRef;
    }

    public void setdRef(int dRef) {
        this.dRef = dRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    public int getNoPatients() {
        return noPatients;
    }

    public void setNoPatients(int noPatients) {
        this.noPatients = noPatients;
    }

    @Override
    public String toString() {
        return "AssignDoctorData{" + "dRef=" + dRef + ", name=" + name + ", specialisation=" + specialisation + ", noPatients=" + noPatients + '}';
    }
    
}
