/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class ChiefComplainsData implements Serializable{
    int ccRef;
    int session;
    int doctor;
    int hospital;
    String diagnosis;
    String chiefComplains;
    String bMI;
    String bloodPressure;
    
    public ChiefComplainsData(){
        ccRef = 0;
        session = 0;
        doctor = 0;
        hospital = 0;
        diagnosis = "";
        chiefComplains = "";
        bMI = "";
        bloodPressure = "";
    }

    public ChiefComplainsData(int ccRef, int session, int doctor, int hospital, String diagnosis, String chiefComplains, String bMI, String bloodPressure) {
        this.ccRef = ccRef;
        this.session = session;
        this.doctor = doctor;
        this.hospital = hospital;
        this.diagnosis = diagnosis;
        this.chiefComplains = chiefComplains;
        this.bMI = bMI;
        this.bloodPressure = bloodPressure;
    }

    public int getCcRef() {
        return ccRef;
    }

    public void setCcRef(int ccRef) {
        this.ccRef = ccRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public int getHospital() {
        return hospital;
    }

    public void setHospital(int hospital) {
        this.hospital = hospital;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getChiefComplains() {
        return chiefComplains;
    }

    public void setChiefComplains(String chiefComplains) {
        this.chiefComplains = chiefComplains;
    }

    public String getbMI() {
        return bMI;
    }

    public void setbMI(String bMI) {
        this.bMI = bMI;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }
    
}
