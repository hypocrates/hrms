/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;
/**
 *
 * @author Wamuyu
 */

public class BioData implements Serializable{
    int pRef;
    String dob;
    String gender;
    String maritalStatus;
    int children;
    String bloodGroup;
    
    public BioData(){
        pRef=0;
        dob= "1900/12/15";
        gender="";
        maritalStatus="";
        children=0;
        bloodGroup="";
    }

    public BioData(int pRef, String dob, String gender, String maritalStatus, int children, String bloodGroup) {
        this.pRef = pRef;
        this.dob = dob;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.children = children;
        this.bloodGroup = bloodGroup;
    }

    public int getpRef() {
        return pRef;
    }

    public void setpRef(int pRef) {
        this.pRef = pRef;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }
    
    }
