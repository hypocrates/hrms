/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class CardicTestData implements Serializable{
    int ctRef;
    int session;
    int ldh;
    int hbd;
    int cpk;
    int sgot;

    public CardicTestData() {
        ctRef = 0;
        session =0;
        ldh = 0;
        hbd = 0;
        cpk = 0;
        sgot = 0;
    }

    public CardicTestData(int ctRef, int session, int ldh, int hbd, int cpk, int sgot) {
        this.ctRef = ctRef;
        this.session = session;
        this.ldh = ldh;
        this.hbd = hbd;
        this.cpk = cpk;
        this.sgot = sgot;
    }

    public int getCtRef() {
        return ctRef;
    }

    public void setCtRef(int ctRef) {
        this.ctRef = ctRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getLdh() {
        return ldh;
    }

    public void setLdh(int ldh) {
        this.ldh = ldh;
    }

    public int getHbd() {
        return hbd;
    }

    public void setHbd(int hbd) {
        this.hbd = hbd;
    }

    public int getCpk() {
        return cpk;
    }

    public void setCpk(int cpk) {
        this.cpk = cpk;
    }

    public int getSgot() {
        return sgot;
    }

    public void setSgot(int sgot) {
        this.sgot = sgot;
    }
    
}
