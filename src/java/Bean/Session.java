/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Wamuyu
 */
public class Session implements Serializable{
    int session;
    int dRef;
    int patient;
    int hospital;
    Date dateEntry;
    String type;
    String status;
    Date dateExit;
    
    public Session(){
        session = 0;
        dRef = 0;
        patient = 0;
        hospital = 0;
        dateEntry = new Date(0000,00,00);
        type = "";
        status = "";
        dateExit = new Date(0000,00,00);
    }


    public Session(int session, int dRef, int patient, int hospital, Date dateEntry, String type, String status, Date dateExit) {
        this.session = session;
        this.dRef = dRef;
        this.patient = patient;
        this.hospital = hospital;
        this.dateEntry = dateEntry;
        this.type = type;
        this.status = status;
        this.dateExit = dateExit;
    }

    public int getdRef() {
        return dRef;
    }

    public void setdRef(int dRef) {
        this.dRef = dRef;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getPatient() {
        return patient;
    }

    public void setPatient(int patient) {
        this.patient = patient;
    }

    public int getHospital() {
        return hospital;
    }

    public void setHospital(int hospital) {
        this.hospital = hospital;
    }

    public Date getDateEntry() {
        return dateEntry;
    }

    public void setDateEntry(Date dateEntry) {
        this.dateEntry = dateEntry;
    }

    public Date getDateExit() {
        return dateExit;
    }

    public void setDateExit(Date dateExit) {
        this.dateExit = dateExit;
    }

    
    
}
