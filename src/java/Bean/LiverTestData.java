/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class LiverTestData implements Serializable{
    int lRef;
    int session;
    int bTotal;
    int bDirect;
    int bIndirect;
    int alkalinePhosphate;
    int sgot;
    int sgpt;
    int ygt;

    public LiverTestData() {
        lRef = 0;
        session = 0;
        bTotal = 0;
        bDirect = 0;
        bIndirect = 0;
        alkalinePhosphate = 0;
        sgot = 0;
        sgpt = 0;
        ygt = 0;
    }

    public LiverTestData(int lRef, int session, int bTotal, int bDirect, int bIndirect, int alkalinePhosphate, int sgot, int sgpt, int ygt) {
        this.lRef = lRef;
        this.session = session;
        this.bTotal = bTotal;
        this.bDirect = bDirect;
        this.bIndirect = bIndirect;
        this.alkalinePhosphate = alkalinePhosphate;
        this.sgot = sgot;
        this.sgpt = sgpt;
        this.ygt = ygt;
    }

    public int getlRef() {
        return lRef;
    }

    public void setlRef(int lRef) {
        this.lRef = lRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getbTotal() {
        return bTotal;
    }

    public void setbTotal(int bTotal) {
        this.bTotal = bTotal;
    }

    public int getbDirect() {
        return bDirect;
    }

    public void setbDirect(int bDirect) {
        this.bDirect = bDirect;
    }

    public int getbIndirect() {
        return bIndirect;
    }

    public void setbIndirect(int bIndirect) {
        this.bIndirect = bIndirect;
    }

    public int getAlkalinePhosphate() {
        return alkalinePhosphate;
    }

    public void setAlkalinePhosphate(int alkalinePhosphate) {
        this.alkalinePhosphate = alkalinePhosphate;
    }

    public int getSgot() {
        return sgot;
    }

    public void setSgot(int sgot) {
        this.sgot = sgot;
    }

    public int getSgpt() {
        return sgpt;
    }

    public void setSgpt(int sgpt) {
        this.sgpt = sgpt;
    }

    public int getYgt() {
        return ygt;
    }

    public void setYgt(int ygt) {
        this.ygt = ygt;
    }
}
