/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class GeneralTestData implements Serializable{
    int gRef;
    int session;
    int bSugar;
    int urea;
    int uricAcid;
    int calcium;
    int acidPhospate;
    int cholestrol;
    int amylase;
    int phosphorus;
    int totalProtein;
    int albumen;
    int globulin;

    public GeneralTestData() {
        gRef = 0;
        session = 0;
        bSugar = 0;
        urea = 0;
        uricAcid = 0;
        calcium = 0;
        acidPhospate = 0;
        cholestrol = 0;
        amylase = 0;
        phosphorus = 0;
        totalProtein = 0;
        albumen = 0;
        globulin = 0;
    }

    public GeneralTestData(int gRef, int session, int bSugar, int urea, int uricAcid, int calcium, int acidPhospate, int cholestrol, int amylase, int phosphorus, int totalProtein, int albumen, int globulin) {
        this.gRef = gRef;
        this.session = session;
        this.bSugar = bSugar;
        this.urea = urea;
        this.uricAcid = uricAcid;
        this.calcium = calcium;
        this.acidPhospate = acidPhospate;
        this.cholestrol = cholestrol;
        this.amylase = amylase;
        this.phosphorus = phosphorus;
        this.totalProtein = totalProtein;
        this.albumen = albumen;
        this.globulin = globulin;
    }

    public int getgRef() {
        return gRef;
    }

    public void setgRef(int gRef) {
        this.gRef = gRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getbSugar() {
        return bSugar;
    }

    public void setbSugar(int bSugar) {
        this.bSugar = bSugar;
    }

    public int getUrea() {
        return urea;
    }

    public void setUrea(int urea) {
        this.urea = urea;
    }

    public int getUricAcid() {
        return uricAcid;
    }

    public void setUricAcid(int uricAcid) {
        this.uricAcid = uricAcid;
    }

    public int getCalcium() {
        return calcium;
    }

    public void setCalcium(int calcium) {
        this.calcium = calcium;
    }

    public int getAcidPhospate() {
        return acidPhospate;
    }

    public void setAcidPhospate(int acidPhospate) {
        this.acidPhospate = acidPhospate;
    }

    public int getCholestrol() {
        return cholestrol;
    }

    public void setCholestrol(int cholestrol) {
        this.cholestrol = cholestrol;
    }

    public int getAmylase() {
        return amylase;
    }

    public void setAmylase(int amylase) {
        this.amylase = amylase;
    }

    public int getPhosphorus() {
        return phosphorus;
    }

    public void setPhosphorus(int phosphorus) {
        this.phosphorus = phosphorus;
    }

    public int getTotalProtein() {
        return totalProtein;
    }

    public void setTotalProtein(int totalProtein) {
        this.totalProtein = totalProtein;
    }

    public int getAlbumen() {
        return albumen;
    }

    public void setAlbumen(int albumen) {
        this.albumen = albumen;
    }

    public int getGlobulin() {
        return globulin;
    }

    public void setGlobulin(int globulin) {
        this.globulin = globulin;
    }
}
