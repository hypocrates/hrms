/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Wamuyu
 */
public class MedicalHistoryListData implements Serializable{
    int session;
    int href;
    String hName;
    Date dateEntry;
    Date dateExit;
    String diagnosis;
    String prescription;

    public MedicalHistoryListData() {
    }

    public MedicalHistoryListData(int session, int href, String hName, Date dateEntry, Date dateExit, String diagnosis, String prescription) {
        this.session = session;
        this.href = href;
        this.hName = hName;
        this.dateEntry = dateEntry;
        this.dateExit = dateExit;
        this.diagnosis = diagnosis;
        this.prescription = prescription;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public Date getDateEntry() {
        return dateEntry;
    }

    public void setDateEntry(Date dateEntry) {
        this.dateEntry = dateEntry;
    }

    public Date getDateExit() {
        return dateExit;
    }

    public void setDateExit(Date dateExit) {
        this.dateExit = dateExit;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getHref() {
        return href;
    }

    public void setHref(int href) {
        this.href = href;
    }

    public String gethName() {
        return hName;
    }

    public void sethName(String hName) {
        this.hName = hName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
