/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class PatientsList implements Serializable {
    int pRef;
    int nationalID;
    String name;
    int waiting;
    String entryDate;
    int session;
    
    public PatientsList(){
        pRef = 0;
        nationalID = 0;
        name = "";
        waiting = 0;
        entryDate = "";
        session = 0;
    }

    public PatientsList(int pRef, int nationalID, String name, int waiting, String entryDate, int session) {
        this.pRef = pRef;
        this.nationalID = nationalID;
        this.name = name;
        this.waiting = waiting;
        this.entryDate = entryDate;
        this.session = session;
    }

    public int getpRef() {
        return pRef;
    }

    public void setpRef(int pRef) {
        this.pRef = pRef;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWaiting() {
        return waiting;
    }

    public void setWaiting(int waiting) {
        this.waiting = waiting;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    
    
}
