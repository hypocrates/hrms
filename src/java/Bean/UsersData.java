/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.sql.Date;

/**
 *
 * @author Wamuyu
 */
public class UsersData {
    int dRef;
    int nationalID;
    String sirName;
    String firstname;
    String otherNames;
    String doB;
    String specialisation;
    String email;
    Date date;

    public UsersData() {
        this.dRef = 0;
        this.nationalID = 0;
        this.sirName = "";
        this.firstname = "";
        this.otherNames = "";
        this.doB = "";
        this.specialisation = "";
        this.email = "";
        Date date = new Date(0000,00,00);
    }

    public UsersData(int dRef, int nationalID, String sirName, String firstname, String otherNames, String doB, String specialisation, String email, Date date) {
        this.dRef = dRef;
        this.nationalID = nationalID;
        this.sirName = sirName;
        this.firstname = firstname;
        this.otherNames = otherNames;
        this.doB = doB;
        this.specialisation = specialisation;
        this.email = email;
        this.date = date;
    }

    public int getdRef() {
        return dRef;
    }

    public void setdRef(int dRef) {
        this.dRef = dRef;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getSirName() {
        return sirName;
    }

    public void setSirName(String sirName) {
        this.sirName = sirName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getDoB() {
        return doB;
    }

    public void setDoB(String doB) {
        this.doB = doB;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
