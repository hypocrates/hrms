/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class OtherTestData implements Serializable{
    int oRef;
    int session;
    int serum;
    int plasma;
    int lipidProfile;
    int thyroidFunction;
    int psa;
    int normalTest;
    

    public OtherTestData() {
        oRef = 0;
        session = 0;
        serum = 0;
        plasma = 0;
        lipidProfile = 0;
        thyroidFunction = 0;
        psa = 0;
        normalTest = 0;
    }

    public OtherTestData(int oRef, int session, int serum, int plasma, int lipidProfile, int thyroidFunction, int psa, int normalTest) {
        this.oRef = oRef;
        this.session = session;
        this.serum = serum;
        this.plasma = plasma;
        this.lipidProfile = lipidProfile;
        this.thyroidFunction = thyroidFunction;
        this.psa = psa;
        this.normalTest = normalTest;
    }

    

    public int getoRef() {
        return oRef;
    }

    public void setoRef(int oRef) {
        this.oRef = oRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getSerum() {
        return serum;
    }

    public void setSerum(int serum) {
        this.serum = serum;
    }

    public int getPlasma() {
        return plasma;
    }

    public void setPlasma(int plasma) {
        this.plasma = plasma;
    }

    public int getLipidProfile() {
        return lipidProfile;
    }

    public void setLipidProfile(int lipidProfile) {
        this.lipidProfile = lipidProfile;
    }

    public int getThyroidFunction() {
        return thyroidFunction;
    }

    public void setThyroidFunction(int thyroidFunction) {
        this.thyroidFunction = thyroidFunction;
    }

    public int getPsa() {
        return psa;
    }

    public void setPsa(int psa) {
        this.psa = psa;
    }

    public int getNormalTest() {
        return normalTest;
    }

    public void setNormalTest(int normalTest) {
        this.normalTest = normalTest;
    }
    
}
