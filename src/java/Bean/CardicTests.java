/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class CardicTests implements Serializable{
    String LDH;
    String HBD;
    String CPK;
    String SGOT;

    public CardicTests() {
        LDH = "NULL";
        HBD = "NULL";
        CPK = "NULL";
        SGOT = "NULL";
    }

    public CardicTests(String LDH, String HBD, String CPK, String SGOT) {
        this.LDH = LDH;
        this.HBD = HBD;
        this.CPK = CPK;
        this.SGOT = SGOT;
    }

    public String getLDH() {
        return LDH;
    }

    public void setLDH(String LDH) {
        this.LDH = LDH;
    }

    public String getHBD() {
        return HBD;
    }

    public void setHBD(String HBD) {
        this.HBD = HBD;
    }

    public String getCPK() {
        return CPK;
    }

    public void setCPK(String CPK) {
        this.CPK = CPK;
    }

    public String getSGOT() {
        return SGOT;
    }

    public void setSGOT(String SGOT) {
        this.SGOT = SGOT;
    }
    
    
}
