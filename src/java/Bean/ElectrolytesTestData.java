/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class ElectrolytesTestData implements Serializable{
    int eRef;
    int session;
    int sodium;
    int potassium;
    int chloride;
    int bicarbonate;

    public ElectrolytesTestData() {
        eRef = 0;
        session = 0;
        sodium = 0;
        potassium = 0;
        chloride = 0;
        bicarbonate = 0;
    }

    public ElectrolytesTestData(int eRef, int session, int sodium, int potassium, int chloride, int bicarbonate) {
        this.eRef = eRef;
        this.session = session;
        this.sodium = sodium;
        this.potassium = potassium;
        this.chloride = chloride;
        this.bicarbonate = bicarbonate;
    }

    public int geteRef() {
        return eRef;
    }

    public void seteRef(int eRef) {
        this.eRef = eRef;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getSodium() {
        return sodium;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public int getPotassium() {
        return potassium;
    }

    public void setPotassium(int potassium) {
        this.potassium = potassium;
    }

    public int getChloride() {
        return chloride;
    }

    public void setChloride(int chloride) {
        this.chloride = chloride;
    }

    public int getBicarbonate() {
        return bicarbonate;
    }

    public void setBicarbonate(int bicarbonate) {
        this.bicarbonate = bicarbonate;
    }
}
