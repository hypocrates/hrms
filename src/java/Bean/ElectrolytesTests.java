/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class ElectrolytesTests implements Serializable{
    String sodium;
    String potassium;
    String chloride;
    String bicarbonate;
    
    public ElectrolytesTests(){
        sodium = "NULL";
        potassium = "NULL";
        chloride = "NULL";
        bicarbonate = "NULL";
    }

    public ElectrolytesTests(String sodium, String potassium, String chloride, String bicarbonate) {
        this.sodium = sodium;
        this.potassium = potassium;
        this.chloride = chloride;
        this.bicarbonate = bicarbonate;
    }

    public String getSodium() {
        return sodium;
    }

    public void setSodium(String sodium) {
        this.sodium = sodium;
    }

    public String getPotassium() {
        return potassium;
    }

    public void setPotassium(String potassium) {
        this.potassium = potassium;
    }

    public String getChloride() {
        return chloride;
    }

    public void setChloride(String chloride) {
        this.chloride = chloride;
    }

    public String getBicarbonate() {
        return bicarbonate;
    }

    public void setBicarbonate(String bicarbonate) {
        this.bicarbonate = bicarbonate;
    }
    
}
