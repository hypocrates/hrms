/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class Year implements Serializable{
    int jan;
    int feb;
    int march;
    int april;
    int may;
    int june;
    int july;
    int august;
    int sept;
    int oct;
    int nov;
    int dec;

    public Year() {
        this.jan = 0;
        this.feb = 0;
        this.march = 0;
        this.april = 0;
        this.may = 0;
        this.june = 0;
        this.july = 0;
        this.august = 0;
        this.sept = 0;
        this.oct = 0;
        this.nov = 0;
        this.dec = 0;
    }

    public Year(int jan, int feb, int march, int april, int may, int june, int july, int august, int sept, int oct, int nov, int dec) {
        this.jan = jan;
        this.feb = feb;
        this.march = march;
        this.april = april;
        this.may = may;
        this.june = june;
        this.july = july;
        this.august = august;
        this.sept = sept;
        this.oct = oct;
        this.nov = nov;
        this.dec = dec;
    }

    public int getJan() {
        return jan;
    }

    public void setJan(int jan) {
        this.jan = jan;
    }

    public int getFeb() {
        return feb;
    }

    public void setFeb(int feb) {
        this.feb = feb;
    }

    public int getMarch() {
        return march;
    }

    public void setMarch(int march) {
        this.march = march;
    }

    public int getApril() {
        return april;
    }

    public void setApril(int april) {
        this.april = april;
    }

    public int getMay() {
        return may;
    }

    public void setMay(int may) {
        this.may = may;
    }

    public int getJune() {
        return june;
    }

    public void setJune(int june) {
        this.june = june;
    }

    public int getJuly() {
        return july;
    }

    public void setJuly(int july) {
        this.july = july;
    }

    public int getAugust() {
        return august;
    }

    public void setAugust(int august) {
        this.august = august;
    }

    public int getSept() {
        return sept;
    }

    public void setSept(int sept) {
        this.sept = sept;
    }

    public int getOct() {
        return oct;
    }

    public void setOct(int oct) {
        this.oct = oct;
    }

    public int getNov() {
        return nov;
    }

    public void setNov(int nov) {
        this.nov = nov;
    }

    public int getDec() {
        return dec;
    }

    public void setDec(int dec) {
        this.dec = dec;
    }

    @Override
    public String toString() {
        return "Year{" + "jan=" + jan + ", feb=" + feb + ", march=" + march + ", april=" + april + ", may=" + may + ", june=" + june + ", july=" + july + ", august=" + august + ", sept=" + sept + ", oct=" + oct + ", nov=" + nov + ", dec=" + dec + '}';
    }
}
