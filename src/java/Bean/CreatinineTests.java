/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class CreatinineTests implements Serializable{
    String clearance;
    String serum;
    String urine;

    public CreatinineTests() {
        clearance = "NULL";
        serum = "NULL";
        urine = "NULL";
    }

    public CreatinineTests(String clearance, String serum, String urine) {
        this.clearance = clearance;
        this.serum = serum;
        this.urine = urine;
    }

    public String getClearance() {
        return clearance;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public String getSerum() {
        return serum;
    }

    public void setSerum(String serum) {
        this.serum = serum;
    }

    public String getUrine() {
        return urine;
    }

    public void setUrine(String urine) {
        this.urine = urine;
    }
    
}
