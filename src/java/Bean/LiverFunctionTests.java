/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class LiverFunctionTests implements Serializable{
    String bTotal;
    String bDirect;
    String bIndirect;
    String alkaline;
    String SGOT;
    String SGPT;
    String YGT;
    
    public LiverFunctionTests(){
        bTotal = "NULL";
        bDirect = "NULL";
        bIndirect = "NULL";
        alkaline = "NULL";
        SGOT = "NULL";
        SGPT = "NULL";
        YGT = "NULL";
    }

    public LiverFunctionTests(String bTotal, String bDirect, String bIndirect, String alkaline, String SGOT, String SGPT, String YGT) {
        this.bTotal = bTotal;
        this.bDirect = bDirect;
        this.bIndirect = bIndirect;
        this.alkaline = alkaline;
        this.SGOT = SGOT;
        this.SGPT = SGPT;
        this.YGT = YGT;
    }

    public String getbTotal() {
        return bTotal;
    }

    public void setbTotal(String bTotal) {
        this.bTotal = bTotal;
    }

    public String getbDirect() {
        return bDirect;
    }

    public void setbDirect(String bDirect) {
        this.bDirect = bDirect;
    }

    public String getbIndirect() {
        return bIndirect;
    }

    public void setbIndirect(String bIndirect) {
        this.bIndirect = bIndirect;
    }

    public String getAlkaline() {
        return alkaline;
    }

    public void setAlkaline(String alkaline) {
        this.alkaline = alkaline;
    }

    public String getSGOT() {
        return SGOT;
    }

    public void setSGOT(String SGOT) {
        this.SGOT = SGOT;
    }

    public String getSGPT() {
        return SGPT;
    }

    public void setSGPT(String SGPT) {
        this.SGPT = SGPT;
    }

    public String getYGT() {
        return YGT;
    }

    public void setYGT(String YGT) {
        this.YGT = YGT;
    }
    
    
}
