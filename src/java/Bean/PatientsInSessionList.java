/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class PatientsInSessionList implements Serializable{
    int pRef;
    int nationalID;
    String name;
    String Status;
    int session;

    public PatientsInSessionList() {
        pRef = 0;
        nationalID = 0;
        name = "";
        Status = "";
        session = 0;
    }

    public PatientsInSessionList(int pRef, int nationalID, String name, String Status, int session) {
        this.pRef = pRef;
        this.nationalID = nationalID;
        this.name = name;
        this.Status = Status;
        this.session = session;
    }

    public int getpRef() {
        return pRef;
    }

    public void setpRef(int pRef) {
        this.pRef = pRef;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }
    
}
