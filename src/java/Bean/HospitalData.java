/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class HospitalData implements Serializable{
    int hRef;
    String name;
    String location;
    String size;
    String address;
    
    public HospitalData(){
        hRef = 0;
        name = "";
        location = "";
        size = "";
        address = "";
    }

    public HospitalData(int hRef, String name, String location, String size, String address) {
        this.hRef = hRef;
        this.name = name;
        this.location = location;
        this.size = size;
        this.address = address;
    }

    public int gethRef() {
        return hRef;
    }

    public void sethRef(int hRef) {
        this.hRef = hRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
