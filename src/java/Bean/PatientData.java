/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class PatientData implements Serializable{
    int pRef;
    int nationalID;
    String sirName;
    String firstName;
    String middleName;
    String lastName;
    
    public PatientData (){
        pRef = 0;
        nationalID = 0;
        sirName= "";
        firstName="";
        middleName="";
        lastName="";
    }

    public PatientData(int pRef, int nationalID, String sirName, String firstName, String middleName, String lastName) {
        this.pRef = pRef;
        this.nationalID = nationalID;
        this.sirName = sirName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public int getpRef() {
        return pRef;
    }

    public void setpRef(int pRef) {
        this.pRef = pRef;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getSirName() {
        return sirName;
    }

    public void setSirName(String sirName) {
        this.sirName = sirName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    
}
