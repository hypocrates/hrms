/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class Login implements Serializable{
    private int dRef;
    private String password;
    
    public Login(){
        dRef=0;
        password="";
    }

    public Login(int dRef, String password) {
        this.dRef = dRef;
        this.password = password;
    }

    public int getDRef() {
        return dRef;
    }

    public void setDRef(int dRef) {
        this.dRef = dRef;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
