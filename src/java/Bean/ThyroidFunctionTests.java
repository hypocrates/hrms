/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class ThyroidFunctionTests implements Serializable{
    String tshLevels;
    String totalT3Levels;
    String totalT4Levels;
    
    public ThyroidFunctionTests(){
        tshLevels = "NULL";
        totalT3Levels = "NULL";
        totalT4Levels = "NULL";
    }

    public ThyroidFunctionTests(String tshLevels, String totalT3Levels, String totalT4Levels) {
        this.tshLevels = tshLevels;
        this.totalT3Levels = totalT3Levels;
        this.totalT4Levels = totalT4Levels;
    }

    public String getTshLevels() {
        return tshLevels;
    }

    public void setTshLevels(String tshLevels) {
        this.tshLevels = tshLevels;
    }

    public String getTotalT3Levels() {
        return totalT3Levels;
    }

    public void setTotalT3Levels(String totalT3Levels) {
        this.totalT3Levels = totalT3Levels;
    }

    public String getTotalT4Levels() {
        return totalT4Levels;
    }

    public void setTotalT4Levels(String totalT4Levels) {
        this.totalT4Levels = totalT4Levels;
    }
}
