/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;
/**
 *
 * @author Wamuyu
 */
public class ContactData implements Serializable{
    int pRef;
    String occupation;
    String county;
    String residence;
    String phoneNumber;
    String email;
    
    public ContactData(){
        pRef=0;
        occupation="";
        county="";
        residence="";
        phoneNumber="";
        email="";
    }

    public ContactData(int pRef, String occupation, String county, String residence, String phoneNumber, String email) {
        this.pRef = pRef;
        this.occupation = occupation;
        this.county = county;
        this.residence = residence;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public int getpRef() {
        return pRef;
    }

    public void setpRef(int pRef) {
        this.pRef = pRef;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
