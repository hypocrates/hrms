/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;

/**
 *
 * @author Wamuyu
 */
public class AdminsAddStuffData implements Serializable{
    int dRef;
    int nationalID;
    String sirName;
    String firstName;
    String otherNames;
    String doB;
    String specialisation;
    String email;
    
    public AdminsAddStuffData (){
        dRef = 0;
        nationalID = 0;
        sirName= "";
        firstName="";
        otherNames="";
        doB="";
        specialisation ="";
        email="";
    }

    public AdminsAddStuffData(int dRef, int nationalID, String sirName, String firstName, String otherNames, String doB, String specialisation, String email) {
        this.dRef = dRef;
        this.nationalID = nationalID;
        this.sirName = sirName;
        this.firstName = firstName;
        this.otherNames = otherNames;
        this.doB = doB;
        this.specialisation = specialisation;
        this.email = email;
    }

    public int getdRef() {
        return dRef;
    }

    public void setdRef(int dRef) {
        this.dRef = dRef;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getSirName() {
        return sirName;
    }

    public void setSirName(String sirName) {
        this.sirName = sirName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getDoB() {
        return doB;
    }

    public void setDoB(String doB) {
        this.doB = doB;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
