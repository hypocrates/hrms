/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.CardicTestData;
import Bean.CreatinineTestData;
import Bean.ElectrolytesTestData;
import Bean.GeneralTestData;
import Bean.LiverTestData;
import Bean.OtherTestData;
import DbClasses.LabOrderDB;
import DbClasses.SessionDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class LabOrderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url;
        String message;
        int normalTest = 0;
        int session = Integer.parseInt(request.getParameter("session"));
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        String complains = request.getParameter("complains");
        String diagnosis = request.getParameter("diagnosis");
        String name = request.getParameter("name");
        String gender = request.getParameter("gender");
        String age = request.getParameter("age");
        String bloodGroup = request.getParameter("bloodGroup");
        String action  = request.getParameter("action");
        switch (action) {
            case "send":
                LabOrderDB lDB = new LabOrderDB();
                //General Tests
                String[] general = request.getParameterValues("general");
                if(general!=null){
                    GeneralTestData gTest = new GeneralTestData();
                    for( String test : general){
                        switch (test) {
                            case "Blood Sugar":
                                gTest.setbSugar(1);
                                normalTest = 1;
                                break;
                            case "Urea":
                                gTest.setUrea(1);
                                normalTest = 1;
                                break;
                            case "Uric Acid":
                                gTest.setUricAcid(1);
                                normalTest = 1;
                                break;
                            case "Calcim":
                                gTest.setCalcium(1);
                                normalTest = 1;
                                break;
                            case "Acid Phosphate (Prostatic)":
                                gTest.setAcidPhospate(1);
                                normalTest = 1;
                                break;
                            case "Cholestrol":
                                gTest.setCholestrol(1);
                                normalTest = 1;
                                break;
                            case "Amylase":
                                gTest.setAmylase(1);
                                normalTest = 1;
                                break;
                            case "Phosphorus":
                                gTest.setPhosphorus(1);
                                normalTest = 1;
                                break;
                            case "Total Protein":
                                gTest.setTotalProtein(1);
                                normalTest = 1;
                            case "Albumen":
                                gTest.setAlbumen(1);
                                normalTest = 1;
                                break;
                            case "Globulin":
                                gTest.setGlobulin(1);
                                normalTest = 1;
                                break;
                            default:
                                break;
                        }
                    }
                    lDB.addGeneralTests(session, gTest);
                }   //Electrolytes
                String [] electrolytes = request.getParameterValues("electrolytes");
                if(electrolytes!=null){
                    ElectrolytesTestData eTests = new ElectrolytesTestData();
                    for(String test : electrolytes){
                        switch (test){
                            case "Sodium":
                                eTests.setSodium(1);
                                normalTest = 1;
                                break;
                            case "Potassium":
                                eTests.setPotassium(1);
                                normalTest = 1;
                                break;
                            case "Chloride":
                                eTests.setChloride(1);
                                normalTest = 1;
                                break;
                            case "Bicarbonate":
                                eTests.setBicarbonate(1);
                                normalTest = 1;
                                break;
                            default:
                                break;
                        }
                    }
                    lDB.addElectrolytesTests(session, eTests);
                }   //Liver Function Tests
                String[] liver = request.getParameterValues("liver");
                if(liver!=null){
                    LiverTestData lTests = new LiverTestData();
                    for(String test : liver){
                        switch (test){
                            case "Bilirubin (Total)":
                                lTests.setbTotal(1);
                                normalTest = 1;
                                break;
                            case "Bilirubin (Direct)":
                                lTests.setbDirect(1);
                                normalTest = 1;
                                break;
                            case "Bilirubin (Indirect)":
                                lTests.setbIndirect(1);
                                normalTest = 1;
                                break;
                            case "Alkaline Phosphate":
                                lTests.setAlkalinePhosphate(1);
                                normalTest = 1;
                                break;
                            case "Amiotransfcrase (S.G.O.T)":
                                lTests.setSgot(1);
                                normalTest = 1;
                                break;
                            case "Amiotransfcrase (S.G.P.T)":
                                lTests.setSgpt(1);
                                normalTest = 1;
                                break;
                            case "Y-Glutamy transfcrase (Y-GT)":
                                lTests.setYgt(1);
                                normalTest = 1;
                                break;
                            default:
                                break;
                        }
                    }
                    lDB.addLiverTest(session, lTests);
                }   //Cardic Enzymes
                String[] cardic = request.getParameterValues("cardic");
                if(cardic!=null){
                    CardicTestData cTests = new CardicTestData();
                    for(String test : cardic){
                        switch (test){
                            case "Lactic Dehydrogenase (LDH)":
                                cTests.setLdh(1);
                                normalTest = 1;
                                break;
                            case "Hydroxybutyrate Dehydrogenase (HBD)":
                                cTests.setHbd(1);
                                normalTest = 1;
                                break;
                            case "Creatinine Phospho Kinase (CPK)":
                                cTests.setCpk(1);
                                normalTest = 1;
                                break;
                            case "Aminotransfcrase (S.G.O.T)":
                                cTests.setSgot(1);
                                normalTest = 1;
                                break;
                            default:
                                break;
                        }
                    }
                    lDB.addCardicTest(session, cTests);
                }   //creatinine Tests
                String[] creatinine = request.getParameterValues("creatinine");
                if(creatinine!=null){
                    CreatinineTestData crTests = new CreatinineTestData();
                    for(String test : creatinine){
                        switch (test){
                            case "Creatinine Clearance":
                                crTests.setClearance(1);
                                normalTest = 1;
                                break;
                            case "Creatinine (Serum)":
                                crTests.setSerum(1);
                                normalTest = 1;
                                break;
                            case "Creatinine (Urine)":
                                crTests.setUrine(1);
                                normalTest = 1;
                                break;
                            default:
                                break;
                        }
                    }
                    lDB.addCreatinineTest(session, crTests);
                }   //check boxes
                OtherTestData oTests = new OtherTestData();
                String serum = request.getParameter("serum");
                if(serum != null){
                    oTests.setSerum(1);
                }   
                String plasma = request.getParameter("plasma");
                if(plasma != null){
                    oTests.setPlasma(1);
                }   
                String lipid = request.getParameter("lipid");
                if(lipid != null){
                    oTests.setLipidProfile(1);
                }   
                String thyroid = request.getParameter("thyroid");
                if(thyroid != null){
                    oTests.setThyroidFunction(1);
                }   
                String psa = request.getParameter("psa");
                if(psa != null){
                    oTests.setPsa(1);
                }   
                oTests.setNormalTest(normalTest);
                //Enter to DB
                lDB.addOtherTests(session, oTests);
                int rows = new SessionDB().changeStatus("Waiting for Lab Tech", session);
                if(rows!=0){
                    message = "Labwork Ordered Successfully.";
                } else
                    message = "Labwork Order ERROR";
                request.setAttribute("message", message);
                url = "/LabOrder.jsp";
                break;
            case "back":
                request.setAttribute("Name", name);
                request.setAttribute("session", session);
                request.setAttribute("pRef", pRef);
                request.setAttribute("gender", gender);
                request.setAttribute("age", age);
                request.setAttribute("bloodGroup", bloodGroup);
                request.setAttribute("complains", complains);
                request.setAttribute("diagnosis", diagnosis);
                String labStatus = new SessionDB().getStatus(session);
                String labResultsButton;
                if(labStatus.equals("Received Lab Results")){
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\">View Lab Results</button>";
                } else
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\" disabled>View Lab Results</button>";
                String labOrderButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#labOrderModal\">Order for Lab Work</button>";
                String prescriptionButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#prescriptionModal\">Prescription</button>";
                request.setAttribute("labOrderButton", labOrderButton);
                request.setAttribute("prescriptionButton", prescriptionButton);
                request.setAttribute("labResultsButton", labResultsButton);
                url = "/ChiefComplains.jsp";
                break;
            default:
                url="index.jsp";
                break;
        }
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
