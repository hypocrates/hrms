/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import API.PasswordUtility;
import Bean.HospitalData;
import Bean.Login;
import DbClasses.LoginDB;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class ChangePassWordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.jsp";
        //get parameters
        String newPw = request.getParameter("newPw");
        String confirmPw = request.getParameter("confirmPw");
        String oldPw = request.getParameter("oldPw");
        String message = "";
        //get session
        HttpSession session = request.getSession(false);
        int dRef = (int) session.getAttribute("dRef");
        //Initialise the api
        PasswordUtility pwUtil = new PasswordUtility();
        
        LoginDB log = new LoginDB();
        String email =(String) session.getAttribute("email");
        //get hospitals the user is in      
        ArrayList<HospitalData> hpArray = log.getHospital(email);
        int hpNumber = hpArray.size();
        
        String dbOldPw = log.getPassword(dRef);
        System.out.println("Old DB = " +dbOldPw);
        System.out.println("Old jsp = " +oldPw);
        
        //Confirm if old password equals equals password in Db
        String hashedOldPw = "";
        String dbSalt = log.getSalt(dRef);
        try{
            hashedOldPw = pwUtil.hashAndSaltPassword(oldPw, dbSalt);
        } catch(NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        
        Login verify = new Login(dRef, hashedOldPw);
        String result = log.AuthenticateUser(verify);
        if(result.equals("SUCCESS")){
            //Confirm new password accuracy
            if( confirmPw.equals(newPw)){
                
                //Secure Password
                String salt = pwUtil.getSalt();
                String hashedPassword;
                int passRows = 0;
                try{
                    hashedPassword = pwUtil.hashAndSaltPassword(newPw, salt);
                    passRows = log.changePassword(dRef, salt, hashedPassword);
                } catch(NoSuchAlgorithmException e){
                    e.printStackTrace();
                    message = "Error Occured. Password not changed. Contact IT Support.";
                }
                
                
                if(passRows == 1){
                    message = "Password Changed Successfully";
                    int rows = log.changePasswordChecker(dRef);
                } else{
                    message = "Error Occured. Password not changed.";
                }
            } else{
                message = "New and Confirm Password do not Match up. PassWord was not Changed";
            }    
        } else {
            message = "Old Password does not Match up. Password was not Changed";
        }
        request.setAttribute("hpArray", hpArray);
        request.setAttribute("size", hpNumber);
        request.setAttribute("message", message);
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
