/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import API.AgeCalculator;
import Bean.BioData;
import Bean.CardicTests;
import Bean.CreatinineTests;
import Bean.ElectrolytesTests;
import Bean.GeneralTests;
import Bean.HospitalData;
import Bean.LipidProfileTests;
import Bean.LiverFunctionTests;
import Bean.MedicalHistoryListData;
import Bean.PatientData;
import Bean.PrescriptionData;
import Bean.ProstaticTests;
import Bean.Session;
import Bean.ThyroidFunctionTests;
import DbClasses.LabResultDB;
import DbClasses.MedicalHistoryDB;
import DbClasses.SearchDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class MedicalHistoryServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.jsp";
        String action;
        String message = "";
        int pRef;
        int session;
        if(request.getAttribute("action") != null){
            action = (String) request.getAttribute("action");
            pRef = (Integer) (request.getAttribute("pRef"));
            session = (Integer) (request.getAttribute("session"));
        } else{
            action = request.getParameter("action");
            pRef = Integer.parseInt(request.getParameter("pRef"));
            session = Integer.parseInt(request.getParameter("session"));
        }
        
        Session sessionData;
        PrescriptionData psData;
        PatientData pData;
        MedicalHistoryDB mhDB = new MedicalHistoryDB();
        pData = mhDB.getPatientName(pRef);
        String name = pData.getSirName()+" "+pData.getFirstName()+" "+pData.getLastName()+" "+pData.getMiddleName();
        
        if(action.equals("List")){
            ArrayList<MedicalHistoryListData> mhlArrayList = null;
            int size = 0;
            int recordsNumber = mhDB.checkExistence(pRef);
            if( recordsNumber == 1){
                //Show No records
                message = "The patient does not seem to have a previous record";
                url = "/MedicalHistoryList.jsp";
            } else{
                int [] sessions = mhDB.getSessions(pRef, recordsNumber);
                if( sessions == null){
                    //Error message
                    message = "An error occured retrieveing the details";
                } else{
                    mhlArrayList = new ArrayList<>();
                    for(int sess : sessions){
                        if(sess != session){
                            sessionData = mhDB.getSession(sess);
                            int hRef = sessionData.getHospital();
                            Date dateEntry = sessionData.getDateEntry();
                            Date dateExit = sessionData.getDateExit();
                            String hospitalName = mhDB.getHospitalName(hRef);
                            psData = mhDB.getPrescriptionDiag(sess);
                            String diagnosis = psData.getDiagnosis();
                            String prescription = psData.getPrescription();
                            
                            //Store in bean
                            MedicalHistoryListData mhlData = new MedicalHistoryListData();
                            mhlData.setDateEntry(dateEntry);
                            mhlData.setDateExit(dateExit);
                            mhlData.setDiagnosis(diagnosis);
                            mhlData.setHref(hRef);
                            mhlData.setPrescription(prescription);
                            mhlData.setSession(sess);
                            mhlData.sethName(hospitalName);
                            
                            //Store in ArrayList
                            mhlArrayList.add(mhlData);
                            size = mhlArrayList.size();
                        }
                    }
                }
            }
            request.setAttribute("size", size);
            request.setAttribute("medicalHistoryListData", mhlArrayList);
            url = "/MedicalHistoryList.jsp";
        } else if(action.equals("details")){
            int sess = Integer.parseInt(request.getParameter("sess"));
            String complains = mhDB.getChiefComplains(sess);
            psData = mhDB.getPrescriptionDiag(sess);
            String diagnosis = psData.getDiagnosis();
            String prescription = psData.getPrescription();
            String doctor = mhDB.getDoctor(psData.getDoctor());
            
            sessionData = mhDB.getSession(sess);
            Date dateEntry = sessionData.getDateEntry();
            Date dateExit = sessionData.getDateExit();
            String incidenceDate = "From " + dateEntry + " To " +dateExit;
            
            BioData bioData;
            bioData = new SearchDB().getBioData(pRef);
            String gender = bioData.getGender();
            String bloodGroup = bioData.getBloodGroup();
            String dOB = bioData.getDob();
            int age = AgeCalculator.calculateAge(dOB);
            int thenAge = AgeCalculator.calculateAge2(dateEntry, dOB);
            
            HospitalData hpData;
            hpData = mhDB.getHospital(sessionData.getHospital());
            String hpName = hpData.getName();
            String hpLocation = hpData.getLocation();
            String hpSize = hpData.getSize();
            String hpAddress = hpData.getAddress();
            
            LabResultDB lrDB = new LabResultDB();
            
            GeneralTests gTests  =  lrDB.getBloodWork(sess);
            ElectrolytesTests eTests = lrDB.getElectrolytesTests(sess);
            LiverFunctionTests lfTests = lrDB.getLiverFunctionTests(sess);
            CardicTests cTests = lrDB.getCardicTests(sess);
            CreatinineTests crTests = lrDB.getCreatinineTests(sess);
            LipidProfileTests lpTests = lrDB.getLipidProfileTests(sess);
            ProstaticTests pTests = lrDB.getProstaticTests(sess);
            ThyroidFunctionTests tfTests = lrDB.getThyroidFunctionTests(sess);
            
            request.setAttribute("GeneralTest", gTests);
            request.setAttribute("ElectrolytesTest", eTests);
            request.setAttribute("LiverTest", lfTests);
            request.setAttribute("CardicTest", cTests);
            request.setAttribute("CreatinineTest", crTests);
            request.setAttribute("LipidTest", lpTests);
            request.setAttribute("ProstaticTest", pTests);
            request.setAttribute("ThyroidTest", tfTests);
            request.setAttribute("Complains", complains);
            request.setAttribute("hpName", hpName);
            request.setAttribute("hpLocation", hpLocation);
            request.setAttribute("hpSize", hpSize);
            request.setAttribute("hpAddress", hpAddress);
            request.setAttribute("gender", gender);
            request.setAttribute("bloodGroup", bloodGroup);
            request.setAttribute("age", age);
            request.setAttribute("thenAge", thenAge);
            request.setAttribute("incidenceDate", incidenceDate);
            request.setAttribute("complains", complains);
            request.setAttribute("diagnosis", diagnosis);
            request.setAttribute("prescription", prescription);
            request.setAttribute("doctor", doctor);
            
            url = "/MedicalHistory.jsp";
        }
        request.setAttribute("session", session);
        request.setAttribute("pRef", pRef);
        request.setAttribute("Name", name);
        request.setAttribute("message", message);
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
