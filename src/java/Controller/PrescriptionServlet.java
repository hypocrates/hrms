/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.PrescriptionData;
import DbClasses.PrescriptionDB;
import DbClasses.SessionDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class PrescriptionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url;
        int session = Integer.parseInt(request.getParameter("session"));
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        String complains = request.getParameter("complains");
        String diagnosis = request.getParameter("diag");
        String name = request.getParameter("name");
        String gender = request.getParameter("gender");
        String age = request.getParameter("age");
        String bloodGroup = request.getParameter("bloodGroup");
        String action  = request.getParameter("action");
        PrescriptionDB prescriptionDB = new PrescriptionDB();
        
        HttpSession sessn = request.getSession(false);
        int dRef =(int) sessn.getAttribute("dRef");
        int hRef =(int) sessn.getAttribute("hRef");
        
        if(action.equals("save")){
            String prescription = request.getParameter("prescription");
            String status = new SessionDB().getStatus(session);
            PrescriptionData dpData = new PrescriptionData();
            dpData.setSession(session);
            dpData.setDoctor(dRef);
            dpData.setDiagnosis(diagnosis);
            dpData.setPrescription(prescription);
            int rows = prescriptionDB.savePrescriptionDiag(dpData);
            if (rows!=0){
                String actions = "details";
                request.setAttribute("action", actions);
                if(status.equals("Admitted")){
                    url = "/AdmittedPatientsList";
                } else {
                    url = "/PatientsInSession";
                }
            } else{
                String message = "an error occured";
                request.setAttribute("message", message);
                url = "/Prescription.jsp";
            }
        } else if(action.equals("back")){
                       
            String labStatus = new SessionDB().getStatus(session);
            String labResultsButton;
            if(labStatus.equals("Received Lab Results")){
                labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\">View Lab Results</button>";
            } else
                labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\" disabled>View Lab Results</button>";
            String labOrderButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#labOrderModal\">Order for Lab Work</button>";
            String prescriptionButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#prescriptionModal\">Prescription</button>";
            request.setAttribute("labOrderButton", labOrderButton);
            request.setAttribute("prescriptionButton", prescriptionButton);
            request.setAttribute("labResultsButton", labResultsButton);
            url = "/ChiefComplains.jsp";
        } else if(action.equals("admit")){
            String prescription = request.getParameter("prescription");
            
            PrescriptionData dpData = new PrescriptionData();
            dpData.setSession(session);
            dpData.setDoctor(dRef);
            dpData.setDiagnosis(diagnosis);
            dpData.setPrescription(prescription);
            int rows = prescriptionDB.savePrescriptionDiag(dpData);
            if(rows!=0){
                SessionDB sessDB = new SessionDB();
                String type = "Admitted";
                int row = sessDB.changeType(type, session);
                url = "/AdmittedPatientsList";
            }  else{
                String message = "an error occured";
                request.setAttribute("message", message);
                url = "/Prescription.jsp";
            }
        } else{
            url = "/index.jsp";
        }
        request.setAttribute("Name", name);
        request.setAttribute("session", session);
        request.setAttribute("pRef", pRef);
        request.setAttribute("gender", gender);
        request.setAttribute("age", age);
        request.setAttribute("bloodGroup", bloodGroup);
        request.setAttribute("complains", complains);
        request.setAttribute("diagnosis", diagnosis);
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
