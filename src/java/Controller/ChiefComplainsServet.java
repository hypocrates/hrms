/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.ChiefComplainsData;
import Bean.PrescriptionData;
import Bean.Session;
import DbClasses.ChiefComplainsDB;
import DbClasses.PrescriptionDB;
import DbClasses.SessionDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class ChiefComplainsServet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url;
        String action = request.getParameter("action");
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        int session = Integer.parseInt(request.getParameter("session"));
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String gender = request.getParameter("gender");
        String bloodGroup = request.getParameter("bloodGroup");
        
        HttpSession sessn = request.getSession(false);
        int dRef =(int) sessn.getAttribute("dRef");
        int hRef =(int) sessn.getAttribute("hRef");
        
        if(action.equals("orderLab")){
            String cc = request.getParameter("complains");
            String diag = request.getParameter("diagnosis");
            String bmi = request.getParameter("bmi");
            String bp = request.getParameter("bp");
            
            ChiefComplainsData ccData = new ChiefComplainsData(0, session, dRef, hRef, diag, cc,  bmi, bp);
            ChiefComplainsDB ccDB = new ChiefComplainsDB();
            int rows = ccDB.saveChiefComplains(ccData);
            if (rows!=0){
                request.setAttribute("complains", cc);
                request.setAttribute("diagnosis", diag);
                url = "/LabOrder.jsp";
            }else{
                String message = "An error Occured while storing";
                request.setAttribute("message", message);
                request.setAttribute("complains", cc);
                request.setAttribute("diagnosis", diag);
                request.setAttribute("bp", bp);
                String labStatus = new SessionDB().getStatus(session);
                String labResultsButton;
                if(labStatus.equals("Received Lab Results")){
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\">View Lab Results</button>";
                } else
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\" disabled>View Lab Results</button>";
                String labOrderButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#labOrderModal\">Order for Lab Work</button>";
                String prescriptionButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#prescriptionModal\">Prescription</button>";
                request.setAttribute("labOrderButton", labOrderButton);
                request.setAttribute("prescriptionButton", prescriptionButton);
                request.setAttribute("labResultsButton", labResultsButton);
                url = "/ChiefComplains.jsp";
            } 
        } else if(action.equals("prescription")){
            String status = new SessionDB().getStatus(session);
            if(!"Admitted".equals(status)){
                String button = "<button class=\"btn btn-warning\" type=\"submit\" name=\"action\" value=\"admit\">Admit patient</button>";
                request.setAttribute("button", button);
            }
            if(status.equals("Admitted")){
                PrescriptionData dpData = new PrescriptionDB().getPrescriptionDiag(session);
                request.setAttribute("prescription", dpData.getPrescription());
            }
            String cc = request.getParameter("complains");
            String diag = request.getParameter("diagnosis");
            String bmi = request.getParameter("bmi");
            String bp = request.getParameter("bp");
            
            ChiefComplainsData ccData = new ChiefComplainsData(0, session, dRef, hRef, diag, cc,  bmi, bp);
            ChiefComplainsDB ccDB = new ChiefComplainsDB();
            int rows = ccDB.saveChiefComplains(ccData);
            if (rows!=0){
                url = "/Prescription.jsp";
            }else{
                String message = "An error Occured while storing";
                request.setAttribute("message", message);
                request.setAttribute("bp", bp);
                String labStatus = new SessionDB().getStatus(session);
                String labResultsButton;
                if(labStatus.equals("Received Lab Results")){
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\">View Lab Results</button>";
                } else
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\" disabled>View Lab Results</button>";
                String labOrderButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#labOrderModal\">Order for Lab Work</button>";
                String prescriptionButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#prescriptionModal\">Prescription</button>";
                request.setAttribute("labOrderButton", labOrderButton);
                request.setAttribute("prescriptionButton", prescriptionButton);
                request.setAttribute("labResultsButton", labResultsButton);
                url = "/ChiefComplains.jsp";
            }
            request.setAttribute("complains", cc);
            request.setAttribute("diagnosis", diag);
        } else if(action.equals("dismiss")){
            SessionDB sessionDB = new SessionDB();
            sessionDB.changeStatus("Cleared", session);
            url = "/NewPatientsList";
        } else if(action.equals("prescription&NotSave")){
            String status = new SessionDB().getStatus(session);
            if(!"Admitted".equals(status)){
                String button = "<button class=\"btn btn-warning\" type=\"submit\" name=\"action\" value=\"admit\">Admit patient</button>";
                request.setAttribute("button", button);
            }
            if(status.equals("Admitted")){
                PrescriptionData dpData = new PrescriptionDB().getPrescriptionDiag(session);
                request.setAttribute("prescription", dpData.getPrescription());
            }
            
            ChiefComplainsData ccData;
            ccData = new ChiefComplainsDB().getChiefComplain(session);
            String complains = ccData.getChiefComplains();
            String diag = ccData.getDiagnosis();
            request.setAttribute("complains", complains);
            request.setAttribute("diagnosis", diag);
            url = "/Prescription.jsp";
        } else if(action.equals("orderLab&NotSave")){
            ChiefComplainsData ccData;
            ccData = new ChiefComplainsDB().getChiefComplain(session);
            String complains = ccData.getChiefComplains();
            String diag = ccData.getDiagnosis();
            request.setAttribute("complains", complains);
            request.setAttribute("diagnosis", diag);
            url = "/LabOrder.jsp";
        } else if(action.equals("viewLabResults")){
            request.setAttribute("action", "view");
            String cc = request.getParameter("complains");
            request.setAttribute("ChiefComplains", cc);
            url = "/LabResult";
        } else {
            url = "/index.jsp";
        }
        
        request.setAttribute("gender", gender);
        request.setAttribute("bloodGroup", bloodGroup);
        request.setAttribute("date", age);
        request.setAttribute("Name", name);
        request.setAttribute("pRef", pRef);
        request.setAttribute("session", session);
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
