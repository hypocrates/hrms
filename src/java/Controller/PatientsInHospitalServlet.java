/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.HospitalPatients;
import Bean.PatientData;
import Bean.Session;
import DbClasses.SessionDB;
import DbClasses.SearchDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class PatientsInHospitalServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String url = "/PatientsInHospital.jsp";
        String message = "";
        String actions = request.getParameter("action");
        SessionDB sessDB = new SessionDB();
        SearchDB search = new SearchDB();
        PatientData pData = new PatientData();
        Session sess = new Session();
        ArrayList<Session> session = new ArrayList<>();
        ArrayList<HospitalPatients> patients = new ArrayList<>();
        
        //create the first table
        session = sessDB.patientsInHospital(1);
        if (session != null){
            int sizes = session.size();
            for(int x = 0; x < sizes; x++){
                sess = session.get(x);
                pData = search.getPatientName(sess.getPatient());
                HospitalPatients patient = new HospitalPatients();
                String action;
                if(pData!=null){
                    //patients data found
                    String name = pData.getSirName()+" "+pData.getFirstName()+" "+pData.getLastName()+" "+pData.getMiddleName();
                    patient.setNationalID(pData.getNationalID());
                    patient.setName(name);
                    patient.setStatus(sess.getType());
                    patient.setCategory(sess.getStatus());
                    if(sess.getStatus().equals("Cleared")){
                        String Button = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#dismissModal\">End the patients Session</button>\n" +
"                            <div class=\"example-modal\">\n" +
"                                <div class=\"modal modal-warning fade\" id=\"dismissModal\" role=\"dialog\">\n" +
"                                    <div class=\"modal-dialog\">\n" +
"                                        <div class=\"modal-content\">\n" +
"                                            <div class=\"modal-header\">\n" +
"                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n" +
"                                                    <span aria-hidden=\"true\">&times;</span></button>\n" +
"                                                <h4 class=\"modal-title\">END SESSION</h4>\n" +
"                                            </div>\n" +
"                                            <div class=\"modal-body\">\n" +
"                                                <p>Are you sure you want to end <b>"+name+"</b> session</p>\n" +
"                                            </div>\n" +
"                                            <div class=\"modal-footer\">\n" +
"                                                <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Continue with session</button>\n" +
"                                                <form action=\"CloseSession\" method=\"post\">\n" +
"                                                    <input type=\"hidden\" name=\"session\" value=\""+sess.getSession()+"\">\n" +
"                                                    <button type=\"submit\" name=\"action\" value=\"dismiss\" class=\"btn btn-danger\">End The Session</button>\n" +
"                                                </form>\n" +
"                                            </div>\n" +
"                                        </div>\n" +
"                                    <!-- /.modal-content -->\n" +
"                                    </div>\n" +
"                                  <!-- /.modal-dialog -->\n" +
"                                </div>\n" +
"                                <!-- /.modal -->\n" +
"                              </div>\n" +
"                            <!-- /.example-modal -->";
                        action = Button;
                    } else {
                        action = "Waiting for Clearance";
                    }
                    patient.setAction(action);
                } else{
                    //patients data missing
                    message += "\nError Getting Patients number " +sess.getPatient()+" records.";
                    patient.setNationalID(000);
                    patient.setName("unkown");
                    patient.setCategory(sess.getStatus());
                    if(sess.getStatus().equals("Cleared")){
                        String Button = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#dismissModal\">End the patients Session</button>\n" +
"                            <div class=\"example-modal\">\n" +
"                                <div class=\"modal modal-warning fade\" id=\"dismissModal\" role=\"dialog\">\n" +
"                                    <div class=\"modal-dialog\">\n" +
"                                        <div class=\"modal-content\">\n" +
"                                            <div class=\"modal-header\">\n" +
"                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n" +
"                                                    <span aria-hidden=\"true\">&times;</span></button>\n" +
"                                                <h4 class=\"modal-title\">END SESSION</h4>\n" +
"                                            </div>\n" +
"                                            <div class=\"modal-body\">\n" +
"                                                <p>Are you sure you want to end <b>unkown patient's</b> session</p>\n" +
"                                            </div>\n" +
"                                            <div class=\"modal-footer\">\n" +
"                                                <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Continue with session</button>\n" +
"                                                <form action=\"CloseSession\" method=\"post\">\n" +
"                                                    <input type=\"hidden\" name=\"session\" value=\""+sess.getSession()+"\">\n" +
"                                                    <button type=\"submit\" name=\"action\" value=\"dismiss\" class=\"btn btn-danger\">End The Session</button>\n" +
"                                                </form>\n" +
"                                            </div>\n" +
"                                        </div>\n" +
"                                    <!-- /.modal-content -->\n" +
"                                    </div>\n" +
"                                  <!-- /.modal-dialog -->\n" +
"                                </div>\n" +
"                                <!-- /.modal -->\n" +
"                              </div>\n" +
"                            <!-- /.example-modal -->";
                        action = Button;
                    } else {
                        action = "Waiting for Clearance";
                    }
                    patient.setAction(action);
                }
                patients.add(patient);
            }
        }
        if (patients != null){
            int size = patients.size();
            if(message == ""){
                request.setAttribute("size", size);
                request.setAttribute("patients", patients);
            } else {
                request.setAttribute("message", message);
                request.setAttribute("size", size);
                request.setAttribute("patients", patients);
            }

        } else {
            message = "Error occured";
            request.setAttribute("message", message);
        }
        
        
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
