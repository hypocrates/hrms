/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.AssignDoctorData;
import Bean.BioData;
import Bean.ContactData;
import Bean.PatientData;
import Bean.Session;
import DbClasses.SavePatientDB;
import DbClasses.SearchDB;
import DbClasses.SessionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class SearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        String message;
        String disabled;
        String AddButton = "<br><button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\">Click to enter Patients Data</button>";
        String action = request.getParameter("action");
        SearchDB searchDB = new SearchDB();
        PatientData pData = new PatientData();
        SessionDB sessionDB = new SessionDB();
        HttpSession sessn = request.getSession(false);
        int hRef =(int) sessn.getAttribute("hRef");
        //Search Doctors
        ArrayList<AssignDoctorData> aDataArray = searchDB.getDoctorsAssign(hRef);
        int size = aDataArray.size();
        request.setAttribute("size", size);
        request.setAttribute("aDataArray", aDataArray);
        
        if (action.equals("")){
            url = "/NewPatient.jsp";
        } 
        else if(action.equals("search")){
            // Search for patient code
            String id = request.getParameter("id");
            int nationalID;
            if(id == ""){
                message = "<div class=\"callout callout-warning\">\n" +
                    "        <h4>Reminder!</h4>\n" +
                    "        Enter National ID before Searching\n" +
                    "      </div>";
                request.setAttribute("message", message);
                url = "/NewPatient.jsp";
            } else {
                nationalID = Integer.parseInt(id);
                //search for patient in database

                pData = searchDB.getPatientData(nationalID);
                if(pData!=null){
                    message = "<div class=\"callout callout-success\">\n" +
                    "        <h4>Huraaay!</h4>\n" +
                    "        We found the patient's Data.\n" +
                    "      </div>";
                    disabled = "disabled";
                    //Search for Bio Data in DB
                    BioData bioData = new BioData();
                    bioData = searchDB.getBioData(pData.getpRef());
                    //Search of contact data in DB
                    ContactData cData = new ContactData();
                    cData = searchDB.getContactData(pData.getpRef());
                    
                    //Set objects for jsp
                    
                    request.setAttribute("pData", pData);
                    request.setAttribute("bData", bioData);
                    request.setAttribute("cData", cData);
                    String button = "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#doctorsModal\">Create Session</button>";
                    request.setAttribute("button", button);
                    
                } else {
                    //if data was not found
                    message = "<div class=\"callout callout-info\">\n" +
                    "        <h4>Ooops!</h4>\n" +
                    "        Patient Data was not found. Feel free to add the data.\n" +
                    "      </div>";
                    disabled = "disabled";
                    request.setAttribute("AddButton", AddButton);
                    request.setAttribute("nationalID", nationalID);
                }
                
                    request.setAttribute("message", message);
                    request.setAttribute("disabled", disabled);
                    url = "/NewPatient.jsp";
            }
            
            
            
        } else if(action.equals("dismiss")){            //dismiss patient button
            message = "Patient Dismissed.";
            url = "/NewPatient.jsp";
        } else if(action.equals("createSession")){       //Create session button   
            //store an instance in the db
            String docName = request.getParameter("docName");
            int dRef = Integer.parseInt(request.getParameter("dRef"));
            int patient = Integer.parseInt(request.getParameter("pRef"));
            int hospital = hRef;
            int count = sessionDB.checkSession(patient, hospital);
            if(count!=0){
                //Reject session creation of there is an ongoing session
                message = "<div class=\"callout callout-warning\">\n" +
                    "        <h4>Ooops!</h4>\n" +
                    "        This patient seems to already have an ongoing session in this hospital.\n" +
                    "      </div>";;
                request.setAttribute("message", message);
                url = "/NewPatient.jsp";
            } else if(count == 999){
                message = "<div class=\"callout callout-warning\">\n" +
                    "        <h4>This is embarassing!</h4>\n" +
                    "        The my database seems to be failing. Feel free to report me.\n" +
                    "      </div>";;
                request.setAttribute("message", message);
                url = "/NewPatient.jsp";
            }else{
                //Create session if there is none in progress
                Session session = new Session();
                session.setdRef(dRef);
                session.setPatient(patient);
                session.setHospital(hospital);
                session.setStatus("Waiting to see the Doctor");
                session.setType("Out_Patient");

                int db = sessionDB.createSession(session);
                if(db != 0){
                    message = "<div class=\"callout callout-success\">\n" +
                    "        <h4>Wow!</h4>\n" +
                    "        That patient's session was created for Dr."+docName+".\n" +
                    "      </div>";
                    
                } else{
                    message = "<div class=\"callout callout-warning\">\n" +
                    "        <h4>Ooops!</h4>\n" +
                    "        That patient session was not started. Call my maker to check out why.\n" +
                    "      </div>";
                }
                request.setAttribute("message", message);
                url = "/NewPatient.jsp";
            }
        }else if(action.equals("add")){
            //get Patients data
            int nationalID = Integer.parseInt(request.getParameter("NationalID"));
            String firstName = request.getParameter("FirstName");
            String sirName = request.getParameter("SirName");
            String middleName = request.getParameter("MiddleName");
            String lastName = request.getParameter("LastName");
            // store patients data in bean
            pData= new PatientData(0, nationalID, sirName, firstName, middleName, lastName);
            //get bio data
            String dob = request.getParameter("DoB");
            String gender = request.getParameter("Gender");
            String maritalStatus = request.getParameter("MaritalStatus");
            int children = Integer.parseInt(request.getParameter("Children"));
            String bloodGroup = request.getParameter("BloodGroup");
            //store in bean
            BioData bioData = new BioData(0, dob, gender,maritalStatus, children, bloodGroup);
            //get contact data
            String occupation = request.getParameter("Occupation");
            String county = request.getParameter("County");
            String residence = request.getParameter("Place");
            String phoneNumber = request.getParameter("Telephone");
            String email = request.getParameter("Email");
            //store in bean
            ContactData cData = new ContactData(0, occupation, county, residence, phoneNumber, email);
            
            //store data in db
            SavePatientDB spd = new SavePatientDB();
            //store pateient data
            int pRef = spd.savePersonalDetails(pData);
            if (pRef != 0){
                pData.setpRef(pRef);
                int brows = spd.saveBioData(pRef, bioData);
                if(brows != 0){
                    int cRows = spd.saveContactData(pRef, cData);
                    if(cRows != 0){
                        message = "<div class=\"callout callout-success\">\n" +
                            "        <h4>Wow!</h4>\n" +
                            "        That patient's Data was stored successfully.\n" +
                            "      </div>";
                        String button = "<button type=\"submit\" name=\"action\" value=\"createSession\" class=\"btn btn-primary \">Create Session</button>";
                        request.setAttribute("disabled", "disabled");
                        request.setAttribute("button", button);
                        request.setAttribute("pData", pData);
                        request.setAttribute("bData", bioData);
                        request.setAttribute("cData", cData);
                    } else {
                        message = "<div class=\"callout callout-warning\">\n" +
                            "        <h4>Oops!</h4>\n" +
                            "        That patient's Contact data was not stored due to some system error.\n" +
                            "      </div>";
                    }
                } else {
                    message = "<div class=\"callout callout-warning\">\n" +
                        "        <h4>Oops!</h4>\n" +
                        "        That patient's Bio data and Contact data was not stored due to some system error.\n" +
                        "      </div>";
                }
            } else {
                message = "<div class=\"callout callout-warning\">\n" +
                    "        <h4>Oops!</h4>\n" +
                    "        That patient's data was not stored due to some system error.\n" +
                    "      </div>";
            }
            request.setAttribute("message", message);
            url = "/NewPatient.jsp";
        }
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
