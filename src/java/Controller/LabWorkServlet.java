/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import API.AgeCalculator;
import Bean.BioData;
import Bean.CardicTestData;
import Bean.ChiefComplainsData;
import Bean.CreatinineTestData;
import Bean.ElectrolytesTestData;
import Bean.GeneralTestData;
import Bean.LiverTestData;
import Bean.OtherTestData;
import Bean.PatientData;
import DbClasses.ChiefComplainsDB;
import DbClasses.SearchDB;
import DbClasses.SessionDB;
import DbClasses.TestDataDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class LabWorkServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.jsp";
        int session = Integer.parseInt(request.getParameter("session"));            
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        String action = request.getParameter("action");
        TestDataDB tdd = new TestDataDB();
        PatientData pData;
        SearchDB searchDB = new SearchDB();
        pData = searchDB.getPatientName(pRef);
        String name = pData.getSirName()+" "+pData.getFirstName()+" "+pData.getLastName()+" "+pData.getMiddleName();
        BioData bioData;
        bioData = searchDB.getBioData(pRef);
        String gender = bioData.getGender();
        String bloodGroup = bioData.getBloodGroup();
        String dOB = bioData.getDob();
        int age = AgeCalculator.calculateAge(dOB);
        
        CardicTestData ctData = tdd.getCardicTest(session);
        CreatinineTestData crData = tdd.getCreatinineTest(session);
        ElectrolytesTestData etData = tdd.getElectrolytesTest(session);
        GeneralTestData gtData = tdd.getGeneralTest(session);
        LiverTestData ltData = tdd.getLiverTest(session);
        OtherTestData otData = tdd.getOtherTest(session); 

        request.setAttribute("ctData", ctData);
        request.setAttribute("crData", crData);
        request.setAttribute("etData", etData);
        request.setAttribute("gtData", gtData);
        request.setAttribute("ltData", ltData);
        request.setAttribute("otData", otData);

        switch (action) {
            case "Send":
                int rows = new SessionDB().changeStatus("Received Lab Results", session);
                if(rows > 0){
                    url = "/LabPatientsList";
                    String message = "Lab Results sent Successfully";
                    String text = "<p class=\"alert alert-info\">"+message+"</p>";
                    request.setAttribute("text", text);
                } else{
                    url = "/LabPatientsList";
                    String message = "Error Occured Sending Results";
                    String text = "<p class=\"alert alert-warning\">"+message+"</p>";
                    request.setAttribute("text", text);
                }
                break;
            case "GeneralForm":
                url = "/LabOrderForm.jsp";
                break;
            case "LipidForm":
                url = "/LipidProfileForm.jsp";
                break;
            case "PsaForm":
                url = "/PSAform.jsp";
                break;
            case "ThyroidForm":
                url = "/ThyroidFunctionForm.jsp";
                break;
            default:
                break;
        }
        request.setAttribute("Name", name);
        request.setAttribute("gender", gender);
        request.setAttribute("bloodGroup", bloodGroup);
        request.setAttribute("date", age);
        request.setAttribute("pRef", pRef);
        request.setAttribute("session", session);
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
