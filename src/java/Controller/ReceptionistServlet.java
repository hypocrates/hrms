/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.PatientData;
import Bean.Session;
import DbClasses.SessionDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class ReceptionistServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/NewPatient.jsp";
        String message;
        String action = request.getParameter("action");
        
        if(action.equals(null)){
            url = "/NewPatient.jsp";
        } else if(action.equals("dismiss")){
            message = "Patient Dismissed.";
            url = "/NewPatient.jsp";
        } else if(action.equals("createSession")){
            //store an instance in the db
            
            SessionDB sessionDB = new SessionDB();
            int patient = Integer.parseInt(request.getParameter("pref"));
            int hospital = 1;
            Session session = new Session();
            session.setPatient(patient);
            session.setHospital(hospital);
            session.setType("Out_Patient");
            
            int db = sessionDB.createSession(session);
            System.out.println("result of Session creation is " +db);
            if(db != 0){
                message = "Session Started Successfully";
            } else{
                message = "Session was not Started";
            }
            request.setAttribute("message2", message);
            url = "/NewPatient.jsp";
        }
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
