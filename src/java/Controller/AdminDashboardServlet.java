/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.Year;
import DbClasses.ChartDB;
import DbClasses.DashboardDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Beast
 */
public class AdminDashboardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/Admins Dashboard.jsp";
        DashboardDB dbDB = new DashboardDB();
        String message = null;
        String commentMessage = null;
        if(request.getAttribute("message")!= null){
            message = (String) request.getAttribute("message");
        } else if(request.getParameter("message")!= null){
            message = request.getParameter("message");
        }
        if(request.getAttribute("commentMessage")!= null){
            commentMessage = (String) request.getAttribute("commentMessage");
        } else if(request.getParameter("commentMessage")!= null){
            commentMessage = request.getParameter("commentMessage");
        }
        
        
        HttpSession session = request.getSession(false);
        int dRef =(int) session.getAttribute("dRef");
        int hRef =(int) session.getAttribute("hRef");
        
        
        ChartDB chartDB = new ChartDB();
        Year year = new Year();
//        
//        year = chartDB.getDoctorsData(dRef, hRef);
        Year thisYearPatients = chartDB.getPatientsData(hRef);
        Year lastYearPatients = chartDB.getPatientsDataLastYear(hRef);
        
        request.setAttribute("lastYearPatients", lastYearPatients);
        request.setAttribute("thisYearPatients", thisYearPatients);

        int patientsInHospital = dbDB.getNumberOfHospitalPatients(hRef);
        int doctorsInHospital = dbDB.getNumberOfHospitalDoctors(hRef);
        int nursesInHospital = dbDB.getNumberOfHospitalNurses(hRef);
        int labAttendantsInHospital = dbDB.getNumberOfHospitalLabTechs(hRef);
        
        //Set to request
        request.setAttribute("message", message);
        request.setAttribute("commentMessage", commentMessage);
        request.setAttribute("patients", patientsInHospital);
        request.setAttribute("doctors", doctorsInHospital);
        request.setAttribute("nurses", nursesInHospital);
        request.setAttribute("labAttendants", labAttendantsInHospital);
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
