/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import API.AgeCalculator;
import Bean.BioData;
import Bean.ChiefComplainsData;
import Bean.PatientData;
import Bean.PrescriptionData;
import DbClasses.ChiefComplainsDB;
import DbClasses.PrescriptionDB;
import DbClasses.SearchDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class AdmittedPatientServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        String action = request.getParameter("action");
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        int session = Integer.parseInt(request.getParameter("session"));
        PatientData pData= new PatientData();
        SearchDB searchDB = new SearchDB();
        pData = searchDB.getPatientName(pRef);
        String name = pData.getSirName()+" "+pData.getFirstName()+" "+pData.getLastName()+" "+pData.getMiddleName();
        if(action.equals("details")){
            // open the jsp
            BioData bioData = new BioData();
            bioData = searchDB.getBioData(pRef);
            String gender = bioData.getGender();
            String bloodGroup = bioData.getBloodGroup();
            String dOB = bioData.getDob();
            int age = AgeCalculator.calculateAge(dOB);
            ChiefComplainsData ccData = new ChiefComplainsDB().getChiefComplain(session);
            PrescriptionData dpData = new PrescriptionDB().getPrescriptionDiag(session);
            //set Attributes
            request.setAttribute("diagnosis", dpData.getDiagnosis());
            request.setAttribute("prescription", dpData.getPrescription());
            request.setAttribute("complains", ccData.getChiefComplains());
            request.setAttribute("Name", name);
            request.setAttribute("gender", gender);
            request.setAttribute("bloodGroup", bloodGroup);
            request.setAttribute("date", age);
            request.setAttribute("pRef", pRef);
            request.setAttribute("session", session);
            url = "/AdmittedDetails.jsp";
        }
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
