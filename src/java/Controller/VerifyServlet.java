/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import API.PasswordUtility;
import Bean.HospitalData;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;;
import Bean.Login;
import DbClasses.LoginDB;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;


/**
 *
 * @author Wamuyu
 */
public class VerifyServlet extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        String url = "/index.jsp";
        String action = request.getParameter("action");
        String message = "";
        
        if(action.equals("Login")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String results;
            
            Login verify = new Login();
            
            LoginDB log = new LoginDB();
            
            int dRef = log.getDRef(email);
            if (dRef == 0){
                message = "No such user in the System";
            } else {
                String salt = log.getSalt(dRef);
                String hashedPassword;
                try{
                    hashedPassword = new PasswordUtility().hashAndSaltPassword(password, salt);
                    System.out.println(hashedPassword);
                    verify.setDRef(dRef);
                    verify.setPassword(hashedPassword);
                } catch(NoSuchAlgorithmException e){
                    e.printStackTrace();
                    message = "Error Occurred with password hashing. Contact admin.";
                }
            }
            
            results = log.AuthenticateUser(verify);
            if(results.equals("SUCCESS")){
                HttpSession session = request.getSession(false);
                if(session!=null && !session.isNew()){
                    
                    String name = log.getName(email);
                    dRef = log.getDRef(email);
                    int pwCheck = log.getPasswordCheck(dRef);
                    System.out.println("pCheck = " +pwCheck);
                    if(pwCheck == 0){
                        message = "Warning. You have not changed your "
                                + "password from the initial default Password. "
                                + "To change click on profile button on the header and select Change Password.";
                        request.setAttribute("message", message);
                    }
                    session.setAttribute("email", email);
                    session.setAttribute("user", name);
                    session.setAttribute("dRef", dRef);
                    String access = log.getType(email);
                    session.setAttribute("access", access);
                    ArrayList<HospitalData> hpArray = log.getHospital(email);
                    int hpNumber = hpArray.size();
                    if(hpNumber == 1){
                        HospitalData hpData = hpArray.get(0);
                        String hpName = hpData.getName();
                        String hpLocation = hpData.getLocation();
                        if(access.equals("Doctor")){
                            url = "/DoctorsDashboard";
                        } else if(access.equals("Nurse")){
                            url = "/Nurses Dashboard.jsp";
                        } else if(access.equals("Admin")){
                            url = "/AdminDashboard";
                        } else if(access.equals("LabTech")){
                            url = "/LabTechs Dashboard.jsp";
                        } else if(access.equals("Root")){
                            url = "/index.jsp";
                        }
                        
                        session.setAttribute("HospitalName", hpName);
                        session.setAttribute("hRef", hpData.gethRef());
                        request.setAttribute("hpArray", hpArray);
                        request.setAttribute("size", hpNumber);
                    } else{
                        request.setAttribute("hpArray", hpArray);
                        request.setAttribute("size", hpNumber);
                        url = "/index.jsp";
                    }
                    
                    //response.sendRedirect("admin");
               
                }else{
                    request.setAttribute("message", message);
                    response.sendRedirect("login");
                }
            } else{
                message = "Wrong Password. AUTHENTICATION failed!!";
                request.setAttribute("errMessage", results);
                request.setAttribute("message", message);
                url="WEB-INF/web/login.jsp";
//                request.getRequestDispatcher("WEB-INF/web/login.jsp").forward(request, response);
            }
        } else{
            url = "WEB-INF/web/login.jsp";
        }
         
        request.getRequestDispatcher(url).forward(request, response);
    }
}
