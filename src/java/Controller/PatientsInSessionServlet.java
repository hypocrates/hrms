/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import API.AgeCalculator;
import Bean.BioData;
import Bean.ChiefComplainsData;
import Bean.PatientData;
import DbClasses.ChiefComplainsDB;
import DbClasses.SearchDB;
import DbClasses.SessionDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class PatientsInSessionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url;
        String action = request.getParameter("action");
        
        if("save".equals(action)){
            action = (String) request.getAttribute("action");
        }
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        int session = Integer.parseInt(request.getParameter("session"));
        PatientData pData= new PatientData();
        SearchDB searchDB = new SearchDB();
        pData = searchDB.getPatientName(pRef);
        String name = pData.getSirName()+" "+pData.getFirstName()+" "+pData.getLastName()+" "+pData.getMiddleName();
        System.out.println("uko sawa naa..."  + action);
        if(null == action){
            url = "/PatientsDetails.jsp";
        } else switch (action) {
            case "details":
                // open the jsp
                request.setAttribute("pRef", pRef);
                request.setAttribute("session", session);
                url = "/PatientsDetails.jsp";
                break;
            case "medicalHistory":
                System.out.println("uko sawa naa...");
                request.setAttribute("action", "List");
                url = "/MedicalHistory";
                break;
            case "continueSession":
                BioData bioData = new BioData();
                bioData = searchDB.getBioData(pRef);
                String gender = bioData.getGender();
                String bloodGroup = bioData.getBloodGroup();
                String dOB = bioData.getDob();
                int age = AgeCalculator.calculateAge(dOB);
                ChiefComplainsData ccData = new ChiefComplainsDB().getChiefComplain(session);
                request.setAttribute("complains", ccData.getChiefComplains());
                request.setAttribute("diagnosis", ccData.getDiagnosis());
                request.setAttribute("gender", gender);
                request.setAttribute("bloodGroup", bloodGroup);
                request.setAttribute("date", age);
                String labStatus = new SessionDB().getStatus(session);
                String labResultsButton;
                if(labStatus.equals("Received Lab Results")){
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\">View Lab Results</button>";
                } else
                    labResultsButton = "<button type=\"submit\" name=\"action\" value=\"viewLabResults\" class=\"btn btn-warning\" disabled>View Lab Results</button>";
                String labOrderButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#labOrderModal\">Order for Lab Work</button>";
                String prescriptionButton = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#prescriptionModal\">Prescription</button>";
                request.setAttribute("labOrderButton", labOrderButton);
                request.setAttribute("prescriptionButton", prescriptionButton);
                request.setAttribute("labResultsButton", labResultsButton);
                url = "/ChiefComplains.jsp";
                break;
            case "clearForExit":
                SessionDB sessionDB = new SessionDB();
                String status = "Cleared";
                int rows = sessionDB.changeStatus(status, session);
                if(rows!=0){
                    url = "/NewPatientsList";
                } else{
                    System.out.println("Error clearing Session");
                    url = "/NewPatientsList";
                }   break;
            default:
                url = "/index.jsp";
                break;
        }
        request.setAttribute("Name", name);
        request.setAttribute("pRef", pRef);
        request.setAttribute("session", session);
       getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
