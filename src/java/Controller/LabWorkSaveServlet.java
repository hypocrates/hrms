/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.CardicTests;
import Bean.CreatinineTests;
import Bean.ElectrolytesTests;
import Bean.GeneralTests;
import Bean.LipidProfileTests;
import Bean.LiverFunctionTests;
import Bean.ProstaticTests;
import Bean.ThyroidFunctionTests;
import DbClasses.LabOrderDB;
import DbClasses.LabResultDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class LabWorkSaveServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url;
        String message = "";
        
        int session = Integer.parseInt(request.getParameter("session"));            
        int pRef = Integer.parseInt(request.getParameter("pRef"));
        String action = request.getParameter("action");
        String test = request.getParameter("test");
        LabResultDB lrDB = new LabResultDB();
        LabOrderDB loDB = new LabOrderDB();
        HttpSession sessn = request.getSession(false);
        int dRef =(int) sessn.getAttribute("dRef");
        int hRef =(int) sessn.getAttribute("hRef");
        
        switch (action) {
            case "GeneralTest":
                int checker = 1;
                String bSugar = request.getParameter("bSugar");
                String urea = request.getParameter("urea");
                String uricAcid = request.getParameter("uricAcid");
                String calcium = request.getParameter("calcium");
                String acidPhosphate = request.getParameter("acidPhosphate");
                String cholesterol = request.getParameter("cholesterol");
                String amylase = request.getParameter("amylase");
                String phosphorus = request.getParameter("phosphorus");
                String totalProtein = request.getParameter("tProtein");
                String albumen = request.getParameter("albumen");
                String globulin = request.getParameter("globulin");
                GeneralTests gTests = new GeneralTests();
                if(!"".equals(bSugar)){
                    gTests.setBloodSugar(bSugar);
                }   if(!"".equals(urea)){
                    gTests.setUrea(urea);
                }   if(!"".equals(uricAcid)){
                    gTests.setUricAcid(uricAcid);
                }   if(!"".equals(calcium)){
                    gTests.setCalcium(calcium);
                }   if(!"".equals(acidPhosphate)){
                    gTests.setAcidPhosphate(acidPhosphate);
                }   if(!"".equals(cholesterol)){
                    gTests.setCholesterol(cholesterol);
                }   if(!"".equals(amylase)){
                    gTests.setAmylase(amylase);
                }   if(!"".equals(phosphorus)){
                    gTests.setPhosphorus(phosphorus);
                }   if(!"".equals(totalProtein)){
                    gTests.setTotalProtein(totalProtein);
                }   if(!"".equals(albumen)){
                    gTests.setAlbumen(albumen);
                }   if(!"".equals(globulin)){
                    gTests.setGlobulin(globulin);
                }   int bRows = lrDB.saveBloodWork(session, dRef, gTests);
                if(bRows > 0){
                    message += "%n Blood Work Results sent Successfully";
                    checker *= 1;
                }else{
                    message += "%n ERROR SENDING BLOOD RESULTS";
                    checker *= 0;
                }   // Electrolytes Save Code
                String sodium = request.getParameter("sodium");
                String potassium = request.getParameter("potassium");
                String chloride = request.getParameter("chloride");
                String bicarbonate = request.getParameter("bicarbonate");
                ElectrolytesTests eTests = new ElectrolytesTests();
                if(!"".equals(sodium)){
                    eTests.setSodium(sodium);
                }   if(!"".equals(potassium)){
                    eTests.setPotassium(potassium);
                }   if(!"".equals(chloride)){
                    eTests.setChloride(chloride);
                }   if(!"".equals(bicarbonate)){
                    eTests.setBicarbonate(bicarbonate);
                }   int eRows = lrDB.saveElectrolytes(session, dRef, eTests);
                if(eRows > 0) {
                    message += "%n Electrolytes Result Sent Successfully";
                    checker *= 1;
                } else{
                    message += "%n ERROR SENDING ELECTROLYTES RESULTS";
                    checker *= 0;
                }   //LiverFuction Test save
                String bTotal = request.getParameter("bTotal");
                String bDirect = request.getParameter("bDirect");
                String bIndirect = request.getParameter("bIndirect");
                String alkaline = request.getParameter("alkalinePhosphate");
                String SGOT = request.getParameter("sgot");
                String SGPT = request.getParameter("sgpt");
                String YGT = request.getParameter("ygt");
                LiverFunctionTests lfTests = new LiverFunctionTests();
                if(!"".equals(bTotal)){
                    lfTests.setbTotal(bTotal);
                }   if(!"".equals(bDirect)){
                    lfTests.setbDirect(bDirect);
                }   if(!"".equals(bIndirect)){
                    lfTests.setbIndirect(bIndirect);
                }   if(!"".equals(alkaline)){
                    lfTests.setAlkaline(alkaline);
                }   if(!"".equals(SGOT)){
                    lfTests.setSGOT(SGOT);
                }   if(!"".equals(SGPT)){
                    lfTests.setSGPT(SGPT);
                }   if(!"".equals(YGT)){
                    lfTests.setYGT(YGT);
                }   int lfRows = lrDB.saveLiverTests(session, dRef, lfTests);
                if(lfRows > 0) {
                    message += "%n Liver Function Tests Result Sent Successfully";
                    checker *= 1;
                } else{
                    message += "%n ERROR SENDING LIVER FUNCTION TESTS RESULTS";
                    checker *= 0;
                }   //Cardic Tests Save
                
                String LDH = request.getParameter("ldh");
                String HBD = request.getParameter("hbd");
                String CPK = request.getParameter("cpk");
                String Sgot = request.getParameter("sgot2");
                CardicTests cTests = new CardicTests();
                if(!"".equals(LDH)){
                    cTests.setLDH(LDH);
                }   if(!"".equals(HBD)){
                    cTests.setHBD(HBD);
                }   if(!"".equals(CPK)){
                    cTests.setCPK(CPK);
                }   if(!"".equals(Sgot)){
                    cTests.setSGOT(Sgot);
                }   int cRows = lrDB.saveCardicTests(session, dRef, cTests);
                if(cRows > 0) {
                    message += "%n Cardic Tests Result Sent Successfully";
                    checker *= 1;
                } else{
                    message += "%n ERROR SENDING CARDIC TESTS RESULTS";
                    checker *= 0;
                }   // Creatinine
                String clearance = request.getParameter("clearance");
                String serum = request.getParameter("cSerum");
                String urine = request.getParameter("cUrine");
                CreatinineTests crTests = new CreatinineTests();
                if(!"".equals(clearance)){
                    crTests.setClearance(clearance);
                }   if(!"".equals(serum)){
                    crTests.setSerum(serum);
                }   if(!"".equals(urine)){
                    crTests.setUrine(urine);
                }   int crRows = lrDB.saveCreatinineTests(session, dRef, crTests);
                if(crRows > 0) {
                    message += "%n Creatinine Tests Result Sent Successfully";
                    checker *= 1;
                } else{
                    message += "%n ERROR SENDING CREATININE TESTS RESULTS";
                    checker *= 0;
                }   if(checker == 1){
                    int rows = loDB.updateOtherTestsNormal(session, 0);
                } else if(checker == 0){
                    int rows = loDB.updateOtherTestsNormal(session, 1);
                }   break;
            case "LipidProfileTest":
                // actions for storing lipids profile
                String totalCholesterol = request.getParameter("totalCholesterol");
                String hdlCholesterol = request.getParameter("hdlCholesterol");
                String ldlCholesterol = request.getParameter("ldlCholesterol");
                String triglycerides = request.getParameter("triglycerides");
                String tchdlRatio = request.getParameter("tchdlRatio");
                LipidProfileTests lpTests = new LipidProfileTests();
                if(!"".equals(totalCholesterol)){
                    lpTests.setTotalCholesterol(totalCholesterol);
                }   if(!"".equals(hdlCholesterol)){
                    lpTests.setHdlCholesterol(hdlCholesterol);
                }   if(!"".equals(ldlCholesterol)){
                    lpTests.setLdlCholesterol(ldlCholesterol);
                }   if(!"".equals(triglycerides)){
                    lpTests.setTriglycerides(triglycerides);
                }   if(!"".equals(tchdlRatio)){
                    lpTests.setTchdlRatio(tchdlRatio);
                }   int lpRows = lrDB.saveLipidProfileTest(session, dRef, lpTests);
                if(lpRows > 0) {
                    message += "%n Lipid Profile Tests Result Sent Successfully";
                    int rows = loDB.updateOtherTestsLipid(session, 0);
                } else{
                    message += "%n ERROR SENDING LIPID PROFILE TESTS RESULTS";
                }  break;
            case "ProstaticTest":
                //Prostatic Tests
                String prostatic = request.getParameter("psa");
                ProstaticTests pTests = new ProstaticTests();
                pTests.setProstatic(prostatic);
                int pRows = lrDB.saveProstaticTests(session, dRef, pTests);
                if(pRows > 0){
                    message += "%n Prostatic Specification Antigen Tests Result Sent Successfully";
                    int rows = loDB.updateOtherTestsPsa(session, 0);
                } else{
                    message += "%n ERROR SENDING PROSTATIC SPECIFICATION ANTIGEN TESTS RESULTS";
                }   break;
            case "ThyroidTest":
                // thyroid Tests
                String tshLevels = request.getParameter("tshLevels");
                String totalT3Levels = request.getParameter("totalT3Levels");
                String totalT4Levels = request.getParameter("totalT4Levels");
                ThyroidFunctionTests tfTests = new ThyroidFunctionTests();
                if(!"".equals(tshLevels)){
                    tfTests.setTshLevels(tshLevels);
                }   if(!"".equals(totalT3Levels)){
                    tfTests.setTotalT3Levels(totalT3Levels);
                }   if(!"".equals(totalT4Levels)){
                    tfTests.setTotalT4Levels(totalT4Levels);
                }   int tfRows = lrDB.saveThyroidTest(session, dRef, tfTests);
                if(tfRows > 0){
                    message += "%n Thyroid Function Tests Result Sent Successfully";
                    int rows = loDB.updateOtherTestsThyroid(session, 0);
                } else
                    message += "%n ERROR SENDING THYROID FUNCTION TESTS RESULTS";
                break;
            default:
                break;
        }
        
        String text = "<p class=\"alert alert-info\">"+message+"</p>";
        request.setAttribute("text", text);
        url = "/LabPatientsList";
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
