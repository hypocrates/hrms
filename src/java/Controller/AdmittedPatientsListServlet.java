/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.PatientData;
import Bean.PatientsList;
import Bean.Session;
import DbClasses.SearchDB;
import DbClasses.SessionDB;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class AdmittedPatientsListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/AdmittedPatients.jsp";
        String message;
        SessionDB sessionDB = new SessionDB();
        SearchDB search = new SearchDB();
        Session session = new Session();
        PatientData pData = new PatientData();
        ArrayList<Session> sessions = new ArrayList<>();
        ArrayList<PatientsList> patients = new ArrayList<>();
        
        //create the first table
        HttpSession sessn = request.getSession(false);
        int hRef =(int) sessn.getAttribute("hRef");
        sessions = sessionDB.getSessionThroType("Admitted", hRef);
        
        if (sessions != null){
            int sizes = sessions.size();
            for(int x = 0; x < sizes; x++){
                session = sessions.get(x);
                pData = search.getPatientName(session.getPatient());
                PatientsList patient = new PatientsList();
                if(pData!=null){
                    //patients data found
                    String name = pData.getSirName()+" "+pData.getFirstName()+" "+pData.getLastName()+" "+pData.getMiddleName();
                    patient.setNationalID(pData.getNationalID());
                    patient.setName(name);
                    int time = sessionDB.timeInHospital(session.getSession());
                    patient.setWaiting(time/86400);
                    String entryDate = sessionDB.entryDate(session.getSession());
                    patient.setEntryDate(entryDate);
                } else{
                    //patients data missing
                    String name = "unknown";
                    patient.setNationalID(000);
                    patient.setName(name);
//                    Time times = sessionDB.timeInHospital(pData.getpRef());
//                    patient.setTime(times);
                    patient.setWaiting(50);
                    
                }
                patient.setpRef(session.getPatient());
                patient.setSession(session.getSession());
                patients.add(patient);
            }
        } 
        if (patients != null){
            int size = patients.size();
            message = "Patients Waiting to see the Doctor.";
            request.setAttribute("message",message);
            request.setAttribute("size", size);
            request.setAttribute("patients", patients);
        } else {
            message = "No Patient currently Waiting.";
//            request.setAttribute("size", 0);
            request.setAttribute("message", message);

        }
        
        
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
