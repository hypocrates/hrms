/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.CardicTests;
import Bean.ChiefComplainsData;
import Bean.CreatinineTests;
import Bean.ElectrolytesTests;
import Bean.GeneralTests;
import Bean.LipidProfileTests;
import Bean.LiverFunctionTests;
import Bean.PrescriptionData;
import Bean.ProstaticTests;
import Bean.ThyroidFunctionTests;
import DbClasses.ChiefComplainsDB;
import DbClasses.LabResultDB;
import DbClasses.PrescriptionDB;
import DbClasses.SessionDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wamuyu
 */
public class LabResultServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "LabResult.jsp";
        int session;
        int pRef;
        String name;
        String age;
        String gender;
        String bloodGroup;
        String action;
        String complains;
        
        if(request.getAttribute("session") != null){
            session =(Integer) request.getAttribute("session");
            pRef = (Integer) request.getAttribute("pRef");
            name = (String) request.getAttribute("Name");
            age = (String) request.getAttribute("date");
            gender = (String) request.getAttribute("gender");
            bloodGroup = (String) request.getAttribute("bloodGroup");
            action = (String) request.getAttribute("action");
            complains = (String) request.getAttribute("ChiefComplains");
        } else{
            session = Integer.parseInt(request.getParameter("session"));
            pRef = Integer.parseInt(request.getParameter("pRef"));
            name = request.getParameter("name");
            age = request.getParameter("date");
            gender = request.getParameter("gender");
            bloodGroup = request.getParameter("bloodGroup");
            action = request.getParameter("action");
            complains = request.getParameter("ChiefComplains");
        }
        
        
        if(action.equals("view")){
            LabResultDB lrDB = new LabResultDB();
            
            GeneralTests gTests  =  lrDB.getBloodWork(session);
            ElectrolytesTests eTests = lrDB.getElectrolytesTests(session);
            LiverFunctionTests lfTests = lrDB.getLiverFunctionTests(session);
            CardicTests cTests = lrDB.getCardicTests(session);
            CreatinineTests crTests = lrDB.getCreatinineTests(session);
            LipidProfileTests lpTests = lrDB.getLipidProfileTests(session);
            ProstaticTests pTests = lrDB.getProstaticTests(session);
            ThyroidFunctionTests tfTests = lrDB.getThyroidFunctionTests(session);
            
            request.setAttribute("GeneralTest", gTests);
            request.setAttribute("ElectrolytesTest", eTests);
            request.setAttribute("LiverTest", lfTests);
            request.setAttribute("CardicTest", cTests);
            request.setAttribute("CreatinineTest", crTests);
            request.setAttribute("LipidTest", lpTests);
            request.setAttribute("ProstaticTest", pTests);
            request.setAttribute("ThyroidTest", tfTests);
            request.setAttribute("Complains", complains);
            
            url = "/LabResult.jsp";
        } else if(action.equals("labOrder")){
            String diag = request.getParameter("diagnosis");
            
            request.setAttribute("complains", complains);
            request.setAttribute("diagnosis", diag);
            url = "/LabOrder.jsp";
            
        } else if(action.equals("prescription")){
            String status = new SessionDB().getType(session);
            if(!"Admitted".equals(status)){
                String button = "<button class=\"btn btn-warning\" type=\"submit\" name=\"action\" value=\"admit\">Admit patient</button>";
                request.setAttribute("button", button);
            }
            if(status.equals("Admitted")){
                PrescriptionData dpData = new PrescriptionDB().getPrescriptionDiag(session);
                request.setAttribute("prescription", dpData.getPrescription());
            }
            String diag = request.getParameter("diagnosis");
            
            request.setAttribute("complains", complains);
            request.setAttribute("diagnosis", diag);
            System.out.println(complains);
            
            url = "/Prescription.jsp";
        }else if (action.equals("dismiss")){
            SessionDB sessionDB = new SessionDB();
            sessionDB.changeStatus("Cleared", session);
            url = "/NewPatientsList";
        }
        
        request.setAttribute("gender", gender);
        request.setAttribute("bloodGroup", bloodGroup);
        request.setAttribute("date", age);
        request.setAttribute("Name", name);
        request.setAttribute("pRef", pRef);
        request.setAttribute("session", session);
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
