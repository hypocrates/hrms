  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Bean.AdminsAddStuffData;
import Bean.UsersData;
import DbClasses.AdminDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wamuyu
 */
public class AdminServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/AdminDashboard";
        String action = request.getParameter("action");
        AdminsAddStuffData aasData;
        AdminDB adminDB = new AdminDB();
        String message = null;
        HttpSession session = request.getSession(false);
        int hRef =(int) session.getAttribute("hRef");
        
        if(action.equals("saveDoctor")){
            aasData = new AdminsAddStuffData();
            aasData.setNationalID(Integer.parseInt(request.getParameter("NationalID")));
            aasData.setSirName(request.getParameter("SirName"));
            aasData.setFirstName(request.getParameter("FirstName"));
            aasData.setOtherNames(request.getParameter("OtherName"));
            aasData.setSpecialisation(request.getParameter("Specialisation"));
            aasData.setDoB(request.getParameter("DoB"));
            aasData.setEmail(request.getParameter("Email"));
            
            
            
            int rows = adminDB.employDoctor(aasData, hRef);
            if( rows == 1){
                message = "Doctor added Successfully. Initial Password = email address";
            } else{
                message = "ERROR adding Doctor";
            }
        } else if(action.equals("saveNurse")){
            aasData = new AdminsAddStuffData();
            aasData.setNationalID(Integer.parseInt(request.getParameter("NationalID")));
            aasData.setSirName(request.getParameter("SirName"));
            aasData.setFirstName(request.getParameter("FirstName"));
            aasData.setOtherNames(request.getParameter("OtherName"));
            aasData.setSpecialisation(request.getParameter("Specialisation"));
            aasData.setDoB(request.getParameter("DoB"));
            aasData.setEmail(request.getParameter("Email"));
            
            int rows = adminDB.employNurse(aasData, hRef);
            if( rows == 1){
                message = "Nurse added Successfully. Initial Password = email address";
            } else{
                message = "ERROR adding Nurse";
            }
        } else if(action.equals("saveLabTech")){
            aasData = new AdminsAddStuffData();
            aasData.setNationalID(Integer.parseInt(request.getParameter("NationalID")));
            aasData.setSirName(request.getParameter("SirName"));
            aasData.setFirstName(request.getParameter("FirstName"));
            aasData.setOtherNames(request.getParameter("OtherName"));
            aasData.setSpecialisation(request.getParameter("Specialisation"));
            aasData.setDoB(request.getParameter("DoB"));
            aasData.setEmail(request.getParameter("Email"));
            
            int rows = adminDB.employLabTech(aasData, hRef);
            if( rows == 1){
                message = "Lab Technician added Successfully. Initial Password = email address";
            } else{
                message = "ERROR adding Lab Technician";
            }
        } else if(action.equals("viewDoctors")){
            ArrayList<UsersData> usArray = adminDB.getUsers("Doctor", hRef);
            int size = usArray.size();
            request.setAttribute("size", size);
            request.setAttribute("type", "Doctor");
            request.setAttribute("usArray", usArray);
            url = "/HospitalStaff.jsp";
        }else if(action.equals("viewLabTechs")){
            ArrayList<UsersData> usArray = adminDB.getUsers("LabTech", hRef);
            int size = usArray.size();
            request.setAttribute("size", size);
            request.setAttribute("type", "LabTech");
            request.setAttribute("usArray", usArray);
            url = "/HospitalStaff.jsp";
        } else if(action.equals("viewNurses")){
            ArrayList<UsersData> usArray = adminDB.getUsers("Nurse", hRef);
            int size = usArray.size();
            request.setAttribute("size", size);
            request.setAttribute("type", "Nurse");
            request.setAttribute("usArray", usArray);
            url = "/HospitalStaff.jsp";
        } else if(action.equals("dismiss")){
            int dRef = Integer.parseInt(request.getParameter("dRef"));
            String name = request.getParameter("Name");
            int rows = adminDB.dismissUser(dRef, hRef);
            if(rows == 1){
                message = name+" dismissed Successfully.";
            } else{
                message = "Error Occurred. Dismissing Failed.";
            }
            
            url = "/AdminDashboard";
        } 
        
        request.setAttribute("commentMessage", message);
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
