<%-- 
    Document   : NewPatientTest
    Created on : 10-May-2017, 15:28:50
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Patient
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patient</a></li>
        <li class="active">New Patient</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        ${message}
        <div class="patient">
            <form action="Search" method="post" class="form-group">
                <input type="hidden" name="action" value="add">
                <div class="row">

                    <div class="col-md-6 center input-group">
                        <br>
                        <img src="dist/img/avatar.png" class="img-circle" alt="Patients Image" height="230px" width="230px"><br>
                        <br>
                        <form action="Search" method="get" class="form-group">
                            <input type="hidden" name="action" value="search">
                            <div class="input-group">
                                <input type="text" name="id" class="form-control" placeholder="Search National ID..." value="${pData.nationalID}">
                                  <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                    </button>
                                  </span>
                            </div>
                        </form>
                    </div>
                </div>
            </form>
        </div>