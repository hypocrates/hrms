<%-- 
    Document   : PSAform
    Created on : 08-Jun-2017, 12:03:35
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laboratory Order
        <small>${message}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
      <div class="row">
            <br>
            <div class="col-md-8">
                <p><h2><b>Prostatic Specification Antigen</b></h2></p>
                <br><br>
                <div>
                    <label>Specimen</label>
                    <p>Blood</p>
                </div>
                <br><br>
                <div>
                    <form action="LabWorkSave" method="post">
                        <input type="hidden" name="session" value="${session}">
                        <input type="hidden" name="pRef" value="${pRef}">
                        <label>Total Prostatic Specification Antigen</label>
                        <input type="text" class="form-control" name="psa" placeholder="P.S.A.........<4.08ng/ml" required>
                        <br><br>
                        <button class="pull-left btn btn-primary" type="submit" name="action" value="back">&larr; Back to Patients Column</button>
                        <button class="pull-right btn btn-success" type="submit" name="action" value="ProstaticTest">Save Lab Result &rarr;</button>
                    </form>
                </div>
                <br>
                <br>
                <div>
                    <p>The clinical sensitivity of PSA for Ca Prostate detection at a cut off value of 4.0ng/ml are 78% and 33% respectively. Determination of free PSA and estimation of free PSA ration can be used to improve specifically in the diagnostic "gray" zone PSA between 4 and 20ng/ml.</p>
                </div>
                
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/LabOrder-end.jsp" %>