<%-- 
    Document   : ClearedPatients
    Created on : 27-Jun-2017, 00:46:25
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admitted Patients
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- Your Page Content Here -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><%=session.getAttribute("HospitalName")%><small>${message}</small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>National ID</th>
                            <th>Name</th>
                            <th>Entry Time</th>
                            <th>Time in Hospital</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%@page import="Bean.PatientsList, java.util.ArrayList" %>
                        <%
                            int size =(Integer) request.getAttribute("size");
                            ArrayList<PatientsList> patients =(ArrayList) request.getAttribute("patients");
                            //if (size < 15) {
                                for (int x = 0; x < size; x++) {
                                PatientsList patient = patients.get(x);
                                int pRef = patient.getpRef();
                                int nationalID = patient.getNationalID();
                                String name = patient.getName();
                                int waiting = patient.getWaiting();
                                String entryDate = patient.getEntryDate();
                                int sess = patient.getSession();

                        %>
                        <tr>
                            <td><%= nationalID %></td>
                            <td><%= name %></td>
                            <td><%= entryDate %></td>
                            <td><span class="badge bg-red"><%= waiting %> days</span></td>
                            <td><form action="ClearedPatients" method="post">
                                    <input type="hidden" name="pRef" value="<%= pRef %>">
                                    <input type="hidden" name="session" value="<%= sess %>">
                                    <button type="submit" name="action" value="details" class="btn btn-primary">View Prescription</button>
                                </form></td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                <tfoot>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <th>Entry Time</th>
                  <th>Time in Hospital</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


      
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/PatientsInHospital-end.jsp" %>