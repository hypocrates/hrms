<%-- 
    Document   : LabPatientsList
    Created on : 24-May-2017, 17:40:47
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laboratory Patients List
        <small>Patients in the Laboratory</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        ${text}
      <!-- Your Page Content Here -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><%= session.getAttribute("HospitalName")%> <small>${message}</small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>General Test</th>
                  <th>Lipid Profile</th>
                  <th>Thyroid Test</th>
                  <th>PSA Test</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <%@page import="Bean.LabPatientListData, java.util.ArrayList" %>
                    <%
                        int size =(Integer) request.getAttribute("size");
                        ArrayList<LabPatientListData> patients =(ArrayList) request.getAttribute("patients");
                        //if (size < 15) {
                            for (int x = 0; x < size; x++) {
                            LabPatientListData patient = patients.get(x);
                            int pRef = patient.getpRef();
                            int nationalID = patient.getNationalID();
                            String name = patient.getName();
                            String Status = patient.getStatus();
                            int sess = patient.getSession();
                            int generalTest = patient.getGeneral();
                            int lipidTest = patient.getLipid();
                            int thyroidTest = patient.getThyroid();
                            int psaTest = patient.getPsa();

                    %>
                    <tr>
                        <td><%= nationalID %></td>
                        <td><%= name %></td>
                        <td><%= Status %></td>
                        <td>
                            <%
                                if(generalTest == 1){
                            %>
                                <form action="LabWork" method="post">
                                    <input type="hidden" name="pRef" value="<%= pRef %>">
                                    <input type="hidden" name="session" value="<%= sess %>">
                                    <button type="submit" name="action" value="GeneralForm" class="btn btn-primary">Perform General Test</button>
                                </form>
                            <%
                                } else {
                            %>
                                <p>No General Test</p>
                            <%
                                }
                            %>    
                        </td>
                        <td>
                            <%
                                if(lipidTest == 1){
                            %>
                                <form action="LabWork" method="post">
                                    <input type="hidden" name="pRef" value="<%= pRef %>">
                                    <input type="hidden" name="session" value="<%= sess %>">
                                    <button type="submit" name="action" value="LipidForm" class="btn btn-primary">Perform Lipid Test</button>
                                </form>
                            <%
                                } else {
                            %>
                                <p>No lipid Test</p>
                            <%
                                }
                            %>    
                        </td>
                        <td>
                            <%
                                if(thyroidTest == 1){
                            %>
                                <form action="LabWork" method="post">
                                    <input type="hidden" name="pRef" value="<%= pRef %>">
                                    <input type="hidden" name="session" value="<%= sess %>">
                                    <button type="submit" name="action" value="ThyroidForm" class="btn btn-primary">Perform Thyroid Test</button>
                                </form>
                            <%
                                } else {
                            %>
                                <p>No Thyroid Test</p>
                            <%
                                }
                            %>    
                        </td>
                        <td>
                            <%
                                if(psaTest == 1){
                            %>
                                <form action="LabWork" method="post">
                                    <input type="hidden" name="pRef" value="<%= pRef %>">
                                    <input type="hidden" name="session" value="<%= sess %>">
                                    <button type="submit" name="action" value="PsaForm" class="btn btn-primary">Perform PSA Test</button>
                                </form>
                            <%
                                } else {
                            %>
                                <p>No PSA Test</p>
                            <%
                                }
                            %>    
                        </td>
                        <td>
                            <%
                                if(generalTest + lipidTest + thyroidTest + psaTest == 0){
                            %>
                                <form action="LabWork" method="post">
                                    <input type="hidden" name="pRef" value="<%= pRef %>">
                                    <input type="hidden" name="session" value="<%= sess %>">
                                    <button type="submit" name="action" value="Send" class="btn btn-success">Send Results</button>
                                </form>
                            <%
                                } else {
                            %>
                            <button type="button" name="action" value="" class="btn btn-warning" disabled>Send Results</button>
                            <%
                                }
                            %>
                        </td>
                        
                    </tr>
                    <%
                        }
                    %>
                </tbody>
                <tfoot>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>General Test</th>
                  <th>Lipid Profile</th>
                  <th>Thyroid Test</th>
                  <th>PSA Test</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/PatientsInHospital-end.jsp" %>