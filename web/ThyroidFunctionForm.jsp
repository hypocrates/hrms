<%-- 
    Document   : ThyroidFunctionForm
    Created on : 08-Jun-2017, 12:27:52
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laboratory Order
        <small>${message}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
      <div class="row">
            <br>
            <div class="col-md-8">
                <p><h2><b>Thyroid Function Test</b></h2></p>
            <form action="LabWorkSave" method="post">
                <input type="hidden" name="session" value="${session}">
                <input type="hidden" name="pRef" value="${pRef}">
                <input type="hidden" name="test" value="thyroidTest">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>TEST</th>
                                <th>RESULT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>TSH Levels</td>
                                <td><input type="text" class="form-control" name="tshLevels" placeholder="Normal Levels 0.3-6.2mlU/L" required></td>
                            </tr>
                            <tr>
                                <td>Total T3 Levels</td>
                                <td><input type="text" class="form-control" name="totalT3Levels" placeholder="Normal Levels 0.6-2.1ng/ml" required></td>
                            </tr>
                            <tr>
                                <td>Total T4 Levels</td>
                                <td><input type="text" class="form-control" name="totalT4Levels" placeholder="Normal Levels 4.7-12.5ug/dl" required></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <br><br>
                <button class="pull-left btn btn-primary" type="submit" name="action" value="back">&larr; Back to Patients Column</button>
                <button class="pull-right btn btn-success" type="submit" name="action" value="ThyroidTest">Save Lab Result &rarr;</button>
            </form>
                
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/LabOrder-end.jsp" %>