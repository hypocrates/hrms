<%-- 
    Document   : AdmitedDetails
    Created on : 26-Apr-2017, 08:51:39
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admitted Patient Details
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patients List</a></li>
        <li class="active">Admitted Patient's Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150/83</h3>

              <p>Blood Pressure</p>
            </div>
            <div class="icon">
              <i class="ion ion-heart"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53</h3>

              <p>BMI</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>Hospital Visits</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65<sup style="font-size: 20px">%</sup></h3>

              <p>Appointment Discipline</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!--Information row-->
        <div class="row">
            <div class="col-md-3">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Patient's Information</h3>
                    </div>
                    <div class="box-body">
                        <p style="font-size: 18px">
                            <b>Name:</b>
                        <p style="font-size: 17px">${Name}</p>
                        </p>
                        <!--<hr>-->
                        <p style="font-size: 18px">
                            <b>Gender:</b>
                        <p style="font-size: 17px">${gender}</p>
                        </p>
                        <!--<hr>-->
                        <p style="font-size: 18px">
                            <b>Age:</b>
                        <p style="font-size: 17px">${date}</p>
                        </p>
                        <!--<hr>-->
                        <p style="font-size: 18px">
                            <b>Blood Group:</b>
                        <p style="font-size: 17px">${bloodGroup}</p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="small-box bg-light-blue-gradient">
                    <label><h4>Complains</h4></label>
                    <textarea class="form-control" rows="11" placeholder="No Recorded Complains ..." name="complains" id="complains" required readonly>${complains}</textarea>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="small-box bg-light-blue">
                    <label><h4>Diagnosis</h4></label>
                    <textarea rows="11" placeholder="Diagnosis" name="diag" id="diagnosis" class="form-control" required readonly>${diagnosis}</textarea>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="small-box bg-light-blue-active">
                    <label><h4>Prescription</h4></label>
                    <textarea rows="11" placeholder="Prescription" name="prescription" id="prescription" class="form-control" required readonly>${prescription}</textarea>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
      <!--/.Info row-->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          
            <!--Current Status-->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Current Status</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!--/.box-header-->
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="ion ion-medkit"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Admission</span>
                      <span class="info-box-number">58</span>

                      <div class="progress">
                        <div class="progress-bar" style="width: 58%"></div>
                      </div>
                          <span class="progress-description">
                            Has been admitted for 58 days
                          </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <%
                    String type = (String) session.getAttribute("access");
                    if(type.equals("Doctor")){
                %>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1">
                        <form action="PatientsInSession" method="post">
                            <input type="hidden" name="pRef" value="${pRef}">
                            <input type="hidden" name="session" value="${session}">
                            <button type="submit" class="btn btn-primary" name="action" value="medicalHistory">View Medical History</button>
                        </form>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <form action="PatientsInSession" method="post">
                            <input type="hidden" name="pRef" value="${pRef}">
                            <input type="hidden" name="session" value="${session}">
                            <button type="submit" class="btn btn-success" name="action" value="continueSession">Continue Current Session</button>
                        </form>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <form action="Patient" method="post" class="form-group">
                            <input type="hidden" name="pRef" value="${pRef}">
                            <input type="hidden" name="session" value="${session}">
                            <button type="submit" name="action" value="clearForExit" class="btn btn-danger">Exit Patient</button>
                        </form>
                    </div>
                </div>
                            <%} else if(type.equals("Nurse")){
                %>
                <h4><a href="AdmittedPatientsList">Back to list</a> </h4>
                <%
                    }
                %>
                <br>
            </div>
            <!--/.Current-status-->
            
            

        </section>
        
      </div>
      <!-- /.row (first row) -->
      

    </section>
    <!-- /.content -->
    
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>

<%@include file="WEB-INF/web/end.jsp" %>
<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  