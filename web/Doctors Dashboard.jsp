<%-- 
    Document   : Doctors Dashboard
    Created on : 27-Jun-2017, 08:10:22
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

  
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Welcome <%=session.getAttribute("user")%> to <%=session.getAttribute("HospitalName")%>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        <% 
            if(request.getAttribute("message") != null) {
        %>
            <p class="alert alert-info">${message}</p>
        <% } %>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- BAR CHART -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bar Chart</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="barChart" style="height:300px"></canvas>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            <!-- /.box -->
            </div>
            
            
        </div>
        <div class="row">
            <div class="col-md-4">
                <!-- general form elements -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Patients Waiting</h3>
                    </div>
                    <div class="box-body">
                        <!-- small box -->
                        <div class="small-box bg-green" style="margin-bottom: 1px">
                            <div class="inner">
                                <h3>14</h3>

                                <p>Patients Waiting</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-user-md"></i>
                            </div>
                            <a href="NewPatientsList" class="small-box-footer">Attend Patients <i class="fa fa-arrow-circle-right"></i></a>
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Patients in Session</h3>
                    </div>
                    <div class="box-body">
                        <!-- small box -->
                        <div class="small-box bg-primary" style="margin-bottom: 1px">
                            <div class="inner">
                                <h3>23</h3>

                                <p>Patients in Session</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-flask"></i>
                            </div>
                            <a href="PatientsInSessionList" class="small-box-footer">View Patients List <i class="fa fa-arrow-circle-right"></i></a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- general form elements -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admitted Patients</h3>
                    </div>
                    <div class="box-body">
                        <!-- small box -->
                        <div class="small-box bg-purple" style="margin-bottom: 1px">
                            <div class="inner">
                                <h3>29</h3>

                                <p>Admitted Patients</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bed"></i>
                            </div>
                            <a href="AdmittedPatientsList" class="small-box-footer">View Admitted Patients<i class="fa fa-arrow-circle-right"></i></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/Doctors-end.jsp" %>