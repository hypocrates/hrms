<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Patient
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patient</a></li>
        <li class="active">New Patient</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        ${message}
        <!--modal to add information-->
        <div class="example-modal">
            <div class="modal modal-primary fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="Search" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Patients Details}</h4>
                            </div>
                            <div class="modal-body">
                                <label>National ID</label>
                                <input type="number" name="NationalID" value="${nationalID}" class="form-control">
                                <br>
                                <label>Sir Name</label>
                                <input type="text" name="SirName" placeholder="Sir Name" class="form-control" value="${pData.sirName}" required>
                                <br>
                                <label>First Name</label>
                                <input type="text" name="FirstName" placeholder="First Name" class="form-control" value="${pData.firstName}" required>
                                <br>
                                <label>Last Name</label>
                                <input type="text" name="LastName" placeholder="Last Name" class="form-control" value="${pData.lastName}" required>
                                <br>
                                <label>Other Names <i>(Optional)</i></label>
                                <input type="text" name="MiddleName" placeholder="Other Names" class="form-control" value="${pData.middleName}">
                                <br>
                            </div>
                            <div class="modal-header">
<!--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>-->
                                <h4 class="modal-title">Patients Bio Data</h4>
                            </div>
                            <div class="modal-body">
                                <label>Date of Birth</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="DoB" class="form-control pull-right" id="datepicker" value="${bData.dob}">
                                </div>
                                <!--</div>-->
                                <br>
                                <label>Gender</label>
                                <select class="form-control select2" style="width: 100%;" name="Gender" value="${bData.gender}">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                                <br>
                                <br>
                                <label>Marital Status</label>
                                <select class="form-control select2" style="width: 100%;" name="MaritalStatus" value="${bData.maritalStatus}">
                                    <option>Single</option>
                                    <option>Monogamous</option>
                                    <option>Polygamous</option>
                                </select>
                                <br>
                                <br>
                                <label>Children</label>
                                <input type="number" name="Children" placeholder="Number of Children" class="form-control" required maxlength="2" value="${bData.children}">
                                <br>

                                <label>Blood Group</label>
                                <select class="form-control select2" style="width: 100%;" name="BloodGroup" value="${bData.bloodGroup}">
                                    <!--<option>B+</option>-->
                                    <option selected="selected">A+</option>
                                    <option>A-</option>
                                    <option>B+</option>
                                    <option>B-</option>
                                    <option>AB+</option>
                                    <option>AB-</option>
                                    <option>O+</option>
                                    <option>O-</option>
                                </select>
                                <br>
                            </div>
                            <div class="modal-header">
<!--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>-->
                                <h4 class="modal-title">Patients Contact Data</h4>
                            </div>
                            <div class="modal-body">
                                <label>Occupation</label>
                                <input type="text" name="Occupation" placeholder="Casual Laborer" class="form-control" value="${cData.occupation}" required>
                                <br>

                                <label>County of Residence</label>
                                <input type="text" name="County" placeholder="County" class="form-control" value="${cData.county}" required>
                                <br>

                                <label>Place of Residence</label>
                                <input type="text" name="Place" placeholder="Place of Residence" class="form-control" value="${cData.residence}" required>
                                <br>

                                <label>Phone Number</label>
                                <input type="tel" name="Telephone" placeholder="Mobile Number" class="form-control" value="${cData.phoneNumber}" required>
                                <br>

                                <label>Email Address</label>
                                <input type="email" name="Email" placeholder="you@mail.com" class="form-control" value="${cData.email}">
                                <br>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline" name="action" value="add">Save</button>
                            </div>   
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
          <!-- /.example-modal -->
        <div class="example-modal">
            <div class="modal fade" id="doctorsModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Assign a Doctor</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Doctors in Hospital</h3>

                                        <div class="box-tools">
                                            <div class="input-group input-group-sm" style="width: 150px;">
                                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                                <div class="input-group-btn">
                                                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover">
                                            <tr>
                                                <th>Name</th>
                                                <th>Specialisation</th>
                                                <th>Number of Patients Waiting</th>
                                                <th>Action</th>
                                            </tr>
                                            <%@page import="Bean.AssignDoctorData, java.util.ArrayList" %>
                                            <%
                                                int size =(Integer) request.getAttribute("size");
                                                ArrayList<AssignDoctorData> adDataArray =(ArrayList) request.getAttribute("aDataArray");
                                                //if (size < 15) {
                                                for (int x = 0; x < size; x++) {
                                                    AssignDoctorData doctor = adDataArray.get(x);
                                                    String name = doctor.getName();
                                                    String specialisation = doctor.getSpecialisation();
                                                    int patients = doctor.getNoPatients();
                                                    int dRef = doctor.getdRef();

                                            %>
                                            <tr>
                                                <td><%= name%></td>
                                                <td><%= specialisation%></td>
                                                <td><%= patients%></td>
                                                <td>
                                                    <form action="Search" method="post" class="form-group">
                                                        <input type="hidden" name="docName" value="<%= name%>">
                                                        <input type="hidden" name="dRef" value="<%= dRef %>">
                                                        <input type="hidden" name="pRef" value="${pData.pRef}">
                                                        <button type="submit" name="action" value="createSession" class="btn btn-primary">Assign Patient</button>
                                                    </form>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </table>
                                    </div>
                                  <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                              </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
<!--                            <button type="button" class="btn btn-outline">Save changes</button>-->
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
        <!-- /.example-modal -->
        <div class="patient">
            <!--Display Area-->
                <div class="row">
                    <div class="col-md-6 center">
                        <br>
                        <img src="dist/img/avatar.png" class="img-circle" alt="Patients Image" height="230px" width="230px"><br>
                        <br>
                        <form action="Search" method="post" class="form-group">
                            <!--<input type="hidden" name="action" value="search">-->
                            <div class="input-group">
                                <input type="number" name="id" class="form-control" placeholder="Search National ID..." value="${pData.nationalID}${nationalID}">
                                  <span class="input-group-btn">
                                    <button type="submit" name="action" value="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                    </button>
                                  </span>
                            </div>
                                  ${AddButton}
                        </form>
                    </div>
            
                    <div class = "col-md-6">
                        <!--<div class="row">-->
                                <br>
                                <div class="form">
                                    <label>Sir Name</label>
                                    <input type="text" name="SirName" placeholder="Sir Name" class="form-control" value="${pData.sirName}" required ${disabled}>
                                </div>
                                <br>
                                <div class="form">
                                    <label>First Name</label>
                                    <input type="text" name="FirstName" placeholder="First Name" class="form-control" value="${pData.firstName}" required ${disabled}>
                                </div>
                                <br>
                                <div class="form">
                                    <label>Last Name</label>
                                    <input type="text" name="LastName" placeholder="Last Name" class="form-control" value="${pData.lastName}" required ${disabled}>
                                </div>
                                <br>
                                <div class="form">
                                    <label>Other Names</label>
                                    <input type="text" name="MiddleName" placeholder="Other Names" class="form-control" value="${pData.middleName}" ${disabled}>
                                </div>
                                <br>
                            <!--</form>-->
                        <!--</div>-->

                    </div>                
                </div></form>
                <div class="row">
                    <div class="col-md-6">
                        <br>
                        <div class="center sub-title>"<h2>Bio Data</h2></div>
                        <br>
                        <!--<div class="form-group">-->
                            <label>Date of Birth</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" value="${bData.dob}" ${disabled}>
                            </div>
                        <!--</div>-->
                        <br>

                        <label>Gender</label>
                        <input  type="text" class="form-control" placeholder="Gender" name="Gender" value="${bData.gender}" required ${disabled}>
                        <br>
                        <label>Marital Status</label>
                        <input type="text" placeholder="Marital Status" class="form-control" name="MaritalStatus" value="${bData.maritalStatus}" required ${disabled}>
                        <br>
                        <label>Children</label>
                        <input type="number" name="Children" placeholder="Number of Children" class="form-control" required maxlength="2" value="${bData.children}" ${disabled}>
                        <br>

                        <label>Blood Group</label>
                        <input type="text" placeholder="Blood Group" class="form-control" name="BloodGroup" value="${bData.bloodGroup}" ${disabled}>
                            
                        <br>
                    </div>
                    <div class="col-md-6">
                        <br>
                        <div class="center sub-title>"<h2>Contact Information</h2></div>
                        <br>
                        <label>Occupation</label>
                        <input type="text" name="Occupation" placeholder="Casual Laborer" class="form-control" value="${cData.occupation}" required ${disabled}>
                        <br>

                        <label>County of Residence</label>
                        <input type="text" name="County" placeholder="County" class="form-control" value="${cData.county}" required ${disabled}>
                        <br>

                        <label>Place of Residence</label>
                        <input type="text" name="Place" placeholder="Place of Residence" class="form-control" value="${cData.residence}" required ${disabled}>
                        <br>

                        <label>Phone Number</label>
                        <input type="tel" name="Telephone" placeholder="Mobile Number" class="form-control" value="${cData.phoneNumber}" required ${disabled}>
                        <br>

                        <label>Email Address</label>
                        <input type="email" name="Email" placeholder="you@mail.com" class="form-control" value="${cData.email}" ${disabled}>
                        <br>
                    </div>
                </div>
                <!--<ul class="pager">-->
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <!--<form action="Search" method="post" class="form-group">-->
                            <!--<input type="hidden">-->
                            <button type="submit" class="btn btn-danger" name="action" value="dismiss">Dismiss Patient</button>
                        <!--</form>-->
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <form action="Search" method="post" class="form-group">
                            <!--<input type="hidden" name="action" value="createSession">-->
                            <input type="hidden" name="pRef" value="${pData.pRef}">
                            ${button}
                        </form>
                    </div>
                </div>
            <!--</form>-->
            <!--</ul>-->
            
        </div>
      <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/new-patient-end.jsp" %>
