<%-- 
    Document   : LipidProfile
    Created on : 08-Jun-2017, 11:25:23
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laboratory Order
        <small>${message}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
      <div class="row">
            <br>
            <div class="col-md-8">
                <p><h2><b>Lipid Profile</b></h2></p>
            <form method="post" action="LabWorkSave">
                <input type="hidden" name="session" value="${session}">
                <input type="hidden" name="pRef" value="${pRef}">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>TEST</th>
                                <th>RESULT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Total Cholesterol</td>
                                <td><input type="text" class="form-control" name="totalCholesterol" placeholder="Total Cholesterol" required></td>
                            </tr>
                            <tr>
                                <td>HDL Cholesterol</td>
                                <td><input type="text" class="form-control" name="hdlCholesterol" placeholder="HDL Cholesterol" required></td>
                            </tr>
                            <tr>
                                <td>LDL Cholesterol</td>
                                <td><input type="text" class="form-control" name="ldlCholesterol" placeholder="LDL Cholesterol" required></td>
                            </tr>
                            <tr>
                                <td>Triglycerides</td>
                                <td><input type="text" class="form-control" name="triglycerides" placeholder="Triglycerides" required></td>
                            </tr>
                            <tr>
                                <td>T.C/HDL Ratio</td>
                                <td><input type="text" class="form-control" name="tchdlRatio" placeholder="T.C/HDL Ratio" required></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br><br>
                <button class="pull-left btn btn-primary" type="submit" name="action" value="back">&larr; Back to Patients Column</button>
                <button class="pull-right btn btn-success" type="submit" name="action" value="LipidProfileTest">Save Lab Results &rarr;</button>
                <br><br>
                <div>
                    <p>For optimum results, it is recommended to fast 12 hours to tasking a lipid profile test (total cholesterol, LDL Cholesterol, Triglycerides)
                    Water may be assumed but abstinence from food is a must. Although total cholesterol and LDL Cholesterol do not vary significantly after a meal,
                    triglycerides can increase by as much as 20%-30% after a meal and it is for this reason that fasting 12 hours is a must, especially when a
                    person is also being tested for Triglycerides.</p>
                </div>
            </form>
                
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/LabOrder-end.jsp" %>