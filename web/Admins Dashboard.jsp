<%-- 
    Document   : Admins Dashboars
    Created on : 24-Jun-2017, 14:46:27
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Welcome to <%= session.getAttribute("HospitalName")%>
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        
            <!-- Your Page Content Here -->
            <!-- Info boxes -->
            <div class="row">
                <!--modals-->
                <div class="example-modal">
                    <div class="modal modal-success fade" id="addDoctor" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Add Doctors</h4>
                                </div>
                                <form method="post" action="Admin">
                                    <input type="hidden" name="hRef" value="<%= session.getAttribute("hRef")%>">
                                    <div class="modal-body">
                                        <label>National ID</label>
                                        <input type="number" name="NationalID" value="" class="form-control" required>
                                        <br>
                                        <label>Sir Name</label>
                                        <input type="text" name="SirName" placeholder="Sir Name" class="form-control" required>
                                        <br>
                                        <label>First Name</label>
                                        <input type="text" name="FirstName" placeholder="First Name" class="form-control" required>
                                        <br>
                                        <label>Other Name</label>
                                        <input type="text" name="Other Names" placeholder="Other Names" class="form-control">
                                        <br>
                                        <label>Date of Birth</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="DoB" class="form-control pull-right" id="datepicker">
                                        </div>
                                        <!--</div>-->
                                        <br>
                                        <label>Specialisation</label>
                                        <input type="text" name="Specialistation" placeholder="Specialisation" class="form-control" required>
                                        <br>
                                        <label>Email</label>
                                        <input type="email" name="Email" placeholder="Email" class="form-control" required>
                                        <br>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" name="action" value="saveDoctor" class="btn btn-outline">Save</button>
                                    </div>
                                </form>
                            </div>
                        <!-- /.modal-content -->
                        </div>
                    <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!--Nurse Modal-->
                <div class="example-modal">
                    <div class="modal modal-info fade" id="addNurse" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Add Nurse</h4>
                                </div>
                                <form method="post" action="Admin">
                                    <input type="hidden" name="hRef" value="<%= session.getAttribute("hRef")%>">
                                    <div class="modal-body">
                                        <label>National ID</label>
                                        <input type="number" name="NationalID" value="" class="form-control" required>
                                        <br>
                                        <label>Sir Name</label>
                                        <input type="text" name="SirName" placeholder="Sir Name" class="form-control" required>
                                        <br>
                                        <label>First Name</label>
                                        <input type="text" name="FirstName" placeholder="First Name" class="form-control" required>
                                        <br>
                                        <label>Other Name</label>
                                        <input type="text" name="Other Names" placeholder="Other Names" class="form-control">
                                        <br>
                                        <label>Date of Birth</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="DoB" class="form-control pull-right" id="datepicker2">
                                        </div>
                                        <!--</div>-->
                                        <br>
                                        <label>Email</label>
                                        <input type="email" name="Email" placeholder="Email" class="form-control" required>
                                        <br>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" name="action" value="saveNurse" class="btn btn-outline">Save</button>
                                    </div>
                                </form>
                            </div>
                        <!-- /.modal-content -->
                        </div>
                    <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!--Lab Modal-->
                <div class="example-modal">
                    <div class="modal modal-primary fade" id="addLabStuff" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Add Laboratory Technician</h4>
                                </div>
                                <form method="post" action="Admin">
                                    <input type="hidden" name="hRef" value="<%= session.getAttribute("hRef")%>">
                                    <div class="modal-body">
                                        <label>National ID</label>
                                        <input type="number" name="NationalID" value="" class="form-control" required>
                                        <br>
                                        <label>Sir Name</label>
                                        <input type="text" name="SirName" placeholder="Sir Name" class="form-control" required>
                                        <br>
                                        <label>First Name</label>
                                        <input type="text" name="FirstName" placeholder="First Name" class="form-control" required>
                                        <br>
                                        <label>Other Name</label>
                                        <input type="text" name="Other Names" placeholder="Other Names" class="form-control">
                                        <br>
                                        <label>Date of Birth</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="DoB" class="form-control pull-right" id="datepicker3">
                                        </div>
                                        <!--</div>-->
                                        <br>
                                        <label>Email</label>
                                        <input type="email" name="Email" placeholder="Email" class="form-control" required>
                                        <br>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" name="action" value="saveLabTech" class="btn btn-outline">Save</button>
                                    </div>
                                </form>
                            </div>
                        <!-- /.modal-content -->
                        </div>
                    <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!--/.modals-->
                <% 
                    if(request.getAttribute("commentMessage") != null) {
                %>
                    <p class="alert alert-info"><%=request.getAttribute("commentMessage")%></p>
                <% } %>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Patients in Hospital</span>
                            <span class="info-box-number">${patients}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-stethoscope"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Doctors</span>
                            <span class="info-box-number">${doctors}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="ion ion-ios-heart-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Nurses</span>
                            <span class="info-box-number">${nurses}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-flask"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Laboratory Attendants</span>
                            <span class="info-box-number">${labAttendants}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Monthly Recap Report</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center">
                                        <strong>Patients in Hospital</strong>
                                    </p>

                                    <div class="chart">
                                        <!-- area Chart Canvas -->
                                        <canvas id="areaChart" style="height: 200px;"></canvas>
                                    </div>
                                    <!-- /.chart-responsive -->
                                </div>
                                <!-- /.col -->
                                
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- ./box-body -->
                        <div class="box-footer">
                            <div class="row">
                                
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Doctors</h3>
                        </div>
                        <div class="box-body">
                            <!-- small box -->
                            <div class="small-box bg-green" style="margin-bottom: 1px">
                                <div class="inner">
                                    <h3>${doctors}</h3>

                                    <p>Doctors Employed</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user-md"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="padding-right: 0.5px">
                                    <button type="button" class="form-control btn btn-primary" name="action" value="add" data-toggle="modal" data-target="#addDoctor">Add Doctor</button>
                                </div>
                                <div class="col-xs-6" style="padding-left: 0.5px">
                                    <form method="post" action="Admin">
                                        <input type="hidden" name="hRef" value="<%= session.getAttribute("hRef")%>">
                                        <button type="submit" class="form-control btn btn-primary" name="action" value="viewDoctors">View Doctors</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Laboratory Staff</h3>
                        </div>
                        <div class="box-body">
                            <!-- small box -->
                            <div class="small-box bg-primary" style="margin-bottom: 1px">
                                <div class="inner">
                                    <h3>${labAttendants}</h3>

                                    <p>Laboratory Staff Employed</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-flask"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="padding-right: 0.5px">
                                    <button type="submit" class="form-control btn btn-primary" name="action" value="add"  data-toggle="modal" data-target="#addLabStuff">Add Lab Staff</button>
                                </div>
                                <div class="col-xs-6" style="padding-left: 0.5px">
                                    <form method="post" action="Admin">
                                        <input type="hidden" name="hRef" value="<%= session.getAttribute("hRef")%>">
                                        <button type="submit" class="form-control btn btn-primary" name="action" value="viewLabTechs">View Lab Techs</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nurses</h3>
                        </div>
                        <div class="box-body">
                            <!-- small box -->
                            <div class="small-box bg-purple" style="margin-bottom: 1px">
                                <div class="inner">
                                    <h3>${nurses}</h3>

                                    <p>Nurses Employed</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-heart"></i>
                                </div>
                                <a href="HospitalPatients" class="small-box-footer">More Info<i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="padding-right: 0.5px">
                                    <button type="submit" class="form-control btn btn-primary" name="action" value="add"  data-toggle="modal" data-target="#addNurse">Add Nurse</button>
                                </div>
                                <div class="col-xs-6" style="padding-left: 0.5px">
                                    <form method="post" action="Admin">
                                        <input type="hidden" name="hRef" value="<%= session.getAttribute("hRef")%>">
                                        <button type="submit" class="form-control btn btn-primary" name="action" value="viewNurses">View Nurses</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/AdminsDashboard-end.jsp" %>
  