<%-- 
    Document   : ChiefComplains
    Created on : 27-Feb-2017, 12:09:09
    Author     : root
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Chief Complains
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patient</a></li>
        <li class="active">Chief Complains</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- Your Page Content Here -->
        <div class="row">
            <br>
            <div class="col-md-8">
                <form action="ChiefComplains" method="post">
                    <input type="hidden" name="pRef" value="${pRef}">
                    <input type="hidden" name="session" value="${session}">
                    <input type="hidden" name="name" value="${Name}">
                    <input type="hidden" name="gender" value="${gender}">
                    <input type="hidden" name="age" value="${date}">
                    <input type="hidden" name="bloodGroup" value="${bloodGroup}">
                    
                    <!--modal for info change during labOrder-->
                    <div class="example-modal">
                        <div class="modal modal-primary fade" id="labOrderModal" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Change of Records Warning</h4>
                                    </div>
                                    <div class="modal-body">
                                        Any changes in the form will overwrite the previous saved data. Do you want to continue?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline pull-left" name="action" value="orderLab&NotSave">Don't Save & continue to LabOrder</button>
                                        <button type="submit" name="action" value="orderLab" class="btn btn-outline">Save & continue to LabOrder</button>
                                    </div>   

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.example-modal -->
                    <!--modal for info change during prescription-->
                    <div class="example-modal">
                        <div class="modal modal-primary fade" id="prescriptionModal" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Change of Records Warning</h4>
                                    </div>
                                    <div class="modal-body">
                                        Any changes in the form will overwrite the previous saved data. Do you want to continue?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline pull-left" name="action" value="prescription&NotSave">Don't Save and continue to Prescription</button>
                                        <button type="submit" name="action" value="prescription" class="btn btn-outline">Save and continue to Prescription</button>
                                    </div>   

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.example-modal -->
                    
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label><h4>Chief Complains</h4></label>
                            <textarea  class="form-control" rows="13" placeholder="Enter ..." name="complains" id="complains" required autofocus>${complains}</textarea>
                        </div>
                        <div class="col-md-4">
                            <label><h4>BMI & BP</h4></label>
                            <div class="well">
                                <span><label>Current Weight:</label><input class="form-control" placeholder="Weight..." type="text"></span>
                                <span><label>Current Height:</label><input class="form-control" placeholder="Height..." type="text"></span>
                                <span><label>BMI:</label><p name="bmi">25.16</p></span>
                                <br>
                                <span><label>Blood Pressure:</label><input class="form-control" name="bp" placeholder="Blood Pressure..." type="text" value="${bp}"></span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form">
                                <label><h4>Diagnosis</h4></label>
                                <textarea rows="8" name="diagnosis" id="diagnosis" class="form-control" placeholder="Enter ...">${diagnosis}</textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form">
                                <br><br>
                                ${labResultsButton}
                                <br><br>
                                ${labOrderButton}
                                <br><br>
                                ${prescriptionButton}
                                <br><br>
                                <button type="submit" name="action" value="dismiss" class="btn btn-danger">Dismiss the Case</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
                    <%@include file="WEB-INF/web/MedicalHistory.jsp" %>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/end.jsp" %>