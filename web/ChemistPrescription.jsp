<%-- 
    Document   : ChemistPrescription
    Created on : 27-Jun-2017, 07:47:13
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admitted Patient Details
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patients List</a></li>
        <li class="active">Admitted Patient's Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150/83</h3>

              <p>Blood Pressure</p>
            </div>
            <div class="icon">
              <i class="ion ion-heart"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53</h3>

              <p>BMI</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>Hospital Visits</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65<sup style="font-size: 20px">%</sup></h3>

              <p>Appointment Discipline</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!--Information row-->
        <div class="row">
            <div class="col-md-3">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Patient's Information</h3>
                    </div>
                    <div class="box-body">
                        <p style="font-size: 18px">
                            <b>Name:</b>
                        <p style="font-size: 17px">${Name}</p>
                        </p>
                        <!--<hr>-->
                        <p style="font-size: 18px">
                            <b>Gender:</b>
                        <p style="font-size: 17px">${gender}</p>
                        </p>
                        <!--<hr>-->
                        <p style="font-size: 18px">
                            <b>Age:</b>
                        <p style="font-size: 17px">${date}</p>
                        </p>
                        <!--<hr>-->
                        <p style="font-size: 18px">
                            <b>Blood Group:</b>
                        <p style="font-size: 17px">${bloodGroup}</p>
                        </p>
                    </div>

                </div>
<!--                <div class='small-box '>
                    <label><h4>Patient's Information</h4></label>
                    
                </div>-->
            </div>
            <div class="col-md-3">
                <div class="small-box bg-light-blue-gradient">
                    <label><h4>Complains</h4></label>
                    <textarea class="form-control" rows="11" placeholder="No Recorded Complains ..." name="complains" id="complains" required readonly>${complains}</textarea>
                    <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            <div class="col-md-3">
                <div class="small-box bg-light-blue">
                    <label><h4>Diagnosis</h4></label>
                    <textarea rows="11" placeholder="Diagnosis" name="diag" id="diagnosis" class="form-control" required readonly>${diagnosis}</textarea>
                    <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            <div class="col-md-3">
                <div class="small-box bg-light-blue-active">
                    <label><h4>Prescription</h4></label>
                    <textarea rows="11" placeholder="Prescription" name="prescription" id="prescription" class="form-control" required readonly>${prescription}</textarea>
                    <!--<a href="#" class="small-box-footer"-->
                </div>
            </div>
        </div>
      <!--/.Info row-->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div>
            <h4><a href="ClearedPatientsList"><i class="fa fa-arrow-circle-left"></i> Go back to Patients List </a></h4>
        </div>
        
      </div>
      <!-- /.row (first row) -->
      

    </section>
    <!-- /.content -->
    
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>

<%@include file="WEB-INF/web/end.jsp" %>