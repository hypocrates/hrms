<%--
    Document   : MedicalHistoryList
    Created on : 22-Jun-2017, 09:55:53
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Medical History
        <small>of ${Name}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p class="alert alert-info">${message}</p>
      <!-- Your Page Content Here -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Medical Records <small>${message}</small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Date Entered</th>
                  <th>Date Exited</th>
                  <th>Hospital</th>
                  <th>Diagnosis</th>
                  <th>Prescription</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <%@page import="Bean.MedicalHistoryListData, java.util.ArrayList, java.sql.Date" %>
                    <%
                        int size =(Integer) request.getAttribute("size");
                        ArrayList<MedicalHistoryListData> records =(ArrayList) request.getAttribute("medicalHistoryListData");
                        //if (size < 15) {
                            for (int x = 0; x < size; x++) {
                            MedicalHistoryListData record = records.get(x);
                            Date dateEnter = record.getDateEntry();
                            Date dateExit = record.getDateExit();
                            String hospital = record.gethName();
                            String diagnosis = record.getDiagnosis();
                            String prescription = record.getPrescription();
                            int sess = record.getSession();
                    %>
                    <tr>
                        <td><%= dateEnter%></td>
                        <td><%= dateExit %></td>
                        <td><%= hospital %></td>
                        <td><%= diagnosis%></td>
                        <td><%= prescription%></td>
                        <td><form action="MedicalHistory" method="post">
                                <input type="hidden" name="pRef" value="${pRef}">
                                <input type="hidden" name="sess" value="<%= sess %>">
                                <input type="hidden" name="session" value="${session}">
                                <button type="submit" name="action" value="details" class="btn btn-primary">View More...</button>
                            </form></td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
                <tfoot>
                <tr>
                  <th>Date Entered</th>
                  <th>Date Exited</th>
                  <th>Hospital</th>
                  <th>Diagnosis</th>
                  <th>Prescription</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
            <form action="PatientsInSession" method="post">
                <input type="hidden" name="pRef" value="${pRef}">
                <input type="hidden" name="session" value="${session}">
                <button class="pull-left btn btn-primary" type="submit" name="action" value="details" >&larr; Back to Patients Column</button>
            </form>
          </div>
          <!-- /.box -->


    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/PatientsInHospital-end.jsp" %>