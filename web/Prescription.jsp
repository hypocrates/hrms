<%-- 
    Document   : Prescription
    Created on : 21-Apr-2017, 02:06:18
    Author     : Wamuyu
--%>

<%-- 
    Document   : index
    Created on : 20-Apr-2017, 22:33:40
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Prescription
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- Your Page Content Here -->
      <div class="row">
            <br>
            <div class="col-md-8">
                <form action="Prescription" method="post">
                    <input type="hidden" name="pRef" value="${pRef}">
                    <input type="hidden" name="diagnosis" value="${diagnosis}">
                    <input type="hidden" name="complains" value="${complains}">
                    <input type="hidden" name="session" value="${session}">
                    <input type="hidden" name="name" value="${Name}">
                    <input type="hidden" name="gender" value="${gender}">
                    <input type="hidden" name="age" value="${date}">
                    <input type="hidden" name="bloodGroup" value="${bloodGroup}">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label><h4>Prescription</h4></label>
                            <textarea rows="10" placeholder="Enter Prescription..." name="prescription" id="prescription" class="form-control" required autofocus>${prescription}</textarea>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><h4>Chief Complains</h4></label>
                                <textarea rows="8" placeholder="Chief Complains" name="complains" id="complains" class="form-control" required readonly>${complains}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><h4>Diagnosis</h4></label>
                                <textarea rows="8" placeholder="Diagnosis" name="diag" id="diagnosis" class="form-control" required >${diagnosis}</textarea>
                            </div>

                        </div>
                    </div>
                    <ul class="pager">
                        <li>
                            <!--<a href="NewPatient.jsp"></a>-->
                            <button class="pull-right btn btn-success" type="submit" name="action" value="save">Save prescription &rarr;</button>
                        </li>
                        <li>
                            ${button}
                        </li>
                        <li class="previous">
                            <button class="pull-left btn btn-primary" type="submit" name="action" value="back">&larr; Back to Complains</button>
                            <!--<a href="ChiefComplains.jsp">&larr; </a>-->
                        </li>
                        
                    </ul>
                </form>
                
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
                <%@include file="WEB-INF/web/MedicalHistory.jsp" %>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/end.jsp" %>