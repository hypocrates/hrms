<%-- 
    Document   : index
    Created on : 20-Apr-2017, 22:33:40
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Welcome to Hypocrates <%=session.getAttribute("user")%>
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <% 
            if(request.getAttribute("message") != null) {
        %>
            <p class="alert alert-info"><%=request.getAttribute("message")%></p>
        <% } %>  
      <!-- Your Page Content Here -->
      <div class="box">
            <div class="box-header">
                <h3 class="box-title">This are the Hospitals you are currently employed in. <small>Click Sign In to start session in that hospital.</small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Hospital</th>
                  <th>Location</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <%@page import="Bean.HospitalData, java.util.ArrayList" %>
                    <%
                        int size =(Integer) request.getAttribute("size");
                        ArrayList<HospitalData> hpArray =(ArrayList) request.getAttribute("hpArray");
                        //if (size < 15) {
                            for (int x = 0; x < size; x++) {
                            HospitalData hpData = hpArray.get(x);
                            int hRef = hpData.gethRef();
                            String name = hpData.getName();
                            String location = hpData.getLocation();

                    %>
                    <tr>
                        <td><%= name%></td>
                        <td><%= location %></td>
                        <td><form action="IndexHospitalSelection" method="post">
                                <input type="hidden" name="hRef" value="<%= hRef %>">
                                <input type="hidden" name="name" value="<%= name %>">
                                <button type="submit" name="action" value="signIn" class="btn btn-primary">Sign In</button>
                            </form></td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
                <tfoot>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/end.jsp" %>