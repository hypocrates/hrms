<%-- 
    Document   : LabOrderForm
    Created on : 26-May-2017, 14:50:13
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laboratory Order
        <small>${message}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
    <%@page import="Bean.GeneralTestData, Bean.CardicTestData, Bean.CreatinineTestData, Bean.ElectrolytesTestData, Bean.GeneralTestData, Bean.LiverTestData, Bean.OtherTestData" %>    
    <%
        GeneralTestData gtData = (GeneralTestData) request.getAttribute("gtData");
        ElectrolytesTestData etData = (ElectrolytesTestData) request.getAttribute("etData");
        LiverTestData ltData = (LiverTestData) request.getAttribute("ltData");
        CardicTestData ctData = (CardicTestData) request.getAttribute("ctData");
        CreatinineTestData crData = (CreatinineTestData) request.getAttribute("crData");
        OtherTestData otData = (OtherTestData) request.getAttribute("otData");

    %>
      <!-- Your Page Content Here -->
      <div class="row">
            <br>
            <div class="col-md-8">
                <div class="col-md-8">
                    <p><h2><b>General Tests</b></h2></p>
                    <form action="LabWorkSave" method="Post">
                        <input type="hidden" name="pRef" value="${pRef}">
                        <input type="hidden" name="diagnosis" value="${diagnosis}">
                        <input type="hidden" name="complains" value="${complains}">
                        <input type="hidden" name="session" value="${session}">
                        <input type="hidden" name="name" value="${Name}">
                        <input type="hidden" name="gender" value="${gender}">
                        <input type="hidden" name="age" value="${date}">
                        <input type="hidden" name="bloodGroup" value="${bloodGroup}">
                        <!--General Test-->
                        <%
                            if(otData.getSerum() == 1){
                        %>
                        <label>Serum</label>
                        <input type="checkbox" name="serum" class="flat-red">
                        <br><br>
                        <%
                            }
                            if(otData.getPlasma() == 1){
                        %>
                        <label>Plasma</label>
                        <input type="checkbox" name="serum" class="flat-red">
                        <br><br>
                        <%
                            }
                            if (gtData.getbSugar() == 1){
                        %>
                        <label>Blood Sugar</label>
                        <input type="text" placeholder="3.3-6.7 m.mols/I" name="bSugar" class="form-control" required>
                        <%
                            }
                            if (gtData.getUrea() == 1){
                        %>
                        <label>Urea</label>
                        <input type="text" placeholder="1.7-8.3 m.mols/I" name="urea" class="form-control" required>
                        <%
                            }
                            if (gtData.getUricAcid()== 1){
                        %>
                        <label>Uric Acid</label>
                        <input type="text" placeholder="0.1-0.5 m.mols/I" name="uricAcid" class="form-control" required>
                        <%
                            }
                            if (gtData.getCalcium() == 1){
                        %>
                        <label>Calcium</label>
                        <input type="text" placeholder="2.20-2.60 m.mols/I" name="calcium" class="form-control" required>
                        <%
                            }
                            if (gtData.getAcidPhospate() == 1){
                        %>
                        <label>Acid Phosphate</label>
                        <input type="text" placeholder="0.2-0.81 U/1" name="acidPhosphate" class="form-control" required>
                        <%
                            }
                            if (gtData.getCholestrol() == 1){
                        %>
                        <label>Cholesterol</label>
                        <input type="text" placeholder="3.5-7.50 m.mols/I" name="cholesterol" class="form-control" required>
                        <%
                            }
                            if (gtData.getAmylase() == 1){
                        %>
                        <label>Amylase</label>
                        <input type="text" placeholder="Below 200somogyi units" name="amylase" class="form-control" required>
                        <%
                            }
                            if (gtData.getPhosphorus() == 1){
                        %>
                        <label>Phosphorus</label>
                        <input type="text" placeholder="0.5-1.5 m.mols/I" name="phosphorus" class="form-control" required>
                        <%
                            }
                            if (gtData.getTotalProtein() == 1){
                        %>
                        <label>Total Protein</label>
                        <input type="text" placeholder="60.80 g/l" name="tProtein" class="form-control" required>
                        <%
                            }
                            if (gtData.getAlbumen() == 1){
                        %>
                        <label>Albumen</label>
                        <input type="text" placeholder="35-52 g/l" name="albumen" class="form-control" required>
                        <%
                            }
                            if (gtData.getGlobulin() == 1){
                        %>
                        <label>Globulin</label>
                        <input type="text" placeholder="2-29 g/l" name="globulin" class="form-control" required>
                        <%
                            }
                        %>
                        <!--/.General Test-->
                        <!--Electrolytes-->
                        <label><h3><b>Electrolytes</b></h3></label><br>
                        <%
                            if(etData.getSodium() == 1){
                        %>
                        <label>Sodium</label>
                        <input type="text" placeholder="135-146 m.mols/I" name="sodium" class="form-control" required>
                        <%
                            }
                            if(etData.getPotassium() == 1){
                        %>
                        <label>Potassium</label>
                        <input type="text" placeholder="3.5-5.0 m.mols/I" name="potassium" class="form-control" required>
                        <%
                            }
                            if(etData.getChloride() == 1){
                        %>
                        <label>Chloride</label>
                        <input type="text" placeholder="9.5-10 m.mols/I" name="chloride" class="form-control" required>
                        <%
                            }
                            if(etData.getBicarbonate() == 1){
                        %>
                        <label>Bicarbonate</label>
                        <input type="text" placeholder="22-30 m.mols/I" name="bicarbonate" class="form-control" required>
                        <%
                            }
                        %>
                        <!--/.Electrolytes-->
                        <!--Liver-->
                        <label><h3><b>Liver Function Tests</b></h3></label><br>
                        <%
                            if(ltData.getbTotal() == 1){
                        %>
                        <label>Bilirubin (Total)</label>
                        <input type="text" placeholder="0-18 umol/I" name="bTotal" class="form-control" required>
                        <%
                            }
                            if(ltData.getbDirect() == 1){
                        %>
                        <label>Bilirubin (Direct)</label>
                        <input type="text" placeholder="4.27" name="bDirect" class="form-control" required>
                        <%
                            }
                            if(ltData.getbIndirect() == 1){
                        %>
                        <label>Bilirubin (Indirect)</label>
                        <input type="text" placeholder="4.27" name="bIndirect" class="form-control" required>
                        <%
                            }
                            if(ltData.getAlkalinePhosphate() == 1){
                        %>
                        <label>Alkaline Phosphate</label>
                        <input type="text" placeholder="98-279 1U/I" name="alkalinePhosphate" class="form-control" required>
                        <%
                            }
                            if(ltData.getSgot() == 1){
                        %>
                        <label>Amiotransfcrase (S.G.O.T)</label>
                        <input type="text" placeholder="0-38 1U/I" name="sgot" class="form-control" required>
                        <%
                            }
                            if(ltData.getSgpt() == 1){
                        %>
                        <label>Amiotransfcrase (S.G.P.T)</label>
                        <input type="text" placeholder="0-40 1U/I" name="sgpt" class="form-control" required>
                        <%
                            }
                            if(ltData.getYgt() == 1){
                        %>
                        <label>Y-Glutamy transfcrase (Y-GT)</label>
                        <input type="text" placeholder="8-36 1U/I" name="ygt" class="form-control" required>
                        <%
                            }
                        %>
                        <!--/.Liver-->
                        <!--cardic Enzymes-->
                        <label><h3><b>Cardic Enzymes</b></h3></label><br>
                        <%
                            if(ctData.getLdh() == 1){
                        %>
                        <label>Lactic Dehydrogenase(LDH)</label>
                        <input type="text" placeholder="140 1U/I" name="ldh" class="form-control" required>
                        <%
                            }
                            if(ctData.getHbd() == 1){
                        %>
                        <label>Hydroxybutyrate Dehydrogenase (HBD)</label>
                        <input type="text" placeholder="70-190 1U/I" name="hbd" class="form-control" required>
                        <%
                            }
                            if(ctData.getCpk() == 1){
                        %>
                        <label>Creatinine Phospho Kinase (CPK)</label>
                        <input type="text" placeholder="15-130 1U/I" name="cpk" class="form-control" required>
                        <%
                            }
                            if(ctData.getSgot() == 1){
                        %>
                        <label>Aminotransfcrase (S.G.O.T)</label>
                        <input type="text" placeholder="8-28 1U/I" name="sgot2" class="form-control" required>
                        <%
                            }
                        %>
                        <!--/.Cardic Enzymes-->
                        <!--Creatinine-->
                        <br><br>
                        <%
                            if(crData.getClearance() == 1){
                        %>
                        <label>Creatinine Clearance</label>
                        <input type="text" placeholder="80-120 ml/min" name="clearance" class="form-control" required>
                        <%
                            }
                            if(crData.getSerum() == 1){
                        %>
                        <label>Creatinine (Serum)</label>
                        <input type="text" placeholder="45 - 105 umol/1" name="cSerum" class="form-control" required>
                        <%
                            }
                            if(crData.getUrine() == 1){
                        %>
                        <label>Creatinine (Urine)</label>
                        <input type="text" placeholder="9-18 m.mol/24hrs" name="cUrine" class="form-control" required>
                        <%
                            }
                        %>
                        <!--/.Creatinine-->
                        <br><br>
                        <button class="pull-left btn btn-primary" type="submit" name="action" value="back">&larr; Back to Patients Column</button>
                        <button class="pull-right btn btn-success" type="submit" name="action" value="GeneralTest">Save Results &rarr;</button>
                    </form>
                </div>
                
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/LabOrder-end.jsp" %>
