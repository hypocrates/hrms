<%-- 
    Document   : MedicalHistory
    Created on : 21-Apr-2017, 02:04:00
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Medical History
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- Your Page Content Here -->
      <div class="row">
            <div class="col-md-8">
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Medical Centre</h3>
                            </div>
                            <div class="box-body">
                                <p>
                                    <b>Hospital Name:</b>
                                    <p><h5>${hpName}</h5></p>
                                </p>
                                <p>
                                    <b>Hospital Category:</b>
                                    <p>${hpSize}</p>
                                </p>
                                <p>
                                    <b>Hospital Location:</b>
                                    <p>${hpLocation}</p>
                                </p>
                                <p>
                                    <b>Hospital Contact</b>
                                <p>${hpAddress}</p>
                                </p>
                                <p>
                                    <b>Doctor Attended the Patient:</b>
                                <p>Dr. ${doctor}</p>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Chief Complains</h3>
                            </div>
                            <div class="box-body">
                                <p>${complains}</p>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Laboratory Test Results</h3>
                        </div>
                        <div class="box-body">
                            <p>Laboratory Results will be projected in this area...</p>
                            <%@page import="Bean.GeneralTests, Bean.CardicTests, Bean.CreatinineTests, Bean.ElectrolytesTests, Bean.GeneralTests, Bean.LiverFunctionTests, Bean.LipidProfileTests, Bean.ProstaticTests, Bean.ThyroidFunctionTests" %>    
                            <%
                                GeneralTests gtTests = (GeneralTests) request.getAttribute("GeneralTest");
                                ElectrolytesTests etTests = (ElectrolytesTests) request.getAttribute("ElectrolytesTest");
                                LiverFunctionTests ltTests = (LiverFunctionTests) request.getAttribute("LiverTest");
                                CardicTests ctTests = (CardicTests) request.getAttribute("CardicTest");
                                CreatinineTests crTests = (CreatinineTests) request.getAttribute("CreatinineTest");
                                LipidProfileTests lpTests = (LipidProfileTests) request.getAttribute("LipidTest");
                                ProstaticTests pTests = (ProstaticTests) request.getAttribute("ProstaticTest");
                                ThyroidFunctionTests tfTests = (ThyroidFunctionTests) request.getAttribute("ThyroidTest");

                            %>
                            <!--General Tests-->
                            <%
                                if(gtTests != null){
                            %>
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">General Tests</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Test</th>
                                                <th>Result</th>
                                            </tr>
                                            <%

                                                if (gtTests.getBloodSugar() != null){
                                            %>
                                                <tr>
                                                    <td>Blood Sugar</td>
                                                    <td>${GeneralTest.bloodSugar}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getUrea() != null){
                                            %>
                                                <tr>
                                                    <td>Urea</td>
                                                    <td>${GeneralTest.urea}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getUricAcid() != null){
                                            %>    
                                                <tr>
                                                    <td>Uric Acid</td>
                                                    <td>${GeneralTest.uricAcid}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getCalcium() != null){
                                            %>
                                                <tr>
                                                    <td>Calcium</td>
                                                    <td>${GeneralTest.calcium}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getAcidPhosphate() != null){
                                            %>
                                                <tr>
                                                    <td>Acid Phosphate</td>
                                                    <td>${GeneralTest.acidPhosphate}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getCholesterol() != null){
                                            %>
                                                <tr>
                                                    <td>Cholesterol</td>
                                                    <td>${GeneralTest.cholesterol}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getAmylase() != null){
                                            %>
                                                <tr>
                                                    <td>Amylase</td>
                                                    <td>${GeneralTest.amylase}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getPhosphorus() != null){
                                            %>
                                                <tr>
                                                    <td>Phosphorus</td>
                                                    <td>${GeneralTest.phosphorus}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getTotalProtein() != null){
                                            %>
                                                <tr>
                                                    <td>Total Protein</td>
                                                    <td>${GeneralTest.totalProtein}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getAlbumen() != null){
                                            %>
                                                <tr>
                                                    <td>Albumen</td>
                                                    <td>${GeneralTest.albumen}</td>
                                                </tr>
                                            <%
                                                }
                                                if (gtTests.getGlobulin() != null){
                                            %>
                                                <tr>
                                                    <td>Globulin</td>
                                                    <td>${GeneralTest.globulin}</td>
                                                </tr>
                                            <%
                                                }
                                            %>
                                      </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            <%
                                }
                            %>
                              <!--/.General Test-->

                            <!--Electrolytes-->
                            <%
                                if(etTests != null){
                            %>
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Electrolytes</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Test</th>
                                                <th>Result</th>
                                            </tr>
                                            <%
                                                if(etTests.getSodium() != null){
                                            %>
                                            <tr>
                                                <td>Sodium</td>
                                                <td>${ElectrolytesTest.sodium}</td>
                                            </tr>
                                            <%
                                                }
                                                if(etTests.getPotassium() != null){
                                            %>
                                            <tr>
                                                <td>Potassium</td>
                                                <td>${ElectrolytesTest.potassium}</td>
                                            </tr>
                                            <%
                                                }
                                                if(etTests.getChloride() != null){
                                            %>
                                            <tr>
                                                <td>Chloride</td>
                                                <td>${ElectrolytesTest.chloride}</td>
                                            </tr>
                                            <%
                                                }
                                                if(etTests.getBicarbonate() != null){
                                            %>
                                            <tr>
                                                <td>Bicarbonate</td>
                                                <td>${ElectrolytesTest.bicarbonate}</td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            <%
                                }
                            %>
                            <!--/.Electrolytes-->

                            <!--Liver-->
                            <%
                                if(ltTests != null){
                            %>
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Liver Tests</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Test</th>
                                                <th>Result</th>
                                            </tr>
                                            <%
                                                if(ltTests.getbTotal() != null){
                                            %>
                                            <tr>
                                                <td>Bilirubin (Total)</td>
                                                <td>${LiverTest.bTotal}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ltTests.getbDirect() != null){
                                            %>
                                            <tr>
                                                <td>Bilirubin (Direct)</td>
                                                <td>${LiverTest.bDirect}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ltTests.getbIndirect() != null){
                                            %>
                                            <tr>
                                                <td>Bilirubin (Indirect)</td>
                                                <td>${LiverTest.bIndirect}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ltTests.getAlkaline() != null){
                                            %>
                                            <tr>
                                                <td>Alkaline Phosphate</td>
                                                <td>${LiverTest.alkaline}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ltTests.getSGOT() != null){
                                            %>
                                            <tr>
                                                <td>Amiotransfcrase (S.G.O.T)</td>
                                                <td>${LiverTest.SGOT}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ltTests.getSGPT() != null){
                                            %>
                                            <tr>
                                                <td>Amiotransfcrase (S.G.P.T)</td>
                                                <td>${LiverTest.SGPT}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ltTests.getYGT() != null){
                                            %>
                                            <tr>
                                                <td>Y-Glutamy transfcrase (Y-GT)</td>
                                                <td>${LiverTest.YGT}</td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->    
                            <%
                                }
                            %>
                            <!--/.Liver-->

                            <!--cardic Enzymes-->
                            <%
                                if(ctTests != null){
                            %>
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Cardic Enzymes Tests</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Test</th>
                                                <th>Result</th>
                                            </tr>
                                            <%
                                                if(ctTests.getLDH() != null){
                                            %>
                                            <tr>
                                                <td>Lactic Dehydrogenase(LDH)</td>
                                                <td>${CardicTest.LDH}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ctTests.getHBD() != null){
                                            %>
                                            <tr>
                                                <td>Hydroxybutyrate Dehydrogenase (HBD)</td>
                                                <td>${CardicTest.HBD}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ctTests.getCPK() != null){
                                            %>
                                            <tr>
                                                <td>Creatinine Phospho Kinase (CPK)</td>
                                                <td>${CardicTest.CPK}</td>
                                            </tr>
                                            <%
                                                }
                                                if(ctTests.getSGOT() != null){
                                            %>
                                            <tr>
                                                <td>Aminotransfcrase (S.G.O.T)</td>
                                                <td>${CardicTest.SGOT}</td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box --> 
                                <!--/.Cardic Enzymes-->
                            <%
                                }
                            %>

                            <!--Creatinine-->
                            <%
                                if(crTests != null){
                            %>
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Creatinine Tests</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Test</th>
                                                <th>Result</th>
                                            </tr>
                                            <%
                                                if(crTests.getClearance() != null){
                                            %>
                                            <tr>
                                                <td>Creatinine Clearance</td>
                                                <td>${CreatinineTest.clearance}</td>
                                            </tr>
                                            <%
                                                }
                                                if(crTests.getSerum() != null){
                                            %>
                                            <tr>
                                                <td>Creatinine (Serum)</td>
                                                <td${CreatinineTest.serum}</td>
                                            </tr>
                                            <%
                                                }
                                                if(crTests.getUrine() != null){
                                            %>
                                            <tr>
                                                <td>Creatinine (Urine)</td>
                                                <td>${CreatinineTest.urine}</td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box --> 
                                <!--/.Creatinine-->
                            <%
                                }
                            %>

                            <!--Lipid Profile-->
                            <%
                                if(lpTests != null){
                            %>
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Lipid Profile Tests</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Test</th>
                                                <th>Result</th>
                                            </tr>
                                            <tr>
                                                <td>Total Cholesterol</td>
                                                <td>${LipidTest.totalCholesterol}</td>
                                            </tr>
                                            <tr>
                                                <td>HDL Cholesterol</td>
                                                <td>${LipidTest.hdlCholesterol}</td>
                                            </tr>
                                            <tr>
                                                <td>LDL Cholesterol</td>
                                                <td>${LipidTest.ldlCholesterol}</td>
                                            </tr>
                                            <tr>
                                                <td>Triglycerides</td>
                                                <td>${LipidTest.triglycerides}</td>
                                            </tr>
                                            <tr>
                                                <td>T.C/HDL Ratio</td>
                                                <td>${LipidTest.tchdlRatio}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box --> 
                            <%
                                }
                            %>
                            <!--/.Lipid Profile Test-->
                            <br><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Diagnosis</h3>
                            </div>
                            <div class="box-body">
                                <p>
                                    ${diagnosis}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Prescription</h3>
                            </div>
                            <div class="box-body">
                                <p>
                                    ${prescription}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="pager">
                    <li>
                        <a href="ChiefComplains.jsp">Back to Complains</a>
                    </li>
                    
                </ul>
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Date of the Incidence:</b>
                    <p><h5>${incidenceDate}</h5></p>
                    </p>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <p>
                        <b>Current Age:</b>
                    <p>${age}</p>
                    </p>
                    <p>
                        <b>Age at the Time of the Incidence:</b>
                    <p>${thenAge}</p>
                    </p>
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
                <%@include file="WEB-INF/web/MedicalHistory.jsp" %>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/end.jsp" %>
