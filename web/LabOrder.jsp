<%-- 
    Document   : LabOrder
    Created on : 21-Apr-2017, 02:00:52
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laboratory Order
        <small>${message}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        
      <!-- Your Page Content Here -->
      <div class="row">
            <br>
            <div class="col-md-8">
                <div class="col-md-8">
                    <p>Select Appropriate Tests Need</p>
                    <form action="LabOrder" method="Post">
                        <input type="hidden" name="pRef" value="${pRef}">
                        <input type="hidden" name="diagnosis" value="${diagnosis}">
                        <input type="hidden" name="complains" value="${complains}">
                        <input type="hidden" name="session" value="${session}">
                        <input type="hidden" name="name" value="${Name}">
                        <input type="hidden" name="gender" value="${gender}">
                        <input type="hidden" name="age" value="${date}">
                        <input type="hidden" name="bloodGroup" value="${bloodGroup}">
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="serum" class="flat-red">
                                Serum
                            </label>

                            <label>
                                <input type="checkbox" name="plasma" class="flat-red">
                                Plasma
                            </label>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>General</label>
                            <select class="form-control select2" name="general" multiple="multiple" data-placeholder="Select a Tests" style="width: 100%;">
                                <option>Blood Sugar</option>
                                <option>Urea</option>
                                <option>Uric Acid</option>
                                <option>Calcium</option>
                                <option>Acid Phosphate(Prostatic)</option>
                                <option>Cholesterol</option>
                                <option>Amylase</option>
                                <option>Phosphorus</option>
                                <option>Total Protein</option>
                                <option>Albumen</option>
                                <option>Globulin</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Electrolytes</label>
                            <select class="form-control select2" name="electrolytes" multiple="multiple" data-placeholder="Select Electrolytes Tests" style="width: 100%;">
                                <option>Sodium</option>
                                <option>Potassium</option>
                                <option>Chloride</option>
                                <option>Bicarbonate</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Liver Function Tests</label>
                            <select class="form-control select2" name="liver" multiple="multiple" data-placeholder="Select Liver Function Tests" style="width: 100%;">
                                <option>Bilirubin (Total)</option>
                                <option>Bilirubin (Direct)</option>
                                <option>Bilirubin (Indirect)</option>
                                <option>Alkaline Phosphate</option>
                                <option>Amiotransfcrase (S.G.O.T)</option>
                                <option>Amiotransfcrase (S.G.P.T)</option>
                                <option>Y-Glutamy transfcrase (Y-GT)</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Cardic Enzymes</label>
                            <select class="form-control select2" name="cardic" multiple="multiple" data-placeholder="Select Cardic Enzymes Tests" style="width: 100%;">
                                <option>Lactic Dehydrogenase (LDH)</option>
                                <option>Hydroxybutyrate Dehydrogenase (HBD)</option>
                                <option>Creatinine Phospho Kinase (CPK)</option>
                                <option>Aminotransfcrase (S.G.O.T)</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Creatinine</label>
                            <select class="form-control select2" name="creatinine" multiple="multiple" data-placeholder="Select Creatinine Tests" style="width: 100%;">
                                <option>Creatinine Clearance</option>
                                <option>Creatinine (Serum)</option>
                                <option>Creatinine (Urine)</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="psa" class="flat-red">
                                Total Prostatic Specification Antigen (P.S.A)
                            </label>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="thyroid" class="flat-red">
                                Thyroid Function Tests
                            </label>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="lipid" class="flat-red">
                                Lipid Profile
                            </label>
                        </div>
                        <br>
                        <button class="pull-left btn btn-primary" type="submit" name="action" value="back">&larr; Back to Patients Column</button>
                        <button class="pull-right btn btn-success" type="submit" name="action" value="send">Send Lab Order &rarr;</button>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><h4>Chief Complains</h4></label>
                        <textarea rows="8" placeholder="Enter ..." name="complains" id="diagnosis" class="form-control" required readonly>${complains}</textarea>
                    </div>
                    <div class="form-group">
                        <label><h4>Diagnosis</h4></label>
                        <textarea rows="8" placeholder="Enter ..." name="diagnosis" id="diagnosis" class="form-control" required readonly>${diagnosis}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <br>
                <div class='well'>
                    <h4>Patient's Information</h4>
                    <p>
                        <b>Name:</b>
                    <p>${Name}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Gender:</b>
                    <p>${gender}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Age:</b>
                    <p>${date}</p>
                    </p>
                    <!--<hr>-->
                    <p>
                        <b>Blood Group:</b>
                    <p>${bloodGroup}</p>
                    </p>
                </div>
                
                <%@include file="WEB-INF/web/MedicalHistory.jsp" %>
            </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/LabOrder-end.jsp" %>