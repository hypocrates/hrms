<%-- 
    Document   : PatientsDetails
    Created on : 21-Apr-2017, 12:55:52
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patients Details
<!--        <small>Optional description</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patients List</a></li>
        <li class="active">Patient's Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150/83</h3>

              <p>Blood Pressure</p>
            </div>
            <div class="icon">
              <i class="ion ion-heart"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53</h3>

              <p>BMI</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>Hospital Visits</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65<sup style="font-size: 20px">%</sup></h3>

              <p>Appointment Discipline</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          
            <!--Current Status-->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Current Status</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!--/.box-header-->
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="ion ion-medkit"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Laboratory test</span>
                      <span class="info-box-number">50%</span>

                      <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                      </div>
                          <span class="progress-description">
                            Has been on test for 35 mins
                          </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="row">
                        <div class="col-sm-3 col-sm-offset-1">
                        <form action="PatientsInSession" method="post">
                            <input type="hidden" name="pRef" value="${pRef}">
                            <input type="hidden" name="session" value="${session}">
                            <button type="submit" class="btn btn-primary" name="action" value="medicalHistory">View Medical History</button>
                        </form>
                        </div>
                        <div class="col-sm-3 col-sm-offset-1">
                        <form action="PatientsInSession" method="post">
                            <input type="hidden" name="pRef" value="${pRef}">
                            <input type="hidden" name="session" value="${session}">
                            <button type="submit" class="btn btn-success" name="action" value="continueSession">Continue Current Session</button>
                        </form>
                        </div>
                        <div class="col-sm-3 col-sm-offset-1">
                            <form action="Patient" method="post" class="form-group">
                                <input type="hidden" name="pRef" value="${pRef}">
                                <input type="hidden" name="session" value="${session}">
                                <button type="submit" name="action" value="clearForExit" class="btn btn-danger">Exit Patient</button>
                            </form>
                        </div>.
                </div>
                <br>
            </div>
            <!--/.Current-status-->
            
            

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
<!--        <section class="col-lg-5 connectedSortable">
          
           
          

        </section>-->
        <!-- right col -->
      </div>
      <!-- /.row (first row) -->
      
      <!--Second Row-->
      <div class="row">
          <div class="col-md-8">
              <!-- quick email widget -->
              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>

                  <h3 class="box-title">Quick Email</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button> 
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fa fa-times"></i></button>
                  </div>
                  <!-- /. tools -->
                </div>
                <div class="box-body">
                  <form action="#" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                    <div>
                      <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  </form>
                </div>
                <div class="box-footer clearfix">
                  <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                    <i class="fa fa-arrow-circle-right"></i></button>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <!--./Graph-Box-->
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Hospital Visits History</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="box-body">
                        <canvas id="pieChart" height="150"></canvas>
                      </div>
                      <!-- ./chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                        <li><i class="fa fa-circle-o text-green"></i> IE</li>
                        <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                        <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                        <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                        <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">United States of America
                      <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                    <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                    </li>
                    <li><a href="#">China
                      <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                  </ul>
                </div>
                <!-- /.footer -->
              </div>
          <!-- /.Graph-Box -->
          </div>
      </div>
      <!--/.Second Row-->
      
      <!--Third-Row-->
      <div class="row">
          <!--Appointment Calendar-->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Appointment Calender</h3>
                    
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!--/.box-header-->
                <div class="row">
                    <div class="col-md-3">
                      <div class="box box-solid">
                        <div class="box-header with-border">
                          <h4 class="box-title">Draggable Events</h4>
                        </div>
                        <div class="box-body">
                          <!-- the events -->
                          <div id="external-events">
                            <div class="external-event bg-green">Lunch</div>
                            <div class="external-event bg-yellow">Go home</div>
                            <div class="external-event bg-aqua">Do homework</div>
                            <div class="external-event bg-light-blue">Work on UI design</div>
                            <div class="external-event bg-red">Sleep tight</div>
                            <div class="checkbox">
                              <label for="drop-remove">
                                <input type="checkbox" id="drop-remove">
                                remove after drop
                              </label>
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /. box -->
                      <div class="box box-solid">
                        <div class="box-header with-border">
                          <h3 class="box-title">Create Event</h3>
                        </div>
                        <div class="box-body">
                          <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                            <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                            <ul class="fc-color-picker" id="color-chooser">
                              <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                              <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                            </ul>
                          </div>
                          <!-- /btn-group -->
                          <div class="input-group">
                            <input id="new-event" type="text" class="form-control" placeholder="Event Title">

                            <div class="input-group-btn">
                              <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                            </div>
                            <!-- /btn-group -->
                          </div>
                          <!-- /input-group -->
                        </div>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                      <div class="box box-primary">
                        <div class="box-body no-padding">
                          <!-- THE CALENDAR -->
                          <div id="calendar"></div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /. box -->
                    </div>
                    <!-- /.col -->
                  </div>
            </div>
      </div>
      <!--/.Third-Row-->
    </section>
    <!-- /.content -->
    
  </div>

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/PatientsDetails-end.jsp" %>
