<%-- 
    Document   : footer
    Created on : 20-Apr-2017, 23:28:43
    Author     : Wamuyu
--%>

<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      By Debull
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">Hypocrates</a>.</strong> All rights reserved.
  </footer>