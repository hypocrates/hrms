<%-- 
    Document   : sidebar
    Created on : 20-Apr-2017, 23:26:03
    Author     : Wamuyu
--%>

<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><%= session.getAttribute("user")%></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
<!--      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        
        <%
            String access =(String) session.getAttribute("access");
            if (access.equals("Nurse")){
        %>
        <li class="active">
            <a href="Nurses Dashboard.jsp">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-registered"></i>
                <span>Reception</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="NewPatientDisplay"><i class="fa fa-plus-square"></i><span>Register a Patient</span></a></li>
                <li><a href="HospitalPatients"><i class="fa fa-list"></i><span>Patient List in Hospital</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-registered"></i>
                <span>Chemist</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <!--<li><a href="NewPatient.jsp"><i class="fa fa-plus-square"></i><span>Register a Patient</span></a></li>-->
                <li><a href="ClearedPatientsList"><i class="fa fa-list"></i><span>Patients Medications List</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-registered"></i>
                <span>Ward Rounds</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <!--<li><a href="NewPatient.jsp"><i class="fa fa-plus-square"></i><span>Register a Patient</span></a></li>-->
                <li><a href="AdmittedPatientsList"><i class="fa fa-bed"></i><span>Patients Admitted in hospital Lists</span></a></li>
            </ul>
        </li>
        <%
            } else if(access.equals("Doctor")){
        %>
        <li class="active">
            <a href="DoctorsDashboard">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-heartbeat"></i> 
                <span>Doctor</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="NewPatientsList"><i class="fa fa-plus-square"></i><span>New Patients</span></a></li>
                <li><a href="PatientsInSessionList"><i class="fa fa-stethoscope"></i><span>Patient in Session</span></a></li>
                <li><a href="AdmittedPatientsList"><i class="fa fa-bed"></i><span>Admitted Patients</span></a></li>
            </ul>
        </li>
        <%
            } else if(access.equals("LabTech")){
        %>
        <li class="active">
            <a href="LabTechDashboard">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list"></i>
                <span>Laboratory Technician</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="LabPatientsList"><i class="fa fa-plus-square"></i><span>Patients Waiting</span></a></li>
                <!--<li><a href="#"><i class="fa fa-plus-square"></i><span>Patients in Session</span></a></li>-->
            </ul>
        </li>
        <%
            } else if(access.equals("Admin")){
        %>
        <li class="active">
            <a href="AdminDashboard">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-h-square"></i> 
                <span>Reports</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-user-md"></i><span>Patients Reports</span></a></li>
                <li><a href="#"><i class="fa fa-dashcube"></i><span>Doctors Reports</span></a></li>
                <li><a href="#"><i class="fa fa-hospital-o"></i><span>Nurses Reports</span></a></li>
                <li><a href="#"><i class="fa fa-user-md"></i><span>Lab Attendants Reports</span></a></li>
            </ul>
        </li>
        <%
            } else if(access.equals("Root")){
        %>
        <li class="active">
            <a href="SuperAdmin Dashboard.jsp">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-registered"></i>
                <span>Nurse</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="NewPatient.jsp"><i class="fa fa-plus-square"></i><span>Register a Patient</span></a></li>
                <li><a href="HospitalPatients"><i class="fa fa-list"></i><span>Patient List in Hospital</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-heartbeat"></i> 
                <span>Doctor</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="NewPatientsList"><i class="fa fa-plus-square"></i><span>New Patients</span></a></li>
                <li><a href="PatientsInSessionList"><i class="fa fa-stethoscope"></i><span>Patient in Session</span></a></li>
                <li><a href="AdmittedPatientsList"><i class="fa fa-bed"></i><span>Admitted Patients</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list"></i>
                <span>Laboratory Technician</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="LabPatientsList"><i class="fa fa-plus-square"></i><span>Patients Waiting</span></a></li>
                <li><a href="#"><i class="fa fa-plus-square"></i><span>Patients in Session</span></a></li>
            </ul>
        </li>
        <li class="treeview">
          <a href="#">
              <i class="fa fa-h-square"></i> 
              <span>Administrator</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="index2.html"><i class="fa fa-dashcube"></i><span>Dashboard</span></a></li>
              <li><a href="#"><i class="fa fa-hospital-o"></i><span>Hospitals</span></a></li>
              <li><a href="#"><i class="fa fa-user-md"></i><span>Physicians</span></a></li>
          </ul>
        </li>
        <%
            }
        %>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
