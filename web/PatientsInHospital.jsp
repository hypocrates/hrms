<%-- 
    Document   : PatientsInHospital
    Created on : 12-May-2017, 09:41:12
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patients in Hospital
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Reception Desk</a></li>
        <li class="active">Patients' list</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <p class="alert alert-info">${message}</p>
      <div class="row">
      <!-- Your Page Content Here -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">.... Hospital</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <%@page import="Bean.HospitalPatients, java.util.ArrayList" %>
                    <%
                        int size =(Integer) request.getAttribute("size");
                        ArrayList<HospitalPatients> patients =(ArrayList) request.getAttribute("patients");
                        //if (size < 15) {
                            for (int x = 0; x < size; x++) {
                            HospitalPatients patient = patients.get(x);
                            int nationalID = patient.getNationalID();
                            String name = patient.getName();
                            String status = patient.getStatus();
                            String category = patient.getCategory();
                            String action = patient.getAction();

                    %>
                    <tr>
                        <td><%= nationalID %></td>
                        <td><%= name %></td>
                        <td><%= status %></td>
                        <td><%= category %></td>
                        <td><%= action %></td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
                <tfoot>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/PatientsInHospital-end.jsp" %>
