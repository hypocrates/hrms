<%-- 
    Document   : HospitalStaff
    Created on : 26-Jun-2017, 21:45:52
    Author     : Wamuyu
--%>

<%@include file="WEB-INF/web/header.jsp" %>
<%@include file="WEB-INF/web/navbar.jsp" %>
<%@include file="WEB-INF/web/left-sidebar.jsp" %>

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          <%= session.getAttribute("HospitalName")%>
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Your Page Content Here -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><%=session.getAttribute("HospitalName")%> ${type}s<small>${message}</small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <%
                      if("Doctor".equals(request.getAttribute("type"))){
                  %>
                  <th>Specialisation</th>
                  <% }%>
                  <th>Date Employed</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <%@page import="Bean.UsersData, java.util.ArrayList, java.sql.Date" %>
                    <%
                        int size =(Integer) request.getAttribute("size");
                        ArrayList<UsersData> usArray =(ArrayList) request.getAttribute("usArray");
                        //if (size < 15) {
                            for (int x = 0; x < size; x++) {
                            UsersData user = usArray.get(x);
                            int nationalID = user.getNationalID();
                            int dRef = user.getdRef();
                            String name = user.getSirName()+ " "+user.getFirstname();
                            if(user.getOtherNames() != null){
                                name += " "+ user.getOtherNames();
                            }
                            String Specialisation = user.getSpecialisation();
                            Date date = user.getDate();

                    %>
                    <tr>
                        <td><%= nationalID %></td>
                        <td><%= name %></td>
                        <%
                            if("Doctor".equals(request.getAttribute("type"))){
                        %>
                        <td><%= Specialisation %></td>
                        <% }%>
                        <td><%= date %></td>
                        <td><form action="Admin" method="post">
                                <input type="hidden" name="dRef" value="<%= dRef %>">
                                <input type="hidden" name="session" value="">
                                <button type="submit" name="action" value="details" class="btn btn-primary">Dismiss</button>
                            </form></td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
                <tfoot>
                <tr>
                  <th>National ID</th>
                  <th>Name</th>
                  <%
                      if("Doctor".equals(request.getAttribute("type"))){
                  %>
                  <th>Specialisation</th>
                  <% }%>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@include file="WEB-INF/web/footer.jsp" %>
<%@include file="WEB-INF/web/right-sidebar.jsp" %>
<%@include file="WEB-INF/web/PatientsInHospital-end.jsp" %>